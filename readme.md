## Dr.Pediu

Web Service para os aplicativos Dr.Pediu (Paciente e Doutor) e para a aplicação SPA Admin.

## Requerimentos
- PHP 7.2 or higher
- Composer
- MySQL
- Redis
- Docker Compose

## Authors
- Clezer Ramos

## Contributing
- Clezer Ramos

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
