<?php

use Illuminate\Database\Seeder;
use DrPediu\Models\Comment;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Comment::firstOrCreate([
            'comments' => 'Cursus curabitur a eu nisi aenean felis volutpat rhoncus turpis laoreet, aenean curabitur felis quisque dui',

        ]);
        Comment::firstOrCreate([
            'comments' => 'Duis fringilla metus aenean sapien litora convallis scelerisque hendrerit quis, vestibulum scelerisque',
        ]);
        Comment::firstOrCreate([
            'comments' => 'jwerrew fringilla metus aenean sapien litora convallis scelerisque hendrerit quis, vestibulum scelerisque',

        ]);
    }
}
