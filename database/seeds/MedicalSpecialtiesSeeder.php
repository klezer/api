<?php

use Illuminate\Database\Seeder;
use DrPediu\Models\SpecialtiesMedical;

class MedicalSpecialtiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = [
            'Acupuntura',
            'Alergia e imunologia',
            'Anestesiologia',
            'Angiologia',
            'Cancerologia',
            'Cardiologia',
            'Cirurgia cardiovascular',
            'Cirurgia da mão',
            'Cirurgia de cabeça e pescoço',
            'Cirurgia do aparelho digestivo',
            'Cirurgia e traumatologia buco-maxilo-facial',
            'Cirurgia geral',
            'Cirurgia pediátrica',
            'Cirurgia plástica',
            'Cirurgia torácica',
            'Cirurgia vascular',
            'Cirurgia veterinária',
            'Clínica médica',
            'Clínica médica de pequenos animais',
            'Coloproctologia',
            'Dentística',
            'Dentística restauradora',
            'Dermatologia',
            'Disfunção têmporo mandibular e dor orofacial',
            'Endocrinologia e metabologia',
            'Endodontia',
            'Endoscopia',
            'Estomatologia',
            'Gastroenterologia',
            'Genética médica',
            'Geriatria',
            'Ginecologia e obstetrícia',
            'Hematologia e hemoterapia',
            'Homeopatia',
            'Implantodontia',
            'Infectologia',
            'Mastologia',
            'Medicina de família e comunidade',
            'Medicina de tráfego',
            'Medicina do trabalho',
            'Medicina esportiva',
            'Medicina felina',
            'Medicina física e reabilitação',
            'Medicina intensiva',
            'Medicina legal e perícia médica',
            'Medicina nuclear',
            'Medicina preventiva e social',
            'Medicina veterinária intensiva',
            'Medicina veterinária legal',
            'Nefrologia',
            'Neurocirurgia',
            'Neurologia',
            'Nutrologia',
            'Odontogeriatria',
            'Odontologia do trabalho',
            'Odontologia em saúde coletiva',
            'Odontologia legal',
            'Odontologia para pacientes com necessidades especiais',
            'Odontopediatria',
            'Oftalmologia',
            'Oncologia',
            'Ortodontia',
            'Ortodontia e ortopedia facial',
            'Ortopedia e traumatologia',
            'Ortopedia funcional dos maxilares',
            'Otorrinolaringologia',
            'Patologia',
            'Patologia bucal',
            'Patologia clínica / medicina laboratorial',
            'Pediatria',
            'Periodontia',
            'Pneumologia',
            'Prótese buco-maxilo-facial',
            'Prótese dentária',
            'Psiquiatria',
            'Radiologia e diagnóstico por imagem',
            'Radiologia odontológica',
            'Radiologia odontológica e imaginologia',
            'Radioterapia',
            'Reumatologia',
            'Saúde coletiva',
            'Urologia',
            'Outras'
        ];


        foreach ($datas as $data){

            SpecialtiesMedical::firstOrCreate([
               'title' => $data
            ]);
        }
    }
}
