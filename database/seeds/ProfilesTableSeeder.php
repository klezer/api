<?php

use Illuminate\Database\Seeder;
use DrPediu\Models\Profile;

class ProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Profile::firstOrCreate([
           'user_id' => 1,
           'image' => \Storage::disk('avatars')->put('', new \Illuminate\Http\File(public_path('seeds-images/av1.png'), 'av1.png')),
            'description' => 'imagem de teste'
        ]);
        Profile::firstOrCreate([
            'user_id' => 2,
            'image' => \Storage::disk('avatars')->put('', new \Illuminate\Http\File(public_path('seeds-images/av2.png'), 'av2.png')),
            'description' => 'imagem de teste'
        ]);
        Profile::firstOrCreate([
            'user_id' => 3,
            'image' => \Storage::disk('avatars')->put('', new \Illuminate\Http\File(public_path('seeds-images/outro.png'), 'outro.png')),
            'description' => 'imagem de teste'
        ]);
    }
}
