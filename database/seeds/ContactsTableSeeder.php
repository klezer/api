<?php

use Illuminate\Database\Seeder;
use DrPediu\Models\Contact;
class ContactsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Contact::firstOrCreate([
            'user_id' => 1,
            'telephone_one' => '(11) 49630563',
            'telephone_two' => '(11) 79635412',
            'cell_phone' => '(11) 979815623'
        ]);
        Contact::firstOrCreate([
            'user_id' => 2,
            'telephone_one' => '(11) 29635648',
            'telephone_two' => '(11) 88963598',
            'cell_phone' => '(11) 979863542'
        ]);
        Contact::firstOrCreate([
            'user_id' => 3,
            'telephone_one' => '(11) 26369892',
            'telephone_two' => '(11) 46986221',
            'cell_phone' => '(11) 980360654'
        ]);
    }
}
