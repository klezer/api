<?php

use Illuminate\Database\Seeder;

use DrPediu\Models\QuestsAnamnese;

class AnamneseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [

            ['title' => 'Durante os últimos 2 anos recebeu algum tratamento médico?','sub_title' => ['Em que especialidade?','Nome do médico?']],
            ['title' => 'Já teve hospitalizado?','sub_title' => ['Quando?','Quanto tempo?','Porque?']],
            ['title' => 'Toma medicamentos?','sub_title' => ['Quais?']],
            ['title' => 'Tem alergia a algum medicamento ou anestésico?'],
            ['title' => 'Já fez cirurgias?','sub_title' => ['Cirurgias bucais?','Quanto?']],
            ['title' => 'Costuma sangrar muito quando se machuca?','sub_title' => ['Demora para cicatrizar?']],
            ['title' => 'Já fez transfusão de sangue?'],
            ['title' => 'Você fuma?','sub_title' => ['Quantos cigarros/dia?']],
            ['title' => 'Toma bebidas alcoólicas frequentemente?'],
            ['title' => 'Costuma sentir tontura ou ter desmaio?'],

            ['title' => 'Problemas cardíacos?'],
            ['title' => 'Hepatite?'],
            ['title' => 'Febre reumática?'],
            ['title' => 'Problemas nervosos?'],
            ['title' => 'Respiratório?'],
            ['title' => 'HIV?'],
            ['title' => 'Pressão alta?'],
            ['title' => 'Diabetes?'],
            ['title' => 'Icterícia?'],
            ['title' => 'Hepático?'],
            ['title' => 'Tuberculose?'],
            ['title' => 'Anemia?'],
            ['title' => 'Epilepsia?'],
            ['title' => 'Reumatismo?'],
            ['title' => 'Renal?'],
            ['title' => 'Depressão?'],
            ['title' => 'Tem fobias?'],
            ['title' => 'Problemas alimentares?'],
            ['title' => 'Está grávida?','sub_title' => ['Quanto tempo?']],
            ['title' => 'Outras?','sub_title' => ['Quais?']]

            ];

        foreach ($data as $key => $item){

            if(isset($item['sub_title'])) {
                $quests = QuestsAnamnese::firstOrCreate([
                    'title' => $item['title']
                ]);

                foreach ($item['sub_title'] as $sub_title){
                    if($key == $key){
                        $quests->sub_quest_anamnese()->firstOrCreate([
                            'title' => $sub_title
                        ]);
                    }
                }
            }
            $quests = QuestsAnamnese::firstOrCreate([
                'title' => $item['title']
            ]);

        }
    }
}
