<?php

use Illuminate\Database\Seeder;
use DrPediu\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user =  User::where('email','admin@admin.doc88.com.br')->first();

        if(!$user)
        {
            $this->call(UsersTableSeeder::class);
        }

        $this->call(StatesTableSeeder::class);
        $this->call(CidadesTableSeeder::class);
        $this->call(TypeRecipesTableSeeder::class);
        $this->call(DoctorsTableSeeder::class);
        $this->call(RecipesTableSeeder::class);
        $this->call(MedicinesTableSeeder::class);
        $this->call(CommentsTableSeeder::class);
        $this->call(UserAddressTableSeeder::class);
        $this->call(ContactsTableSeeder::class);
        $this->call(ProfilesTableSeeder::class);
        $this->call(OauthClientTableSeeder::class);
        $this->call(MedicalSpecialtiesSeeder::class);
        $this->call(DiscartPointsSeeder::class);
        $this->call(AnamneseTableSeeder::class);
        $this->call(PivotRecipesxMedicinesxTreatmentsSeeder::class);
        $this->call(PivotDoctorsxRecipesxUsersSeeder::class);
        $this->call(PivotCommenstxRecipesxUsersSeeder::class);
        $this->call(DrugInteractionTableSeeder::class);
   ; }
}
