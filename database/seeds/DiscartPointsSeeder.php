<?php

use Illuminate\Database\Seeder;

use DrPediu\Models\DiscartPoint;

class DiscartPointsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $json = [

            [
                "title"=> "UBS ADELAIDE LOPES",
                "fone"=> "20183519",
                "street_title"=> "JOSE RODRIGUES SANTAREN AV",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM COLONIAL",
                "latitude"=> -23.4982318,
                "longitude"=> -46.67763859999999
            ],
            [
                "title"=> "UBS AE CARVALHO",
                "fone"=> "20468809",
                "street_title"=> "RUA JOSE SILVA ALCANTARA FILHO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "BURGO PAULISTA",
                "latitude"=> -23.5232747,
                "longitude"=> -46.4905738
            ],
            [
                "title"=> "UBS AGUA FUNDA",
                "fone"=> "20511596",
                "street_title"=> "RUA TREVO DE SANTA MARIA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "SAO MIGUEL PAULISTA",
                "latitude"=> -23.512565,
                "longitude"=> -46.4625978
            ],
            [
                "title"=> "UBS AGUA RASA",
                "fone"=> "20736039",
                "street_title"=> "RUA PAULINO CERQUEIRA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "ITAQUERA",
                "latitude"=> -23.534011,
                "longitude"=> -46.442704
            ],
            [
                "title"=> "UBS AGUIA DE HAIA",
                "fone"=> "22417317",
                "street_title"=> "RUA DR ANTONIO CESAR NETO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JACANA",
                "latitude"=> -23.4611844,
                "longitude"=> -46.58323550000001
            ],
            [
                "title"=> "UBS ALCINA PIMENTEL PIZA",
                "fone"=> "22624846",
                "street_title"=> "RUA PAULO CESAR",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM TREMEMBE",
                "latitude"=> -23.7688467,
                "longitude"=> -46.6492449
            ],
            [
                "title"=> "UBS ALMIRANTE DELAMARE",
                "fone"=> "22734602",
                "street_title"=> "RUA ALVARO FRAGOSO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA CARIOCA",
                "latitude"=> -23.5966692,
                "longitude"=> -46.5924077
            ],
            [
                "title"=> "UBS ALPES DO JARAGUA",
                "fone"=> "22860015",
                "street_title"=> "RUA AMERICO SALVADOR NOVELLI",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "ITAQUERA",
                "latitude"=> -23.4467938,
                "longitude"=> -46.753799
            ],
            [
                "title"=> "UBS ALTO DA RIVIERA",
                "fone"=> "22943028",
                "street_title"=> "RUA ANTONIO CAMARDO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA GOMES CARDIM",
                "latitude"=> -23.6996022,
                "longitude"=> -46.7673756
            ],
            [
                "title"=> "UBS ALTO DE PINHEIROS",
                "fone"=> "22978709",
                "street_title"=> "RUA EDIPO FELICIANO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA JACUI",
                "latitude"=> -23.5025625,
                "longitude"=> -46.4500403
            ],
            [
                "title"=> "UBS ALTO DO UMUARAMA",
                "fone"=> "25211226",
                "street_title"=> "RUA MURMURIOS DA TARDE",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "COHAB JOSE BONIFACIO",
                "latitude"=> -23.5537467,
                "longitude"=> -46.4446892
            ],
            [
                "title"=> "UBS AMERICANOPOLIS",
                "fone"=> "25230577",
                "street_title"=> "RUA CACHOEIRA DA ILHA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "ITAQUERA",
                "latitude"=> -23.5593535,
                "longitude"=> -46.46016729999999
            ],
            [
                "title"=> "UBS ANHANGUERA",
                "fone"=> "25567997",
                "street_title"=> "RUA JORGE MARACCINI PONFILIO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "COHAB JUSCELINO",
                "latitude"=> -23.4312912,
                "longitude"=> -46.7875045
            ],
            [
                "title"=> "UBS ANTONIO P F VILLALOBO",
                "fone"=> "25863812",
                "street_title"=> "AVN KUMAKI AOKI",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM HELENA",
                "latitude"=> -23.47926,
                "longitude"=> -46.4196512
            ],
            [
                "title"=> "UBS ATUALPA GIRAO RABELO",
                "fone"=> "26185255",
                "street_title"=> "RUA TAQUARI",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "MOOCA",
                "latitude"=> -23.5082541,
                "longitude"=> -46.3954403
            ],
            [
                "title"=> "UBS AUGUSTO LEOPOLDO AYROSA GALVAO",
                "fone"=> "26219507",
                "street_title"=> "AV CANGAIBA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "CANGAIBA",
                "latitude"=> -23.544422,
                "longitude"=> -46.650611
            ],
            [
                "title"=> "UBS AURELIO MELLONE",
                "fone"=> "26576630",
                "street_title"=> "RUA MARIA CARLOTA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA ESPERANCA",
                "latitude"=> -23.6573493,
                "longitude"=> -46.6142714
            ],
            [
                "title"=> "UBS BARRAGEM",
                "fone"=> "26745246",
                "street_title"=> "PRACA MARQUES DE NAZARE",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA FORMOSA",
                "latitude"=> -23.8777007,
                "longitude"=> -46.6502341
            ],
            [
                "title"=> "UBS BARRO BRANCO",
                "fone"=> "26858036",
                "street_title"=> "R MADALENA JULIA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA GRANADA",
                "latitude"=> -23.5837702,
                "longitude"=> -46.3930573
            ],
            [
                "title"=> "UBS BELENZINHO MARCUS WOLOSKER",
                "fone"=> "27249034",
                "street_title"=> "RUA CORONEL JOAO OLIVEIRA MELO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA ANTONIETA",
                "latitude"=> -23.5371988,
                "longitude"=> -46.59556080000001
            ],
            [
                "title"=> "UBS BOM RETIRO OCTAVIO AUGUSTO RODOVALHO",
                "fone"=> "27421367",
                "street_title"=> "RUA SILVIO TORRES",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "ARTHUR ALVIM",
                "latitude"=> -23.5256699,
                "longitude"=> -46.64070599999999
            ],
            [
                "title"=> "UBS BORACEA",
                "fone"=> "27916421",
                "street_title"=> "AV JAIME TORRES",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM POPULAR",
                "latitude"=> -23.5178091,
                "longitude"=> -46.5062691
            ],
            [
                "title"=> "UBS BRAS MANOEL SALDIVA NETO",
                "fone"=> "29195006",
                "street_title"=> "ARISTIDES RICARDO DR R",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "PARQUE SAO RAFAEL",
                "latitude"=> -23.5381787,
                "longitude"=> -46.612042
            ],
            [
                "title"=> "UBS BRASILANDIA",
                "fone"=> "29446086",
                "street_title"=> "RUA SILVIO BARBINI",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "ITAQUERA",
                "latitude"=> -23.540886,
                "longitude"=> -46.439532
            ],
            [
                "title"=> "UBS BRASILIA M BOI MIRIM",
                "fone"=> "29461443",
                "street_title"=> "RUA NOSSA SENHORA DAS MERCES",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA DAS MERCES",
                "latitude"=> -23.6296066,
                "longitude"=> -46.6049212
            ],
            [
                "title"=> "UBS BURGO PAULISTA",
                "fone"=> "29468532",
                "street_title"=> "RUA CARLOS MAURO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "SAO JOAO CLIMACO",
                "latitude"=> -23.5233397,
                "longitude"=> -46.4905062
            ],
            [
                "title"=> "UBS BUTANTA",
                "fone"=> "29563837",
                "street_title"=> "RUA GRAZIELE BALDARK GOMES",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "SAO MIGUEL PAULISTA",
                "latitude"=> -23.4861588,
                "longitude"=> -46.4598332
            ],
            [
                "title"=> "UBS CAMBUCI",
                "fone"=> "36212181",
                "street_title"=> "RUA PEDRO RAVARA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARAGUA",
                "latitude"=> -23.5696198,
                "longitude"=> -46.6235888
            ],
            [
                "title"=> "UBS CAMPO GRANDE",
                "fone"=> "37120704",
                "street_title"=> "RUA GUSTAVO BERTHIER",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA DALVA",
                "latitude"=> -23.5636092,
                "longitude"=> -46.7650772
            ],
            [
                "title"=> "UBS CAMPO LIMPO",
                "fone"=> "37261838",
                "street_title"=> "R CABRAL DE MENEZES",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA GOMES",
                "latitude"=> -23.5769234,
                "longitude"=> -46.7338403
            ],
            [
                "title"=> "UBS CAMPO LIMPO FRANCISCO SCALAMANDRE SOBRINHO",
                "fone"=> "37422942",
                "street_title"=> "RUA SILVEIRA SAMPAIO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JD MORUMBI",
                "latitude"=> -23.6366519,
                "longitude"=> -46.76394819999999
            ],
            [
                "title"=> "UBS CANGAIBA CARLOS GENTILE DE MELLO",
                "fone"=> "37445223",
                "street_title"=> "RUA MELCHIOR GIOLA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JD PARAISOPOLIS",
                "latitude"=> -23.505447,
                "longitude"=> -46.5209518
            ],
            [
                "title"=> "UBS CARANDIRU",
                "fone"=> "38588303",
                "street_title"=> "RUA JOSE RANGEL CAMARGO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "PARQUE PERUCHE",
                "latitude"=> -23.4977009,
                "longitude"=> -46.6549657
            ],
            [
                "title"=> "UBS CARLOS GENTILE DE MELO",
                "fone"=> "39048999",
                "street_title"=> "RUA MARCELA ALVES DE CASSIA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM JARAGUA",
                "latitude"=> -23.4886416,
                "longitude"=> -46.7590851
            ],
            [
                "title"=> "UBS CARRAOZINHO",
                "fone"=> "39318242",
                "street_title"=> "RUA FRANCISCO LOTUFO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "FREGUESIA DO O",
                "latitude"=> -23.4945558,
                "longitude"=> -46.6852145
            ],
            [
                "title"=> "UBS CASA VERDE",
                "fone"=> "39368370",
                "street_title"=> "AVN CLAVASIO ALVES DA SILVA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "LIMAO",
                "latitude"=> -23.4982318,
                "longitude"=> -46.67763859999999
            ],
            [
                "title"=> "UBS CASA VERDE ALTA",
                "fone"=> "39418845",
                "street_title"=> "PCA LUIZ VAZ DE CAMOES",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "SOL NASCENTE",
                "latitude"=> -23.4893061,
                "longitude"=> -46.664612
            ],
            [
                "title"=> "UBS CASTRO ALVES",
                "fone"=> "39751289",
                "street_title"=> "RUA ANTONIO GENELLE",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM MONTE ALEGRE",
                "latitude"=> -23.5804798,
                "longitude"=> -46.404453
            ],
            [
                "title"=> "UBS CAXINGUI NANCI ABRANCHES",
                "fone"=> "39759019",
                "street_title"=> "RUA DOM MANOEL D EL BOUK",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM SAO JOSE",
                "latitude"=> -23.5791109,
                "longitude"=> -46.7149382
            ],
            [
                "title"=> "UBS CDHU PALANQUE",
                "fone"=> "39813127",
                "street_title"=> "RUA CEL WALFRIDO DE CARVALHO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VL NOVA CACHOEIRINHA",
                "latitude"=> -23.5950974,
                "longitude"=> -46.4206183
            ],
            [
                "title"=> "UBS CEO I SANTO AMARO DR SERGIO VILLACA BRAGA",
                "fone"=> "50542851",
                "street_title"=> "AV INDIANOPOLIS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "INDIANOPOLIS",
                "latitude"=> -23.6073819,
                "longitude"=> -46.6529522
            ],
            [
                "title"=> "UBS CEO SAO FRANCISCO II",
                "fone"=> "55111630",
                "street_title"=> "RUA TEREZA MAIA PINTO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA PREL",
                "latitude"=> -23.6481765,
                "longitude"=> -46.7541754
            ],
            [
                "title"=> "UBS CHACARA CRUZEIRO DO SUL",
                "fone"=> "55111761",
                "street_title"=> "R JOAO FUGULIM",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JD NOVA GERMANIA",
                "latitude"=> -23.511479,
                "longitude"=> -46.5319084
            ],
            [
                "title"=> "UBS CHACARA DO CONDE",
                "fone"=> "55639070",
                "street_title"=> "RUA ANTONIO GIL",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA FILOMENA",
                "latitude"=> -23.6620771,
                "longitude"=> -46.6696664
            ],
            [
                "title"=> "UBS CHACARA DO SOL",
                "fone"=> "55882366",
                "street_title"=> "AV ENG ARMANDO DE ARRUDA PERREIRA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA DO ENCONTRO",
                "latitude"=> -23.6521797,
                "longitude"=> -46.6380721
            ],
            [
                "title"=> "UBS CHACARA INGLESA",
                "fone"=> "55884890",
                "street_title"=> "RUA DOS COMERCIARIOS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "CIDADE VARGAS",
                "latitude"=> -23.4937328,
                "longitude"=> -46.72524079999999
            ],
            [
                "title"=> "UBS CHACARA SANTANA",
                "fone"=> "56110122",
                "street_title"=> "RUA SAMUEL ARNOLD",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM NITEROI",
                "latitude"=> -23.6824443,
                "longitude"=> -46.6680309
            ],
            [
                "title"=> "UBS CHACARA SANTO AMARO",
                "fone"=> "56116219",
                "street_title"=> "R HENRIQUE MUZZIO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VARGINHA",
                "latitude"=> -23.821353,
                "longitude"=> -46.673458
            ],
            [
                "title"=> "UBS CHACARA SANTO ANTONIO DR MARCILIO DE ARRUDA PENTEADO F",
                "fone"=> "56125686",
                "street_title"=> "MATSUISHI WADA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "BALN MAR PAULISTA",
                "latitude"=> -23.6353207,
                "longitude"=> -46.6990981
            ],
            [
                "title"=> "UBS CHACARA STA MARIA",
                "fone"=> "56129644",
                "street_title"=> "AVENIDA BATISTA MACIEL",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA PORTELA",
                "latitude"=> -23.6928032,
                "longitude"=> -46.6697637
            ],
            [
                "title"=> "UBS CHORA MENINO",
                "fone"=> "56736180",
                "street_title"=> "RUA DENIS FURTEL",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "LARANJEIRAS",
                "latitude"=> -23.4943794,
                "longitude"=> -46.6373103
            ],
            [
                "title"=> "UBS CID PATRIARCA",
                "fone"=> "58165540",
                "street_title"=> "RUA FRANCISCO SOARES",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM INGA",
                "latitude"=> -23.6343781,
                "longitude"=> -46.74893609999999
            ],
            [
                "title"=> "UBS CIDADE IPAVA",
                "fone"=> "58211147",
                "street_title"=> "RUA LOUIS BOULOGNE",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM MACEDONIA",
                "latitude"=> -23.7203373,
                "longitude"=> -46.7648042
            ],
            [
                "title"=> "UBS CIDADE JULIA",
                "fone"=> "58211632",
                "street_title"=> "RUA ERNESTO SOARES FILHO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "PARQUE FERNANDA",
                "latitude"=> -23.6759423,
                "longitude"=> -46.7921297
            ],
            [
                "title"=> "UBS CIDADE KEMEL",
                "fone"=> "58335262",
                "street_title"=> "RUA CICLADES",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "CHACARA SANTA MARIA",
                "latitude"=> -23.504318,
                "longitude"=> -46.3669824
            ],
            [
                "title"=> "UBS CIDADE LIDER I",
                "fone"=> "58922327",
                "street_title"=> "ESTRADA DO BOI MIRIM",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "J COIMBRA",
                "latitude"=> -23.6786394,
                "longitude"=> -46.7626377
            ],
            [
                "title"=> "UBS CIDADE NOVA SAO MIGUEL",
                "fone"=> "58972564",
                "street_title"=> "R HUMBERTO DE ALMEIDA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "CHACARA SANTANA",
                "latitude"=> -23.5221372,
                "longitude"=> -46.4692967
            ],
            [
                "title"=> "UBS CIDADE PEDRO JOSE NUNES",
                "fone"=> "62322244",
                "street_title"=> "RUA ENGENHEIRO JEAN BUFF",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "CHACARA DO ENCOSTO",
                "latitude"=> -23.5009956,
                "longitude"=> -46.4623725
            ],
            [
                "title"=> "UBS CIDADE TIRADENTES I",
                "fone"=> "62391561",
                "street_title"=> "RUA OSCAR DE MOURA LACERDA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "IMIRIM",
                "latitude"=> -23.4867078,
                "longitude"=> -46.6555857
            ],
            [
                "title"=> "UBS CIDADE VARGAS",
                "fone"=> "63313089",
                "street_title"=> "RUA FRANCOIS BUNEL",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "PARQUE BRISTOL",
                "latitude"=> -23.6479362,
                "longitude"=> -46.63833349999999
            ],
            [
                "title"=> "UBS CITY JARAGUA",
                "fone"=> "112017270",
                "street_title"=> "ANGELO DE CANDIA R",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "SAO MATEUS",
                "latitude"=> -23.439877,
                "longitude"=> -46.7357674
            ],
            [
                "title"=> "UBS COLONIA",
                "fone"=> "112025969",
                "street_title"=> "RUA SILVEIRA PIRES",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "PARQUE PAULISTANO",
                "latitude"=> -23.4871521,
                "longitude"=> -46.43217749999999
            ],
            [
                "title"=> "UBS COLORADO",
                "fone"=> "112282032",
                "street_title"=> "RUA CADERNO DE VIAGEM",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "CASTRO ALVES",
                "latitude"=> -23.5805171,
                "longitude"=> -46.4043889
            ],
            [
                "title"=> "UBS COMENDADOR JOSE GONZALEZ",
                "fone"=> "112522317",
                "street_title"=> "RUA MALMEQUER DO CAMPO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "PRQ DO CAMPO",
                "latitude"=> -23.5700464,
                "longitude"=> -46.5346103
            ],
            [
                "title"=> "UBS CONJUNTO DO IPESP",
                "fone"=> "112558386",
                "street_title"=> "RUA LUIS BORDESE",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "CIDADE TIRADENTES",
                "latitude"=> -23.6029224,
                "longitude"=> -46.4052962
            ],
            [
                "title"=> "UBS COSTA MELO",
                "fone"=> "112731648",
                "street_title"=> "RAQUEB CHOHF AV",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JD TRES MARIAS",
                "latitude"=> -23.6056725,
                "longitude"=> -46.4711319
            ],
            [
                "title"=> "UBS CRUZ DAS ALMAS",
                "fone"=> "112755192",
                "street_title"=> "CINIRA POLONIO RUA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JAD RIO CLARO",
                "latitude"=> -23.6213551,
                "longitude"=> -46.4569781
            ],
            [
                "title"=> "UBS CUPECE DR WALDOMIRO PREGNOLATTO",
                "fone"=> "155274071",
                "street_title"=> "AVENIDA PROFESSORA MARTA MARIA BERNARDES",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA NATAL",
                "latitude"=> -23.7639671,
                "longitude"=> -46.7074129
            ],
            [
                "title"=> "UBS DOM ANGELICO",
                "fone"=> "1120128262",
                "street_title"=> "SOL R",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "CID SAT STA BARBARA",
                "latitude"=> -23.5804682,
                "longitude"=> -46.3829576
            ],
            [
                "title"=> "UBS DOM JOAO NERY",
                "fone"=> "1120153444",
                "street_title"=> "AVENIDA AUGUSTIN LUBERT",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "FAZENDA DA JUTA",
                "latitude"=> -23.522594,
                "longitude"=> -46.3998053
            ],
            [
                "title"=> "UBS DOMINGOS MANTELLI",
                "fone"=> "1120153459",
                "street_title"=> "TRAVESSA LEVI LANDAU",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "FAZENDA DA JUTA",
                "latitude"=> -23.4702811,
                "longitude"=> -46.7213234
            ],
            [
                "title"=> "UBS DOMINGOS MAZZONETO DE CILO VILA AURORA",
                "fone"=> "1120171431",
                "street_title"=> "ENGENHO NOVO AV",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM TIETE",
                "latitude"=> -23.4758709,
                "longitude"=> -46.6304113
            ],
            [
                "title"=> "UBS DON LUCIANO BERGAMIM",
                "fone"=> "1120197077",
                "street_title"=> "AV MANUEL FRANCA DOS SANTOS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM SAPOPEMBA",
                "latitude"=> -23.6098972,
                "longitude"=> -46.49270569999999
            ],
            [
                "title"=> "UBS DONA MARIQUINHA SCIASCIA",
                "fone"=> "1120351671",
                "street_title"=> "RUA CLAUDIO DA COSTA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "GUAIANASES",
                "latitude"=> -23.4609507,
                "longitude"=> -46.6226077
            ],
            [
                "title"=> "UBS DR CARLOS OLIVALDO DE SOUZA LOPES MUNIZ",
                "fone"=> "1120351756",
                "street_title"=> "RUA MANOEL TEODORO XAVIER",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "GUAIANAZES",
                "latitude"=> -23.513748,
                "longitude"=> -46.4829339
            ],
            [
                "title"=> "UBS DR GERALDO DA SILVA FERREIRA CEO II",
                "fone"=> "1120351860",
                "street_title"=> "AV NORDESTINA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "GUAIANAZES",
                "latitude"=> -23.6521797,
                "longitude"=> -46.6380721
            ],
            [
                "title"=> "UBS DR HUMBERTO PASCALE SANTA CECILIA",
                "fone"=> "1120352406",
                "street_title"=> "RUA JOSE MARIA ALVES DE DEUS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "SAO MIGUEL PAULISTA",
                "latitude"=> -23.5320617,
                "longitude"=> -46.6523853
            ],
            [
                "title"=> "UBS DR JOSE TOLEDO PIZA",
                "fone"=> "1120352488",
                "street_title"=> "RUA REAL HORTO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA PROGRESSO",
                "latitude"=> -23.5149791,
                "longitude"=> -46.4334286
            ],
            [
                "title"=> "UBS DR WALTER ELIAS CASA VERDE",
                "fone"=> "1120354034",
                "street_title"=> "RUA CRESCENTE",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "ITAIM PAULISTA",
                "latitude"=> -23.5093443,
                "longitude"=> -46.664241
            ],
            [
                "title"=> "UBS EDUARDO ROMANO RESCHILIAN",
                "fone"=> "1120358010",
                "street_title"=> "TRAVESSA ACUCENA DO BREJO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "PRIMEIRO DE OUTUBRO",
                "latitude"=> -23.638505,
                "longitude"=> -46.6060326
            ],
            [
                "title"=> "UBS ELISIO TEIXEIRA LEITE",
                "fone"=> "1120411744",
                "street_title"=> "RUA BARTOLOMEU SOARES",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "ALTO DA PONTE RASA",
                "latitude"=> -23.4465182,
                "longitude"=> -46.714503
            ],
            [
                "title"=> "UBS ENG GOULART JOSE PIRES",
                "fone"=> "1120434617",
                "street_title"=> "RUA BRENO ACCIOLI",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA SAO FRANCISCO",
                "latitude"=> -23.5185943,
                "longitude"=> -46.4922721
            ],
            [
                "title"=> "UBS ENG TRINDADE",
                "fone"=> "1120513962",
                "street_title"=> "RUA VITOR JOSE DE CASTRO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA SANTANA",
                "latitude"=> -23.5254142,
                "longitude"=> -46.4458273
            ],
            [
                "title"=> "UBS ERMELINO MATARAZZO",
                "fone"=> "1120515221",
                "street_title"=> "AVN MOACIR DANTAS DE ITAPICURU",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "SAO MIGUEL PAULISTA",
                "latitude"=> -23.488993,
                "longitude"=> -46.476532
            ],
            [
                "title"=> "UBS FAZENDA DA JUTA I",
                "fone"=> "1120526945",
                "street_title"=> "AVN AUGUSTO ANTUNES",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "LIMOEIRO",
                "latitude"=> -23.5207542,
                "longitude"=> -46.4638499
            ],
            [
                "title"=> "UBS FAZENDA DA JUTA II",
                "fone"=> "1120537174",
                "street_title"=> "RUA MARIA SANTANA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "SAO MIGUEL PAULISTA",
                "latitude"=> -23.5107912,
                "longitude"=> -46.45063409999999
            ],
            [
                "title"=> "UBS FAZENDA DO CARMO",
                "fone"=> "1120583722",
                "street_title"=> "RUA SERRA DA JURUOCA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "SAO MIGUEL PAULISTA",
                "latitude"=> -23.4876566,
                "longitude"=> -46.44915719999999
            ],
            [
                "title"=> "UBS FERROVIARIOS",
                "fone"=> "1120619030",
                "street_title"=> "RUA ALMIRANTE MARIATH",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "HELIOPOLIS",
                "latitude"=> -23.5977222,
                "longitude"=> -46.3915707
            ],
            [
                "title"=> "UBS GAIVOTAS",
                "fone"=> "1120635479",
                "street_title"=> "RUA ANTOMIO FREDERICO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA INDEPENDENCIA",
                "latitude"=> -23.7345569,
                "longitude"=> -46.6611558
            ],
            [
                "title"=> "UBS GLEBA DO PESSEGO",
                "fone"=> "1122014101",
                "street_title"=> "RUA FRANCISCO PEIXOTO BEZERRA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM BRASIL",
                "latitude"=> -23.4861546,
                "longitude"=> -46.5766856
            ],
            [
                "title"=> "UBS GRAFICOS",
                "fone"=> "1122017219",
                "street_title"=> "PCA CAMPINOPOLIS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA NIVI",
                "latitude"=> -23.4777755,
                "longitude"=> -46.5879396
            ],
            [
                "title"=> "UBS GUAIANASES II",
                "fone"=> "1122030063",
                "street_title"=> "RUA DR JOSE VICENTE",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "TREMEMBE",
                "latitude"=> -23.5455579,
                "longitude"=> -46.4155018
            ],
            [
                "title"=> "UBS GUAIANAZES",
                "fone"=> "1122049986",
                "street_title"=> "RUA ANTONIO JOAQUIM DE OLIVEIRA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA ESMERALDA",
                "latitude"=> -23.4491227,
                "longitude"=> -46.6103969
            ],
            [
                "title"=> "UBS HORIZONTE AZUL",
                "fone"=> "1122056003",
                "street_title"=> "RUA CATARINA LOPES",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "ITAQUERA",
                "latitude"=> -23.7469313,
                "longitude"=> -46.7778285
            ],
            [
                "title"=> "UBS HORTO FLORESTAL",
                "fone"=> "1122074797",
                "street_title"=> "AV ANGELINA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA LEONOR",
                "latitude"=> -23.4986387,
                "longitude"=> -46.5968382
            ],
            [
                "title"=> "UBS HUMAITA",
                "fone"=> "1122110884",
                "street_title"=> "RUA MIGUEL BASTOS SOARES",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM IVA",
                "latitude"=> -23.5569836,
                "longitude"=> -46.6420056
            ],
            [
                "title"=> "UBS IACAPE JD PLANALTO",
                "fone"=> "1122163746",
                "street_title"=> "RUA PLANALTO DE CONQUISTA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JD INDEPENDENCIA",
                "latitude"=> -23.6103246,
                "longitude"=> -46.5094815
            ],
            [
                "title"=> "UBS IGUACU",
                "fone"=> "1122168076",
                "street_title"=> "RUA PEDRO DE CASTRO VELHO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA BANCARIA",
                "latitude"=> -23.591855,
                "longitude"=> -46.51652319999999
            ],
            [
                "title"=> "UBS ILZA WELTMAN HUTZLER",
                "fone"=> "1122318619",
                "street_title"=> "RUA VALORBE",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "LAUZANE PAULISTA",
                "latitude"=> -23.4746574,
                "longitude"=> -46.6435986
            ],
            [
                "title"=> "UBS INACIO MONTEIRO",
                "fone"=> "1122400065",
                "street_title"=> "RUA SAO GERALDINO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "V CONSTANCIA",
                "latitude"=> -23.5763909,
                "longitude"=> -46.3917736
            ],
            [
                "title"=> "UBS ITAIM PAULISTA JULIO DE GOUVEIA",
                "fone"=> "1122405277",
                "street_title"=> "RUA HUM",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM APUANA",
                "latitude"=> -23.494482,
                "longitude"=> -46.4005343
            ],
            [
                "title"=> "UBS ITAQUERA",
                "fone"=> "1122417366",
                "street_title"=> "RUA ALPHEU LUIZ GASPARINI",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA NOVA GALVAO",
                "latitude"=> -23.4512904,
                "longitude"=> -46.5725569
            ],
            [
                "title"=> "UBS J ALFREDO",
                "fone"=> "1122532723",
                "street_title"=> "PERAMIRIM",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM DA CONQUISTA",
                "latitude"=> -23.6128554,
                "longitude"=> -46.453159
            ],
            [
                "title"=> "UBS J APUANA",
                "fone"=> "1122631569",
                "street_title"=> "AV JOAO DOS SANTOS ABREU",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA ESPANHOLA",
                "latitude"=> -23.4814166,
                "longitude"=> -46.6679263
            ],
            [
                "title"=> "UBS J ARACATI",
                "fone"=> "1122673796",
                "street_title"=> "RUA ANTONIO PICAROLLO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JD FONTALIS",
                "latitude"=> -23.4357535,
                "longitude"=> -46.5805963
            ],
            [
                "title"=> "UBS J AURORA",
                "fone"=> "1122725763",
                "street_title"=> "PCA CENTENARIO DE VILA PRUDENTE",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA PRUDENTE",
                "latitude"=> -23.5821133,
                "longitude"=> -46.5827001
            ],
            [
                "title"=> "UBS J BRASIL",
                "fone"=> "1122741059",
                "street_title"=> "RUA BELGRADO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "MOINHO VELHO",
                "latitude"=> -23.6123354,
                "longitude"=> -46.60379160000001
            ],
            [
                "title"=> "UBS J BRASILIA",
                "fone"=> "1122760149",
                "street_title"=> "RUA JACOPO BASSANO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA MONTE ALEGRE",
                "latitude"=> -23.6287908,
                "longitude"=> -46.64631370000001
            ],
            [
                "title"=> "UBS J CAICARA",
                "fone"=> "1122802373",
                "street_title"=> "RUA JAPANI",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "CIDADE AE CARVALHO",
                "latitude"=> -23.5393133,
                "longitude"=> -46.476159
            ],
            [
                "title"=> "UBS J CAMARGO NOVO",
                "fone"=> "1122806211",
                "street_title"=> "RUA NICOLO TARTAGLIA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM COIMBRA",
                "latitude"=> -23.5301733,
                "longitude"=> -46.4799501
            ],
            [
                "title"=> "UBS J CAMPOS",
                "fone"=> "1122822522",
                "street_title"=> "AVN DOS TEXTEIS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "CIDADE TIRADENTES",
                "latitude"=> -23.5885605,
                "longitude"=> -46.3964948
            ],
            [
                "title"=> "UBS J CAPELA",
                "fone"=> "1122825151",
                "street_title"=> "RUA DOS TEXTEIS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "CIDADE TIRADENTES",
                "latitude"=> -23.5885605,
                "longitude"=> -46.3964948
            ],
            [
                "title"=> "UBS J CASTRO ALVES",
                "fone"=> "1122826050",
                "street_title"=> "RUA LAJEDO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "CIDADE TIRADENTES",
                "latitude"=> -23.5873611,
                "longitude"=> -46.3867225
            ],
            [
                "title"=> "UBS J CELESTE",
                "fone"=> "1122856477",
                "street_title"=> "RUA GONCALVES NINA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "CIDADE TIRADENTES",
                "latitude"=> -23.5977222,
                "longitude"=> -46.3915707
            ],
            [
                "title"=> "UBS J COIMBRA",
                "fone"=> "1122860017",
                "street_title"=> "RUA IBIAJARA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "XV DE NOVEMBRO",
                "latitude"=> -23.5273652,
                "longitude"=> -46.4356029
            ],
            [
                "title"=> "UBS J COLONIAL",
                "fone"=> "1122948853",
                "street_title"=> "RUA ANTONIO LINDORO DA SILVA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA ARICANDUVA",
                "latitude"=> -23.5351095,
                "longitude"=> -46.5368708
            ],
            [
                "title"=> "UBS J COMERCIAL",
                "fone"=> "1122954163",
                "street_title"=> "PRACA HAROLDO DALTRO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA NOVA MANCHESTER",
                "latitude"=> -23.546588,
                "longitude"=> -46.5319789
            ],
            [
                "title"=> "UBS J COPA",
                "fone"=> "1122974782",
                "street_title"=> "RUA ADAO MANOEL DA SILVA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "SAO MIGUEL PAULISTA",
                "latitude"=> -23.4893634,
                "longitude"=> -46.4559571
            ],
            [
                "title"=> "UBS J DA CONQUISTA I",
                "fone"=> "1122975453",
                "street_title"=> "RUA JOSE BARGAS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JD PEDRO JOSE NUNES",
                "latitude"=> -23.5009956,
                "longitude"=> -46.4623725
            ],
            [
                "title"=> "UBS J DA CONQUISTA II",
                "fone"=> "1122979669",
                "street_title"=> "RUA FREI FIDELIS MOTA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "PRQ CRUZEIRO DO SUL",
                "latitude"=> -23.4923282,
                "longitude"=> -46.4622596
            ],
            [
                "title"=> "UBS J DA CONQUISTA III",
                "fone"=> "1123022347",
                "street_title"=> "RUA AYRES QUARESMA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA HELOISA",
                "latitude"=> -23.5896451,
                "longitude"=> -46.5348143
            ],
            [
                "title"=> "UBS J DA SAUDE NEUSA ROSALIA MORALES",
                "fone"=> "1123312131",
                "street_title"=> "RUA ATILIO SELVA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM CELESTE",
                "latitude"=> -23.6127322,
                "longitude"=> -46.6162809
            ],
            [
                "title"=> "UBS J DAS CAMELIAS",
                "fone"=> "1123319624",
                "street_title"=> "AV CARLOS LIVIERO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA LIVIERO",
                "latitude"=> -23.6448423,
                "longitude"=> -46.5931195
            ],
            [
                "title"=> "UBS J DAS LARANJEIRAS",
                "fone"=> "1123355319",
                "street_title"=> "RUA DR LAFAIETE S CAMARGO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JD CLIMAX",
                "latitude"=> -23.638505,
                "longitude"=> -46.6060326
            ],
            [
                "title"=> "UBS J DAS PALMAS",
                "fone"=> "1125132255",
                "street_title"=> "RUA PEDRO MEIRA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA CURUCA",
                "latitude"=> -23.5020436,
                "longitude"=> -46.4229626
            ],
            [
                "title"=> "UBS J ELEDY",
                "fone"=> "1125137109",
                "street_title"=> "RUA JOSE PESSOTA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "PRQ SANTA RITA",
                "latitude"=> -23.5117174,
                "longitude"=> -46.4153741
            ],
            [
                "title"=> "UBS J ELIANE",
                "fone"=> "1125152990",
                "street_title"=> "RUA NARCEJA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "SAO MIGUEL PAULISTA",
                "latitude"=> -23.5161128,
                "longitude"=> -46.4266133
            ],
            [
                "title"=> "UBS J ETELVINA",
                "fone"=> "1125182152",
                "street_title"=> "RUA FRANCISCO CARDOSO JUNIOR",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "CIDADE TIRADENTES",
                "latitude"=> -23.5650715,
                "longitude"=> -46.4182083
            ],
            [
                "title"=> "UBS J FANGANIELLO",
                "fone"=> "1125217615",
                "street_title"=> "RUA CARMEM CARDOSO BORDINI",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM HELIAN",
                "latitude"=> -23.5768148,
                "longitude"=> -46.4559349
            ],
            [
                "title"=> "UBS J FLOR DE MAIO",
                "fone"=> "1125245020",
                "street_title"=> "RUA PONTE DE LUCENA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "ITAQUERA",
                "latitude"=> -23.4311755,
                "longitude"=> -46.5750977
            ],
            [
                "title"=> "UBS J FONTALIS",
                "fone"=> "1125247796",
                "street_title"=> "RUA IPOPOCA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "ITAQUERA",
                "latitude"=> -23.5497482,
                "longitude"=> -46.44867869999999
            ],
            [
                "title"=> "UBS J GERMANIA",
                "fone"=> "1125440051",
                "street_title"=> "RUA ANTONIO FREITAS DE TOLEDO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "ERMELINO MATARAZZO",
                "latitude"=> -23.488993,
                "longitude"=> -46.476532
            ],
            [
                "title"=> "UBS J GUARANI",
                "fone"=> "1125451026",
                "street_title"=> "RUA ARRAIAL DE SANTA BARBARA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "CID PEDRO JOSE NUNES",
                "latitude"=> -23.5009956,
                "longitude"=> -46.4623725
            ],
            [
                "title"=> "UBS J GUARUJA",
                "fone"=> "1125456362",
                "street_title"=> "RUA BELEM SANTOS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA SILVIA",
                "latitude"=> -23.4916477,
                "longitude"=> -46.5045905
            ],
            [
                "title"=> "UBS J HELENA",
                "fone"=> "1125464111",
                "street_title"=> "RUA PAULO BIFANO ALVES",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "PARQUE BOTURUSSU",
                "latitude"=> -23.5031492,
                "longitude"=> -46.4840623
            ],
            [
                "title"=> "UBS J HELGA",
                "fone"=> "1125465100",
                "street_title"=> "RUA CACULE",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "ERMELINO MATARAZZO",
                "latitude"=> -23.6414076,
                "longitude"=> -46.7715618
            ],
            [
                "title"=> "UBS J HELIAN",
                "fone"=> "1125465867",
                "street_title"=> "AV SAO MIGUEL",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "PONTE RASA",
                "latitude"=> -23.5095558,
                "longitude"=> -46.4970669
            ],
            [
                "title"=> "UBS J HERCULANO",
                "fone"=> "1125466177",
                "street_title"=> "RUA LUCAS GONCALVES",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM KERALUX",
                "latitude"=> -23.4822423,
                "longitude"=> -46.4933173
            ],
            [
                "title"=> "UBS J ICARAI BRASILANDIA",
                "fone"=> "1125525355",
                "street_title"=> "RUA SILVIANOPOLIS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM SAO PEDRO",
                "latitude"=> -23.548515,
                "longitude"=> -46.4300158
            ],
            [
                "title"=> "UBS J ICARAI QUINTANA",
                "fone"=> "1125552676",
                "street_title"=> "RUA INACIO PINTO LIMA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "CIDADE TIRADENTES",
                "latitude"=> -22.0695288,
                "longitude"=> -50.3060827
            ],
            [
                "title"=> "UBS J INDAIA",
                "fone"=> "1125553894",
                "street_title"=> "EST MANOEL DE OLIVEIRA RAMOS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "CIDADE TIRADENTES",
                "latitude"=> -23.5804682,
                "longitude"=> -46.3829576
            ],
            [
                "title"=> "UBS J IV CENTENARIO",
                "fone"=> "1125556455",
                "street_title"=> "RUA INACIO MONTEIRO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "CIDADE TIRADENTES",
                "latitude"=> -23.5900703,
                "longitude"=> -46.4999829
            ],
            [
                "title"=> "UBS J IVA",
                "fone"=> "1125558583",
                "street_title"=> "RUA ALDEIA MARIA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "GUAIANASES",
                "latitude"=> -23.5605659,
                "longitude"=> -46.4233678
            ],
            [
                "title"=> "UBS J JAPAO",
                "fone"=> "1125575072",
                "street_title"=> "RUA FELICIANO DE MENDONCA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM SOARES",
                "latitude"=> 36.204824,
                "longitude"=> 138.252924
            ],
            [
                "title"=> "UBS J JOAMAR",
                "fone"=> "1125575136",
                "street_title"=> "AV PROF OSVALDO DE OLIVEIRA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JD HELENA",
                "latitude"=> -23.542697,
                "longitude"=> -46.4311328
            ],
            [
                "title"=> "UBS J KAGOHARA",
                "fone"=> "1125576698",
                "street_title"=> "RUA FRANCISCO NUNES CUBAS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "LAJEADO",
                "latitude"=> -23.5469315,
                "longitude"=> -46.3952439
            ],
            [
                "title"=> "UBS J KERALUX",
                "fone"=> "1125578177",
                "street_title"=> "RUA PROFESSOR COSME DEODATO TADEU",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "GUAIANASES",
                "latitude"=> -23.543505,
                "longitude"=> -46.4094338
            ],
            [
                "title"=> "UBS J LADEIRA ROSA",
                "fone"=> "1125579571",
                "street_title"=> "ESTRADA DO LAJEADO VELHO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "LAJEADO",
                "latitude"=> -23.5353577,
                "longitude"=> -46.4018349
            ],
            [
                "title"=> "UBS J LIDIA",
                "fone"=> "1125614680",
                "street_title"=> "EST DOM JOAO NERY",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM NAZARE",
                "latitude"=> -23.522594,
                "longitude"=> -46.3998053
            ],
            [
                "title"=> "UBS J LOURDES",
                "fone"=> "1125618076",
                "street_title"=> "RUA DOMINGUES VIDIGAL",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "CURUCA",
                "latitude"=> -23.504666,
                "longitude"=> -46.4102075
            ],
            [
                "title"=> "UBS J MACEDONIA",
                "fone"=> "1125623532",
                "street_title"=> "RUA COMANDANTE CARLOS RUHL",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "GUAIANASES",
                "latitude"=> -23.5455579,
                "longitude"=> -46.4155018
            ],
            [
                "title"=> "UBS J MAGDALENA",
                "fone"=> "1125623532",
                "street_title"=> "RUA CONJUNTO DA PAZ",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JD MIRIAM",
                "latitude"=> -23.5211896,
                "longitude"=> -46.38876459999999
            ],
            [
                "title"=> "UBS J MAIA",
                "fone"=> "1125675959",
                "street_title"=> "RUA BOIGUACU",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JAD CAMARGO NOVO",
                "latitude"=> -23.5039467,
                "longitude"=> -46.3856186
            ],
            [
                "title"=> "UBS J MARACA",
                "fone"=> "1125720367",
                "street_title"=> "RUA JOSE DA CRUZ CAMARGO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "ITAIM PAULISTA",
                "latitude"=> -23.4949963,
                "longitude"=> -46.38007229999999
            ],
            [
                "title"=> "UBS J MARILIA",
                "fone"=> "1125720672",
                "street_title"=> "PRC MAJOR JOSE LEVY SOBRINHO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "ITAIM PAULISTA",
                "latitude"=> -23.494482,
                "longitude"=> -46.4005343
            ],
            [
                "title"=> "UBS J MITSUTANI",
                "fone"=> "1125775273",
                "street_title"=> "RUA PACHECO ARANHA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "GUAIANASES",
                "latitude"=> -23.5423284,
                "longitude"=> -46.3927255
            ],
            [
                "title"=> "UBS J N SRA DO CARMO",
                "fone"=> "1125813737",
                "street_title"=> "RUA RIO MANOEL ALVES",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "ITAIM PAULISTA",
                "latitude"=> -23.4864765,
                "longitude"=> -46.39271309999999
            ],
            [
                "title"=> "UBS J NAKAMURA",
                "fone"=> "1125816962",
                "street_title"=> "RUA MARFIM VEGETAL",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM MAIA",
                "latitude"=> -23.4923149,
                "longitude"=> -46.4158811
            ],
            [
                "title"=> "UBS J NELIA",
                "fone"=> "1126055307",
                "street_title"=> "RUA SERRA DE JAIRE",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "AGUA RASA",
                "latitude"=> -23.5538329,
                "longitude"=> -46.5799418
            ],
            [
                "title"=> "UBS J NORDESTE",
                "fone"=> "1126217402",
                "street_title"=> "RUA AUGUSTO CORREA LEITE",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "ENGENHEIRO GOULART",
                "latitude"=> -23.5001716,
                "longitude"=> -46.51682659999999
            ],
            [
                "title"=> "UBS J NOVE DE JULHO",
                "fone"=> "1126415130",
                "street_title"=> "RUA MERCEDES LOPES",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA SANTANA",
                "latitude"=> -23.5138383,
                "longitude"=> -46.5320743
            ],
            [
                "title"=> "UBS J NOVO HORIZONTE",
                "fone"=> "1126416973",
                "street_title"=> "AVENIDA GABRIELA MISTRAL",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "PENHA DE FRANCA",
                "latitude"=> -23.5109607,
                "longitude"=> -46.5508184
            ],
            [
                "title"=> "UBS J OLINDA",
                "fone"=> "1126848466",
                "street_title"=> "RUA MARCONDES DE BRITO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA MATILDE",
                "latitude"=> -23.5367136,
                "longitude"=> -46.527624
            ],
            [
                "title"=> "UBS J PARAGUACU",
                "fone"=> "1126852124",
                "street_title"=> "RUA JOAO MARCHIORI",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA GUILHERMINA",
                "latitude"=> -23.5378431,
                "longitude"=> -46.5143798
            ],
            [
                "title"=> "UBS J PARANAPANEMA",
                "fone"=> "1126920788",
                "street_title"=> "RUA SAMPSON",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "BRAS",
                "latitude"=> -23.6934553,
                "longitude"=> -46.7805338
            ],
            [
                "title"=> "UBS J PAULISTANO",
                "fone"=> "1126920788",
                "street_title"=> "RUA REBELO DA SILVA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JAD S NICOLAU",
                "latitude"=> -23.5263781,
                "longitude"=> -46.4799583
            ],
            [
                "title"=> "UBS J PENHA",
                "fone"=> "1127017933",
                "street_title"=> "RUA POEMA DAS AMERICAS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JD DOS EUCALIPTOS",
                "latitude"=> -23.6010052,
                "longitude"=> -46.508021
            ],
            [
                "title"=> "UBS J POPULAR DR MATHEUS SANTAMARIA",
                "fone"=> "1127025889",
                "street_title"=> "RUA BOLEADEIRAS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "PQ STA MADALENA",
                "latitude"=> -23.5178091,
                "longitude"=> -46.5062691
            ],
            [
                "title"=> "UBS J ROBRU GUAIANAZES",
                "fone"=> "1127025904",
                "street_title"=> "RUA SAO JOSE DAS ESPINHARAS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA INDUSTRIAL",
                "latitude"=> -23.6038793,
                "longitude"=> -46.5324787
            ],
            [
                "title"=> "UBS J ROBRU ITAIM",
                "fone"=> "1127025919",
                "street_title"=> "RUA PALMEIRA BACABA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM ELBA",
                "latitude"=> -23.6154629,
                "longitude"=> -46.5105878
            ],
            [
                "title"=> "UBS J ROMANO",
                "fone"=> "1127025934",
                "street_title"=> "AV DO ORATORIO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA INDUSTRIAL",
                "latitude"=> -23.4864765,
                "longitude"=> -46.39271309999999
            ],
            [
                "title"=> "UBS J ROSELI",
                "fone"=> "1127025953",
                "street_title"=> "RUA ERVA IMPERIAL",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM GUAIRACA",
                "latitude"=> -23.5998816,
                "longitude"=> -46.5399624
            ],
            [
                "title"=> "UBS J SAO BENTO",
                "fone"=> "1127031198",
                "street_title"=> "RUA IACAPE",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "PQ STA MADALENA",
                "latitude"=> -23.6103246,
                "longitude"=> -46.5094815
            ],
            [
                "title"=> "UBS J SAO FRANCISCO",
                "fone"=> "1127060555",
                "street_title"=> "ESTRADA CASA GRANDE",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA UNIAO",
                "latitude"=> -23.6049132,
                "longitude"=> -46.5209358
            ],
            [
                "title"=> "UBS J SAO NICOLAU",
                "fone"=> "1127211815",
                "street_title"=> "AVENIDA DOS LATINOS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JD SANTA TEREZINHA",
                "latitude"=> -23.5294763,
                "longitude"=> -46.47639830000001
            ],
            [
                "title"=> "UBS J SAO PEDRO FRANCISCO ANTONIO CESARONI",
                "fone"=> "1127212707",
                "street_title"=> "R DR EDGARD MAGALHAES NORONHA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA NOVA YORK",
                "latitude"=> -23.5787744,
                "longitude"=> -46.5099656
            ],
            [
                "title"=> "UBS J SECKLER",
                "fone"=> "1127246146",
                "street_title"=> "RUA COSTEIRA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM ARISI",
                "latitude"=> -23.5601845,
                "longitude"=> -46.5101297
            ],
            [
                "title"=> "UBS J SILVA TELLES",
                "fone"=> "1127247004",
                "street_title"=> "RUA EMBIRATAI",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JD SANTA MARIA",
                "latitude"=> -23.5001752,
                "longitude"=> -46.40876979999999
            ],
            [
                "title"=> "UBS J SOARES",
                "fone"=> "1127260174",
                "street_title"=> "CAMPO FLORIDO RUA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JD IV CENTENARIO",
                "latitude"=> -23.5900703,
                "longitude"=> -46.4999829
            ],
            [
                "title"=> "UBS J SOUZA",
                "fone"=> "1127270484",
                "street_title"=> "TAIOBERA R",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM PARAGUACU",
                "latitude"=> -23.5959077,
                "longitude"=> -46.5002225
            ],
            [
                "title"=> "UBS J STA MARGARIDA",
                "fone"=> "1127315143",
                "street_title"=> "SIMAO NUNES RUA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM ROSELI",
                "latitude"=> -23.5971737,
                "longitude"=> -46.4395208
            ],
            [
                "title"=> "UBS J STA MARIA",
                "fone"=> "1127317252",
                "street_title"=> "BENTO GUELFI AV",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JD DAS LARANJEIRAS",
                "latitude"=> -23.6038004,
                "longitude"=> -46.4300193
            ],
            [
                "title"=> "UBS J STA TEREZINHA",
                "fone"=> "1127343271",
                "street_title"=> "R PEDRO RAMAZZANI",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "RECANTO VERDE SOL",
                "latitude"=> -23.6146956,
                "longitude"=> -46.4190624
            ],
            [
                "title"=> "UBS J THOMAS",
                "fone"=> "1127353079",
                "street_title"=> "TRAV SOMOS TODOS IGUAIS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM DA CONQUISTA",
                "latitude"=> -23.6070013,
                "longitude"=> -46.44460549999999
            ],
            [
                "title"=> "UBS J TIETE I",
                "fone"=> "1127362573",
                "street_title"=> "RUA PONTE DA AMIZADE",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM PALANQUE",
                "latitude"=> -23.5950974,
                "longitude"=> -46.4206183
            ],
            [
                "title"=> "UBS J TRES CORACOES",
                "fone"=> "1127367092",
                "street_title"=> "TV AV SOMOS TODOS IGUAIS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JD DA CONQUISTA",
                "latitude"=> -23.6070013,
                "longitude"=> -46.44460549999999
            ],
            [
                "title"=> "UBS J TRES MARIAS MAURICIO ZAMIJOVSKY",
                "fone"=> "1127411563",
                "street_title"=> "AVN DR FRANCISCO MUNHOZ FILHO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "CIDADE LIDER",
                "latitude"=> -23.5582163,
                "longitude"=> -46.4667841
            ],
            [
                "title"=> "UBS J VALQUIRIA",
                "fone"=> "1127416938",
                "street_title"=> "AVN OSVALDO DO VALLE CORDEIRO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JD BRASILIA",
                "latitude"=> -23.5548787,
                "longitude"=> -46.4919856
            ],
            [
                "title"=> "UBS J VERA CRUZ",
                "fone"=> "1127439526",
                "street_title"=> "RUA ELZA DOS ANJOS NEVES",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JD MARILIA",
                "latitude"=> -23.5665396,
                "longitude"=> -46.4934658
            ],
            [
                "title"=> "UBS J VERA CRUZ PERDIZES",
                "fone"=> "1127480646",
                "street_title"=> "RUA VERISSIMO DA SILVA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JD N SRA DO CARMO",
                "latitude"=> -23.5900703,
                "longitude"=> -46.4999829
            ],
            [
                "title"=> "UBS J VISTA ALEGRE",
                "fone"=> "1127493521",
                "street_title"=> "RUA COELHO DE CASTRO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA NHOCUNE",
                "latitude"=> -23.5473595,
                "longitude"=> -46.5003711
            ],
            [
                "title"=> "UBS J VITORIA",
                "fone"=> "1127494235",
                "street_title"=> "RUA HENRIQUE JACOB",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "ARTHUR ALVIM",
                "latitude"=> -23.5377025,
                "longitude"=> -46.4918894
            ],
            [
                "title"=> "UBS JACANA",
                "fone"=> "1127514339",
                "street_title"=> "MIGUEL FERREIRA DE MELO R",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM SANTO ANDRE",
                "latitude"=> -23.630282,
                "longitude"=> -46.443644
            ],
            [
                "title"=> "UBS JARAGUA",
                "fone"=> "1127516712",
                "street_title"=> "BANDEIRA DE ARACAMBI R",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "J RODOLFO PIRANI",
                "latitude"=> -23.6313192,
                "longitude"=> -46.4580176
            ],
            [
                "title"=> "UBS JARDIM BANDEIRANTES",
                "fone"=> "1127540622",
                "street_title"=> "MARCOS GONCALVES CORREA RUA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "J VL CARRAO",
                "latitude"=> -23.6168982,
                "longitude"=> -46.456937
            ],
            [
                "title"=> "UBS JARDIM CAMPINAS",
                "fone"=> "1127542382",
                "street_title"=> "JOSE DE ARAUJO VIEIRA R",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JAD RODOLFO PIRANI",
                "latitude"=> -23.771628,
                "longitude"=> -46.7126652
            ],
            [
                "title"=> "UBS JARDIM CIDADE PIRITUBA",
                "fone"=> "1127572808",
                "street_title"=> "RUA JURITI PIRANGA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "CANGAIBA",
                "latitude"=> -23.4975132,
                "longitude"=> -46.530339
            ],
            [
                "title"=> "UBS JARDIM DAS FONTES",
                "fone"=> "1127915322",
                "street_title"=> "RUA COREMU",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA ESPERANCA",
                "latitude"=> -23.5234676,
                "longitude"=> -46.5263225
            ],
            [
                "title"=> "UBS JARDIM DAS PEDRAS",
                "fone"=> "1128855685",
                "street_title"=> "RUA FRANCISCO JOSE VIANA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "CIDADE TIRADENTES",
                "latitude"=> -23.6086883,
                "longitude"=> -46.398941
            ],
            [
                "title"=> "UBS JARDIM DOS EUCALIPTOS HELIO MOREIRA SALLES",
                "fone"=> "1129100235",
                "street_title"=> "RUA NOSSA SENHORA DAS DORES",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA FORMOSA",
                "latitude"=> -23.6010052,
                "longitude"=> -46.508021
            ],
            [
                "title"=> "UBS JARDIM ELBA HUMBERTO GASTAO BODRA",
                "fone"=> "1129102958",
                "street_title"=> "RUA TERESINHA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JD MARINGA",
                "latitude"=> -23.6212232,
                "longitude"=> -46.5092039
            ],
            [
                "title"=> "UBS JARDIM EMBURA",
                "fone"=> "1129158200",
                "street_title"=> "ESTRADA DAS LAGRIMAS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "SACOMA",
                "latitude"=> -23.613783,
                "longitude"=> -46.59370149999999
            ],
            [
                "title"=> "UBS JARDIM GRIMALDI",
                "fone"=> "1129170136",
                "street_title"=> "RUA PARAMU",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA ALPINA",
                "latitude"=> -23.591855,
                "longitude"=> -46.51652319999999
            ],
            [
                "title"=> "UBS JARDIM GUAIRACA",
                "fone"=> "1129171117",
                "street_title"=> "PRACA CONDE DE JANUARIO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA CALIFORNIA",
                "latitude"=> -23.6050911,
                "longitude"=> -46.5550486
            ],
            [
                "title"=> "UBS JARDIM INDEPENDENCIA HERMENEGILDO MORBIM JUNIOR",
                "fone"=> "1129183595",
                "street_title"=> "RUA DR NOGUEIRA DE NORONHA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "PQ SAO LUCAS",
                "latitude"=> -23.5886846,
                "longitude"=> -46.5602886
            ],
            [
                "title"=> "UBS JARDIM IPANEMA",
                "fone"=> "1129190200",
                "street_title"=> "SIBALDO LINS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "SAO MATEUS",
                "latitude"=> -23.5921271,
                "longitude"=> -46.4848297
            ],
            [
                "title"=> "UBS JARDIM IPORA",
                "fone"=> "1129191120",
                "street_title"=> "RUA SARGENTO EDGAR LOURENCO PINTO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "C MASCARENHAS MORAES",
                "latitude"=> -23.6166897,
                "longitude"=> -46.4957512
            ],
            [
                "title"=> "UBS JARDIM LAPENNA",
                "fone"=> "1129541747",
                "street_title"=> "RUA BENEDITA DORNELAS CLARO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "PARQUE NOVO MUNDO",
                "latitude"=> -23.5178197,
                "longitude"=> -46.5768867
            ],
            [
                "title"=> "UBS JARDIM PANAMERICANO",
                "fone"=> "1129544580",
                "street_title"=> "RUA SOLDADO ANTONIO MATIAS DE CAMARGO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "PARQUE NOVO MUNDO",
                "latitude"=> -23.4526601,
                "longitude"=> -46.7309598
            ],
            [
                "title"=> "UBS JARDIM ROSINHA",
                "fone"=> "1129566923",
                "street_title"=> "RUA CUMARU",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "SAO MIGUEL PAULISTA",
                "latitude"=> -23.4388165,
                "longitude"=> -46.8019543
            ],
            [
                "title"=> "UBS JARDIM SANTA FE",
                "fone"=> "1129581488",
                "street_title"=> "RUA FREDERICO BROTERO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "CIDADE PATRIARCA",
                "latitude"=> -23.8320275,
                "longitude"=> -46.7110247
            ],
            [
                "title"=> "UBS JARDIM SANTO ANDRE",
                "fone"=> "1129582981",
                "street_title"=> "RUA LUIS ASSON",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "V MONTEVIDEO",
                "latitude"=> -23.512776,
                "longitude"=> -46.5138867
            ],
            [
                "title"=> "UBS JARDIM SAO NORBERTO",
                "fone"=> "1129631436",
                "street_title"=> "RUA ILHA DO ARVOREDO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VL MORGADOURO",
                "latitude"=> -23.8176066,
                "longitude"=> -46.705345
            ],
            [
                "title"=> "UBS JARDIM SAPOPEMBA",
                "fone"=> "1129632346",
                "street_title"=> "AVN KEMES ADDAS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "CIDADE KEMEL",
                "latitude"=> -23.504318,
                "longitude"=> -46.3669824
            ],
            [
                "title"=> "UBS JARDIM SELMA CIDADE ADEMAR",
                "fone"=> "1129633711",
                "street_title"=> "RUA ANTONIO LEME DA GUERRA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "ITAIM PAULISTA",
                "latitude"=> -23.6902736,
                "longitude"=> -46.6570954
            ],
            [
                "title"=> "UBS JARDIM SILVEIRA",
                "fone"=> "1129633802",
                "street_title"=> "RUA ITAJUIBE",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "ITAIM PAULISTA",
                "latitude"=> -23.5115858,
                "longitude"=> -46.3844044
            ],
            [
                "title"=> "UBS JARDIM SINHA",
                "fone"=> "1129648826",
                "street_title"=> "RUA EDUARDO REUTER",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "COHAB BARRO BRANCO I",
                "latitude"=> -23.5837702,
                "longitude"=> -46.3930573
            ],
            [
                "title"=> "UBS JD AEROPORTO DR MASSAKI UDIHARA",
                "fone"=> "1129730733",
                "street_title"=> "AVN BRAZ LEME",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "SANTANA",
                "latitude"=> -23.6336565,
                "longitude"=> -46.6644988
            ],
            [
                "title"=> "UBS JD APURA",
                "fone"=> "1129815578",
                "street_title"=> "RUA EURICO SODRE",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA MEDEIROS",
                "latitude"=> -23.4925332,
                "longitude"=> -46.5851042
            ],
            [
                "title"=> "UBS JD BOA VISTA",
                "fone"=> "1129894368",
                "street_title"=> "RUA PADRE MARCOS SIMONI",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA EDE",
                "latitude"=> -23.4906455,
                "longitude"=> -46.5914837
            ],
            [
                "title"=> "UBS JD DAS OLIVEIRAS",
                "fone"=> "1129915398",
                "street_title"=> "RUA AIMOPOLO LOBO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM JOAMAR",
                "latitude"=> -23.4492598,
                "longitude"=> -46.596125
            ],
            [
                "title"=> "UBS JD JAQUELINE",
                "fone"=> "1129956006",
                "street_title"=> "AV NOVA PAULISTA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "J FLOR DE MAIO",
                "latitude"=> -23.4311755,
                "longitude"=> -46.5750977
            ],
            [
                "title"=> "UBS JD MIRNA",
                "fone"=> "1130227074",
                "street_title"=> "AVN QUEIROZ FILHO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "V LE0POLDINA",
                "latitude"=> -23.7715593,
                "longitude"=> -46.6962993
            ],
            [
                "title"=> "UBS JD NITEROI",
                "fone"=> "1130329891",
                "street_title"=> "RUA PURPURINA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA MADALENA",
                "latitude"=> -23.5537496,
                "longitude"=> -46.6910563
            ],
            [
                "title"=> "UBS JD PERI",
                "fone"=> "1131010812",
                "street_title"=> "PRACA DA BANDEIRA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "REPUBLICA",
                "latitude"=> -23.4643478,
                "longitude"=> -46.6545938
            ],
            [
                "title"=> "UBS JD SAO CARLOS CIDADE ADEMAR",
                "fone"=> "1131012344",
                "street_title"=> "RUA FREDERICO ALVARENGA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "PARQUE DOM PEDRO",
                "latitude"=> -23.682487,
                "longitude"=> -46.6579594
            ],
            [
                "title"=> "UBS JD SAO JORGE",
                "fone"=> "1131686571",
                "street_title"=> "RUA SALVADOR CARDOSO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "ITAIM BIBI",
                "latitude"=> -23.5874307,
                "longitude"=> -46.68525169999999
            ],
            [
                "title"=> "UBS JD UMUARAMA",
                "fone"=> "1132220619",
                "street_title"=> "R TENENTE PENA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "BOM RETIRO",
                "latitude"=> -23.6620771,
                "longitude"=> -46.6696664
            ],
            [
                "title"=> "UBS JOAQUIM ANTONIO EIRADO",
                "fone"=> "1132411632",
                "street_title"=> "RUA HUMAITA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "BELA VISTA",
                "latitude"=> -23.5037281,
                "longitude"=> -46.6313287
            ],
            [
                "title"=> "UBS JOAQUIM ROSSINE",
                "fone"=> "1132610818",
                "street_title"=> "RUA ALMIR DEHAR",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "BRASILANDIA",
                "latitude"=> -23.4647743,
                "longitude"=> -46.6983561
            ],
            [
                "title"=> "UBS JORDANOPOLIS",
                "fone"=> "1132766480",
                "street_title"=> "AVN LACERDA FRANCO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "CAMBUCI",
                "latitude"=> -23.5696198,
                "longitude"=> -46.6235888
            ],
            [
                "title"=> "UBS JOSE BONIFACIO I",
                "fone"=> "1132770081",
                "street_title"=> "RUA DAS OLARIAS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "CANINDE",
                "latitude"=> -23.5537467,
                "longitude"=> -46.4446892
            ],
            [
                "title"=> "UBS JOSE BONIFACIO II",
                "fone"=> "1132844601",
                "street_title"=> "RUA ALM MARQUES LEAO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "BELA VISTA",
                "latitude"=> -23.5609011,
                "longitude"=> -46.6489566
            ],
            [
                "title"=> "UBS JOSE BONIFACIO III DRA LUCY MAYUMI UDAKIRI",
                "fone"=> "1133921281",
                "street_title"=> "RUA BORACEA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "SANTA CECILIA",
                "latitude"=> -23.540886,
                "longitude"=> -46.439532
            ],
            [
                "title"=> "UBS JOSE DE BARROS MAGALDI",
                "fone"=> "1134755208",
                "street_title"=> "RUA ANDRE DA FONSECA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA MUNHOZ",
                "latitude"=> -23.6641251,
                "longitude"=> -46.7371379
            ],
            [
                "title"=> "UBS JOSE MARCILIO MALTA CARDOSO",
                "fone"=> "1134758800",
                "street_title"=> "RUA JOSE PEREIRA JORGE",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "CARANDIRU",
                "latitude"=> -23.572737,
                "longitude"=> -46.7611375
            ],
            [
                "title"=> "UBS LARANJEIRAS",
                "fone"=> "1134787250",
                "street_title"=> "RUA FRANCISCO FRANCO MACHADO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA SABRINA",
                "latitude"=> -23.4925308,
                "longitude"=> -46.5730975
            ],
            [
                "title"=> "UBS LAUZANE PAULISTA",
                "fone"=> "1135012839",
                "street_title"=> "RUA PASQUALE GUALUPPI",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "PARAISOPOLIS",
                "latitude"=> -23.612536,
                "longitude"=> -46.7261477
            ],
            [
                "title"=> "UBS LUAR DO SERTAO",
                "fone"=> "1136214508",
                "street_title"=> "PCA CAMILO CASTELO BRANCO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA PIAUI",
                "latitude"=> -23.5007634,
                "longitude"=> -46.7569968
            ],
            [
                "title"=> "UBS LUIZ ERNESTO MAZZONI",
                "fone"=> "1136738552",
                "street_title"=> "R SARAMENHA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "PERDIZES",
                "latitude"=> -23.6448423,
                "longitude"=> -46.5931195
            ],
            [
                "title"=> "UBS MANUEL JOAQUIM PERA",
                "fone"=> "1136738552",
                "street_title"=> "RUA TURIASSU",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA POMPEIA",
                "latitude"=> -23.5537496,
                "longitude"=> -46.6910563
            ],
            [
                "title"=> "UBS MAR PAULISTA",
                "fone"=> "1136761399",
                "street_title"=> "RUA CATAO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA ROMANA",
                "latitude"=> -23.6965274,
                "longitude"=> -46.6629361
            ],
            [
                "title"=> "UBS MARSILAC",
                "fone"=> "1137216406",
                "street_title"=> "RUA LADISLAU ROMAN",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "CAXINGUI",
                "latitude"=> -23.5791109,
                "longitude"=> -46.7149382
            ],
            [
                "title"=> "UBS MASCARENHAS DE MORAES",
                "fone"=> "1137429041",
                "street_title"=> "RUA ANDRE DE ANDRADE",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA PRAIA",
                "latitude"=> -23.6174452,
                "longitude"=> -46.7457553
            ],
            [
                "title"=> "UBS MASSAGISTA MARIO AMERICO",
                "fone"=> "1137429844",
                "street_title"=> "RUA ABRAO CALIL REZEK",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA SONIA",
                "latitude"=> -23.5980987,
                "longitude"=> -46.7346724
            ],
            [
                "title"=> "UBS MATA VIRGEM",
                "fone"=> "1137511665",
                "street_title"=> "RUA BONIFACIO VERONESE",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM JAQUELINE",
                "latitude"=> -23.7145102,
                "longitude"=> -46.6275282
            ],
            [
                "title"=> "UBS MENINOPOLIS MARIO FRANCISCO NAPOLITANO",
                "fone"=> "1137563264",
                "street_title"=> "RUA TANTAS PALAVRAS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "CIDADE A E CARVALHO",
                "latitude"=> -23.6206731,
                "longitude"=> -46.68792879999999
            ],
            [
                "title"=> "UBS MILTON SANTOS",
                "fone"=> "1137582329",
                "street_title"=> "RUA BARAO MELGACO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "REAL PARQUE",
                "latitude"=> -23.6099239,
                "longitude"=> -46.705592
            ],
            [
                "title"=> "UBS MOINHO VELHO",
                "fone"=> "1137681527",
                "street_title"=> "RUA SATALIEL DE CAMPOS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JAGUARE",
                "latitude"=> -23.5431831,
                "longitude"=> -46.7444491
            ],
            [
                "title"=> "UBS MOINHO VELHO II",
                "fone"=> "1137681569",
                "street_title"=> "RUA DR BERNARDO GUERTZENSTEIN",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "RIO PEQUENO",
                "latitude"=> -23.572737,
                "longitude"=> -46.7611375
            ],
            [
                "title"=> "UBS MOOCA I",
                "fone"=> "1137689460",
                "street_title"=> "RUA JOAO LUIZ MATHEUS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "RIO PEQUENO",
                "latitude"=> -23.5627129,
                "longitude"=> -46.7516434
            ],
            [
                "title"=> "UBS MORADA DO SOL",
                "fone"=> "1137813817",
                "street_title"=> "AV ANGELO APARECIDO DOS SANTOS DIAS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM SAO JORGE",
                "latitude"=> -23.5926537,
                "longitude"=> -46.78364029999999
            ],
            [
                "title"=> "UBS MORRO DOCE",
                "fone"=> "1137820838",
                "street_title"=> "AV VATICANO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JD JOAO XXIII",
                "latitude"=> -23.4393856,
                "longitude"=> -46.7941145
            ],
            [
                "title"=> "UBS NIR J MARCELO",
                "fone"=> "1137824739",
                "street_title"=> "RUA JACINTO DE MORAIS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM CLAUDIA",
                "latitude"=> -23.5872032,
                "longitude"=> -46.770333
            ],
            [
                "title"=> "UBS NOSSA S DO BRASIL ARMANDO DARIENZO",
                "fone"=> "1137828380",
                "street_title"=> "RUA CANDIDO FONTOURA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM BOA VISTA",
                "latitude"=> -23.5841455,
                "longitude"=> -46.791897
            ],
            [
                "title"=> "UBS NOVA AMERICA",
                "fone"=> "1138260096",
                "street_title"=> "RUA VITORINO CARMILO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "SANTA CECILIA",
                "latitude"=> -23.5320846,
                "longitude"=> -46.6523928
            ],
            [
                "title"=> "UBS NOVO CAMINHO",
                "fone"=> "1138327587",
                "street_title"=> "RUA DA LIGACAO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "CHACARA INGLESA",
                "latitude"=> -23.4937328,
                "longitude"=> -46.72524079999999
            ],
            [
                "title"=> "UBS NOVO JARDIM I",
                "fone"=> "1138352063",
                "street_title"=> "RUA PRESIDENTE VARGAS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "PERUS",
                "latitude"=> -23.3999249,
                "longitude"=> -46.7510521
            ],
            [
                "title"=> "UBS PADRE JOSE DE ANCHIETA",
                "fone"=> "1138361146",
                "street_title"=> "RUA BARTOLOMEU PAES",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA ANASTACIO",
                "latitude"=> -23.5524381,
                "longitude"=> -46.4875654
            ],
            [
                "title"=> "UBS PARADA XV DE NOVEMBRO",
                "fone"=> "1138368029",
                "street_title"=> "RUA BERGSON",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VL LEOPOLDINA",
                "latitude"=> -23.5316121,
                "longitude"=> -46.7205586
            ],
            [
                "title"=> "UBS PARAISOPOLIS",
                "fone"=> "1138425146",
                "street_title"=> "RUA JACQUES FELIX",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA NOVA CONCEICAO",
                "latitude"=> -23.5926128,
                "longitude"=> -46.6713283
            ],
            [
                "title"=> "UBS PARAISOPOLIS II",
                "fone"=> "1138518201",
                "street_title"=> "RUA URUPEVA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA PENTEADO",
                "latitude"=> -23.612536,
                "longitude"=> -46.7261477
            ],
            [
                "title"=> "UBS PARAISOPOLIS III",
                "fone"=> "1138518235",
                "street_title"=> "RUA JOAQUINA MARIA DOS SANTOS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA RICA",
                "latitude"=> -23.6167669,
                "longitude"=> -46.7201814
            ],
            [
                "title"=> "UBS PARELHEIROS",
                "fone"=> "1138518818",
                "street_title"=> "RUA JOSE DA COSTA GAVIAO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "BRASILANDIA",
                "latitude"=> -23.8269519,
                "longitude"=> -46.7270204
            ],
            [
                "title"=> "UBS PARI",
                "fone"=> "1138569908",
                "street_title"=> "RUA VICHY",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "CASA VERDE BAIXA",
                "latitude"=> -23.5043883,
                "longitude"=> -46.6602288
            ],
            [
                "title"=> "UBS PARQUE MARIA DOMITILA",
                "fone"=> "1138588592",
                "street_title"=> "RUA LAVINIO SALES ARCURI",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "CASA VERDE ALTA",
                "latitude"=> -23.4893061,
                "longitude"=> -46.664612
            ],
            [
                "title"=> "UBS PARQUE MARIA HELENA",
                "fone"=> "1138588593",
                "street_title"=> "RUA MOURAO VIEIRA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "CASA VERDE BAIXA",
                "latitude"=> -23.663138,
                "longitude"=> -46.76231629999999
            ],
            [
                "title"=> "UBS PARQUE PAULISTANO",
                "fone"=> "1138624102",
                "street_title"=> "RUA VESPASIANO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA ROMANA",
                "latitude"=> -23.4871337,
                "longitude"=> -46.4322386
            ],
            [
                "title"=> "UBS PARQUE PERUCHE",
                "fone"=> "1139035572",
                "street_title"=> "AVN DO ANASTACIO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "PQ MARIA DOMITILA",
                "latitude"=> -23.4886143,
                "longitude"=> -46.7380436
            ],
            [
                "title"=> "UBS PASTORAL",
                "fone"=> "1139040016",
                "street_title"=> "RUA FABIO DE ALMEDIA MAGALHAES",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JD SANTO ELIAS",
                "latitude"=> -23.5734657,
                "longitude"=> -46.5379108
            ],
            [
                "title"=> "UBS PAULO FELDMAN NITRO OPERARIA",
                "fone"=> "1139041292",
                "street_title"=> "RUA JOAQUIM OLIVEIRA FREITAS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "SAO DOMINGOS",
                "latitude"=> -23.4927302,
                "longitude"=> -46.43510029999999
            ],
            [
                "title"=> "UBS PAULO VI",
                "fone"=> "1139042509",
                "street_title"=> "RUA RIBEIRAO VERMELHO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA PIRITUBA",
                "latitude"=> -23.4824479,
                "longitude"=> -46.746972
            ],
            [
                "title"=> "UBS PEDRO DE SOUZA CAMPOS",
                "fone"=> "1139111756",
                "street_title"=> "R ALBERTO CALIX",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "MORRO DOCE",
                "latitude"=> -23.5031492,
                "longitude"=> -46.4840623
            ],
            [
                "title"=> "UBS PERUS",
                "fone"=> "1139156452",
                "street_title"=> "RUA PAVAO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "PERUS",
                "latitude"=> -23.6020797,
                "longitude"=> -46.67385549999999
            ],
            [
                "title"=> "UBS PQ ARARIBA CEO II LRPD",
                "fone"=> "1139164100",
                "street_title"=> "R DALVA DE OLIVEIRA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "MORRO DOCE",
                "latitude"=> -23.6422923,
                "longitude"=> -46.7514763
            ],
            [
                "title"=> "UBS PQ ARTHUR ALVIM",
                "fone"=> "1139176245",
                "street_title"=> "PCA VIGARIO JOAO GONCALVES LIMA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "PERUS",
                "latitude"=> -23.5487514,
                "longitude"=> -46.4771951
            ],
            [
                "title"=> "UBS PQ BOA ESPERANCA",
                "fone"=> "1139211078",
                "street_title"=> "RUA EUVALDO AUGUSTO FREIRE",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "BRASILANDIA",
                "latitude"=> -23.6015256,
                "longitude"=> -46.4533908
            ],
            [
                "title"=> "UBS PQ BRISTOL",
                "fone"=> "1139213830",
                "street_title"=> "RUA PARAPUA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "BRASILANDIA",
                "latitude"=> -23.6492784,
                "longitude"=> -46.610417
            ],
            [
                "title"=> "UBS PQ DA LAPA",
                "fone"=> "1139216385",
                "street_title"=> "RUA SANTANA DO ARACUAI",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "BRASILANDIA",
                "latitude"=> -23.4620757,
                "longitude"=> -46.6907021
            ],
            [
                "title"=> "UBS PQ DO LAGO",
                "fone"=> "1139240208",
                "street_title"=> "RUA FERRAZ DE VASCONCELOS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "PARQUE MANDI",
                "latitude"=> -23.4824475,
                "longitude"=> -46.6780218
            ],
            [
                "title"=> "UBS PQ DOROTEIA",
                "fone"=> "1139242591",
                "street_title"=> "RUA DOMINGOS FRANCISCO DE MEDEIROS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA TEREZINHA",
                "latitude"=> -23.4586975,
                "longitude"=> -46.6965882
            ],
            [
                "title"=> "UBS PQ EDU CHAVES",
                "fone"=> "1139281270",
                "street_title"=> "RUA BARRA DA FORQUILHA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM PANAMERICANO",
                "latitude"=> -23.4518799,
                "longitude"=> -46.730604
            ],
            [
                "title"=> "UBS PQ ENGENHO II",
                "fone"=> "1139455958",
                "street_title"=> "ALAMEDA DAS LIMEIRAS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "ALPES DO JARAGUA",
                "latitude"=> -23.4467938,
                "longitude"=> -46.753799
            ],
            [
                "title"=> "UBS PQ FERNANDA",
                "fone"=> "1139472690",
                "street_title"=> "ESTRADA DE TAIPAS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARAGUA",
                "latitude"=> -23.4542912,
                "longitude"=> -46.7376831
            ],
            [
                "title"=> "UBS PQ FIGUEIRA GRANDE",
                "fone"=> "1139651686",
                "street_title"=> "RUA PRF DARIO RIBEIRO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "LIMAO",
                "latitude"=> -23.6820149,
                "longitude"=> -46.74905829999999
            ],
            [
                "title"=> "UBS PQ IMPERIAL MANOEL ANTONIO DA SILVA SARAGOCA",
                "fone"=> "1139661302",
                "street_title"=> "AVENIDA MANDAQUI",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "LIMAO",
                "latitude"=> -23.6287908,
                "longitude"=> -46.64631370000001
            ],
            [
                "title"=> "UBS PQ NOVO MUNDO I",
                "fone"=> "1139712432",
                "street_title"=> "AV ELISIO TEIXEIRA LEITE",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "PARADA DE TAIPAS",
                "latitude"=> -23.4356435,
                "longitude"=> -46.7180836
            ],
            [
                "title"=> "UBS PQ NOVO MUNDO II",
                "fone"=> "1139720333",
                "street_title"=> "RUA JOAO AMADO COUTINHO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARAGUA",
                "latitude"=> -23.5119745,
                "longitude"=> -46.5658755
            ],
            [
                "title"=> "UBS PQ NOVO STO AMARO",
                "fone"=> "1139721435",
                "street_title"=> "RUA CONDE MONTERONE",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "PIRITUBA",
                "latitude"=> -23.6990141,
                "longitude"=> -46.78081580000001
            ],
            [
                "title"=> "UBS PQ REGINA",
                "fone"=> "1139722916",
                "street_title"=> "RUA ENCRUZILHADA DO SUL",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JAD PAULISTANO",
                "latitude"=> -23.4614804,
                "longitude"=> -46.7090518
            ],
            [
                "title"=> "UBS PQ RESIDENCIAL COCAIA INDEPENDENTE",
                "fone"=> "1139743829",
                "street_title"=> "RUA POVOADO DO RIO NOVO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "PIRITUBA",
                "latitude"=> -23.7449871,
                "longitude"=> -46.6688972
            ],
            [
                "title"=> "UBS PQ SAO LUCAS",
                "fone"=> "1139748210",
                "street_title"=> "RUA COMENDADOR FEIZ ZARZUR",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM CIDADE PIRITU",
                "latitude"=> -23.5962688,
                "longitude"=> -46.5462835
            ],
            [
                "title"=> "UBS PQ SAO RAFAEL",
                "fone"=> "1139755331",
                "street_title"=> "RUA PADRE FELICIANO DOMINGUES",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "FREGUESIA DO O",
                "latitude"=> -23.6261545,
                "longitude"=> -46.4708103
            ],
            [
                "title"=> "UBS PQ STA RITA",
                "fone"=> "1139760514",
                "street_title"=> "RUA MONSENHOR MANOEL GOMES",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA ZATT",
                "latitude"=> -23.476988,
                "longitude"=> -46.71167579999999
            ],
            [
                "title"=> "UBS PQ STO ANTONIO CEO I",
                "fone"=> "1139767601",
                "street_title"=> "PRACA DOMINGOS COELHO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "PIRITUBA",
                "latitude"=> -23.6636433,
                "longitude"=> -46.7537809
            ],
            [
                "title"=> "UBS PREF CELSO AUGUSTO DANIEL",
                "fone"=> "1139823777",
                "street_title"=> "RUA IBIRAIARAS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "BRASILANDIA",
                "latitude"=> -23.5634542,
                "longitude"=> -46.3967268
            ],
            [
                "title"=> "UBS PREFEITO PRESTES MAIA",
                "fone"=> "1139849329",
                "street_title"=> "RUA CHEN FERRAZ FALCAO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA DIONISIA",
                "latitude"=> -23.4650738,
                "longitude"=> -46.661776
            ],
            [
                "title"=> "UBS PRIMEIRO DE OUTUBRO",
                "fone"=> "1150114278",
                "street_title"=> "R ARTUR MENDES DA SILVA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA CAMPESTRE",
                "latitude"=> -23.5305133,
                "longitude"=> -46.3980276
            ],
            [
                "title"=> "UBS PROFETA JEREMIAS",
                "fone"=> "1150213432",
                "street_title"=> "AV ENG ARMANDO DE ARRUDA PEREIRA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JABAQUARA",
                "latitude"=> -23.6521797,
                "longitude"=> -46.6380721
            ],
            [
                "title"=> "UBS REAL PQ PAULO MANGABEIRA ALBERNAZ FILHO",
                "fone"=> "1150418088",
                "street_title"=> "RUA VIAZA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JD AEROPORTO",
                "latitude"=> -23.6099239,
                "longitude"=> -46.705592
            ],
            [
                "title"=> "UBS RECANTO CAMPO BELO",
                "fone"=> "1150580891",
                "street_title"=> "RUA GIOVANNI DI BALDUCCIO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA MORAES",
                "latitude"=> -23.7923543,
                "longitude"=> -46.7386132
            ],
            [
                "title"=> "UBS RECANTO DOS HUMILDES",
                "fone"=> "1150629586",
                "street_title"=> "RUA DOMINGOS DE ROGATIS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM DA SAUDE",
                "latitude"=> -23.6020797,
                "longitude"=> -46.67385549999999
            ],
            [
                "title"=> "UBS RECANTO VERDE SOL",
                "fone"=> "1150736253",
                "street_title"=> "RUA ROSA DE MORAES",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "AGUA FUNDA",
                "latitude"=> -23.6145057,
                "longitude"=> -46.4156724
            ],
            [
                "title"=> "UBS REPUBLICA",
                "fone"=> "1150961058",
                "street_title"=> "RUA OSCAR GOMES CARDIM",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VL CORDEIRO",
                "latitude"=> -23.6206731,
                "longitude"=> -46.68792879999999
            ],
            [
                "title"=> "UBS REUNIDAS I",
                "fone"=> "1151817894",
                "street_title"=> "RUA ALEXANDRE DUMAS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "CHACARA STO ANTONIO",
                "latitude"=> -23.6353207,
                "longitude"=> -46.6990981
            ],
            [
                "title"=> "UBS REUNIDAS II",
                "fone"=> "1155108380",
                "street_title"=> "RUA BENEDITO MATARAZZO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "PARQUE MARIA HELENA",
                "latitude"=> -23.663138,
                "longitude"=> -46.76231629999999
            ],
            [
                "title"=> "UBS RIO CLARO",
                "fone"=> "1155114249",
                "street_title"=> "RUA MANOEL BORDALO PINHEIRO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "PARQUE SANTO ANTONIO",
                "latitude"=> -23.6215369,
                "longitude"=> -46.4573672
            ],
            [
                "title"=> "UBS RIO PEQUENO PAULO DE BARROS FRANCA",
                "fone"=> "1155114428",
                "street_title"=> "RUA GUTEMBERG JOSE FERREIRA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM LIDIA",
                "latitude"=> -23.5627129,
                "longitude"=> -46.7516434
            ],
            [
                "title"=> "UBS SACOMA",
                "fone"=> "1155114712",
                "street_title"=> "RUA TTE ISAIAS BRANCO DE ARAUJO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA DAS BELEZAS",
                "latitude"=> -23.6433177,
                "longitude"=> -46.741341
            ],
            [
                "title"=> "UBS SANTA BARBARA",
                "fone"=> "1155118717",
                "street_title"=> "RUA MAPORE",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "J BRASILIA",
                "latitude"=> -23.653836,
                "longitude"=> -46.7450237
            ],
            [
                "title"=> "UBS SANTO ELIAS",
                "fone"=> "1155140345",
                "street_title"=> "RUA AUDALIO GONCALVES DOS SANTOS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM THOMAS",
                "latitude"=> -23.4959822,
                "longitude"=> -46.7501471
            ],
            [
                "title"=> "UBS SAO JORGE CIDADE ADEMAR",
                "fone"=> "1155146355",
                "street_title"=> "RUA DINAR",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM ALFREDO",
                "latitude"=> -23.6856033,
                "longitude"=> -46.6614845
            ],
            [
                "title"=> "UBS SAO MATEUS I",
                "fone"=> "1155146430",
                "street_title"=> "RUA MARIA JOSE DE SOUZA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM SOUZA",
                "latitude"=> -23.6888511,
                "longitude"=> -46.7484253
            ],
            [
                "title"=> "UBS SE",
                "fone"=> "1155146448",
                "street_title"=> "RUA BALTAZAR DE SA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "CHACARA SANTANA",
                "latitude"=> -23.6715991,
                "longitude"=> -46.7470553
            ],
            [
                "title"=> "UBS SERGIO CHADDAD",
                "fone"=> "1155146609",
                "street_title"=> "RUA DANIEL KLEIN",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "PQ FIGUEIRA GRANDE",
                "latitude"=> -23.6787699,
                "longitude"=> -46.7489838
            ],
            [
                "title"=> "UBS SIGMUND FREUD INDIANOPOLIS",
                "fone"=> "1155172011",
                "street_title"=> "AV TAQUANDAVA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "CIDADE IPAVA",
                "latitude"=> -23.7203373,
                "longitude"=> -46.7648042
            ],
            [
                "title"=> "UBS SILMARYA REJANE MARCOLINO SOUZA",
                "fone"=> "1155173860",
                "street_title"=> "RUA ALBERGATI CAPACELLI",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "HORIZONTE AZUL",
                "latitude"=> -23.4546049,
                "longitude"=> -46.6995339
            ],
            [
                "title"=> "UBS SITIO DA CASA PINTADA",
                "fone"=> "1155236905",
                "street_title"=> "AVENIDA CLARA MANTELLI",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VELEIROS",
                "latitude"=> -23.6825728,
                "longitude"=> -46.7116508
            ],
            [
                "title"=> "UBS STA INES",
                "fone"=> "1155265175",
                "street_title"=> "RUA DR JUVENAL HUDSON FERREIRA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM MIRNA",
                "latitude"=> -23.7715593,
                "longitude"=> -46.6962993
            ],
            [
                "title"=> "UBS STA LUCIA",
                "fone"=> "1155267823",
                "street_title"=> "RUA CONSTELACAO DO ESQUADRO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "J CAMPINAS",
                "latitude"=> -22.9396676,
                "longitude"=> -47.1150371
            ],
            [
                "title"=> "UBS STA LUZIA",
                "fone"=> "1155272000",
                "street_title"=> "RUA DAS PLEIADES",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM MARIA AMALIA",
                "latitude"=> -23.771628,
                "longitude"=> -46.7126652
            ],
            [
                "title"=> "UBS STA MADALENA",
                "fone"=> "1155286223",
                "street_title"=> "RUA GEN JOSE DE OLIVEIRA RAMOS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM TRES CORACOES",
                "latitude"=> -23.7696124,
                "longitude"=> -46.6846632
            ],
            [
                "title"=> "UBS STO ESTEVAO CARMOSINA",
                "fone"=> "1155621476",
                "street_title"=> "RUA CIDADE DE SANTOS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "AMERICANOPOLIS",
                "latitude"=> -23.5553039,
                "longitude"=> -46.4534104
            ],
            [
                "title"=> "UBS THERSIO VENTURA",
                "fone"=> "1155643079",
                "street_title"=> "AV SANTA CATARINA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA SANTA CATARINA",
                "latitude"=> -23.5039716,
                "longitude"=> -46.47074540000001
            ],
            [
                "title"=> "UBS UNIAO DAS VILAS DE TAIPAS",
                "fone"=> "1155660395",
                "street_title"=> "RUA BELMIRO ZANETTI ESTEVES",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA SANTA CATARINA",
                "latitude"=> -23.6533302,
                "longitude"=> -46.6523928
            ],
            [
                "title"=> "UBS UNIAO DE V NOVA II ADAO MANOEL",
                "fone"=> "1155810426",
                "street_title"=> "AV CECI",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "PLANALTO PAULISTA",
                "latitude"=> -23.4861588,
                "longitude"=> -46.4598332
            ],
            [
                "title"=> "UBS UNIAO VILA NOVA I",
                "fone"=> "1156111641",
                "street_title"=> "RUA PEDRO FERNANDES ARAGAO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM SELMA",
                "latitude"=> -23.6902736,
                "longitude"=> -46.6570954
            ],
            [
                "title"=> "UBS V ALBERTINA DR OSVALDO MARCAL",
                "fone"=> "1156115777",
                "street_title"=> "RUA RAINHA DAS MISSOES",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA MISSIONARIA",
                "latitude"=> -22.2022609,
                "longitude"=> -46.6143541
            ],
            [
                "title"=> "UBS V ANASTACIO",
                "fone"=> "1156116933",
                "street_title"=> "AV EDUARDO PEREIRA RAMOS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "SAO JORGE",
                "latitude"=> -23.6856033,
                "longitude"=> -46.6614845
            ],
            [
                "title"=> "UBS V ANGLO JOSE SERRA RIBEIRO",
                "fone"=> "1156145595",
                "street_title"=> "RUA JULIETA DE ARAUJO ALMEIDA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JAD ARAUJO ALMEIDA",
                "latitude"=> -23.6556216,
                "longitude"=> -46.7582505
            ],
            [
                "title"=> "UBS V ANTONIETA",
                "fone"=> "1156219200",
                "street_title"=> "RUA CLAUDIA MUZIO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "J SAO CARLOS",
                "latitude"=> -23.682487,
                "longitude"=> -46.6579594
            ],
            [
                "title"=> "UBS V APARECIDA",
                "fone"=> "1156220844",
                "street_title"=> "RUA LUIS VIVES",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA JOANIZA",
                "latitude"=> -23.6741311,
                "longitude"=> -46.65957230000001
            ],
            [
                "title"=> "UBS V ARAPUA",
                "fone"=> "1156236883",
                "street_title"=> "RUA ROLANDO CURTI",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA CLARA",
                "latitude"=> -23.6722765,
                "longitude"=> -46.63750599999999
            ],
            [
                "title"=> "UBS V ARICANDUVA",
                "fone"=> "1156239993",
                "street_title"=> "RUA PASCOAL GRIECO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "CIDADE JULIA",
                "latitude"=> -23.6910389,
                "longitude"=> -46.6404976
            ],
            [
                "title"=> "UBS V ARRIETE DECIO PACHECO PEDROSO",
                "fone"=> "1156246797",
                "street_title"=> "NESTOR SAMPAIO PENTEADO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "AMERICANOPOLIS",
                "latitude"=> -23.6806064,
                "longitude"=> -46.6864982
            ],
            [
                "title"=> "UBS V BARBOSA",
                "fone"=> "1156341643",
                "street_title"=> "RUA ARNALDO MAGNICARO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "CAMPO GRANDE",
                "latitude"=> -23.6736007,
                "longitude"=> -46.6852715
            ],
            [
                "title"=> "UBS V BORGES",
                "fone"=> "1156727444",
                "street_title"=> "RUA DOS ANIQUIS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JAD SANTA TEREZINHA",
                "latitude"=> -23.6973425,
                "longitude"=> -46.6485628
            ],
            [
                "title"=> "UBS V CARMOSINA",
                "fone"=> "1156730001",
                "street_title"=> "RUA DR DARI BARCELOS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JD APURA",
                "latitude"=> -23.7106654,
                "longitude"=> -46.6531101
            ],
            [
                "title"=> "UBS V CHABILANDIA",
                "fone"=> "1156734455",
                "street_title"=> "ESTRADA DA SAUDE",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM MATA VIRGEM",
                "latitude"=> -23.7145102,
                "longitude"=> -46.6275282
            ],
            [
                "title"=> "UBS V CISPER",
                "fone"=> "1156740921",
                "street_title"=> "RUA VALENTINO FIORAVANTE",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM RUBILENE",
                "latitude"=> -23.7026945,
                "longitude"=> -46.635972
            ],
            [
                "title"=> "UBS V CLARA",
                "fone"=> "1156787444",
                "street_title"=> "RUA HERMENEGILDO MARTINI",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA DO CASTELO",
                "latitude"=> -23.6744815,
                "longitude"=> -46.6659658
            ],
            [
                "title"=> "UBS V CONSTANCIA VICENTE OCTAVIO GUIDA",
                "fone"=> "1156873367",
                "street_title"=> "RUA CONDE DE ITU",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "SANTO AMARO",
                "latitude"=> -23.6487389,
                "longitude"=> -46.7000116
            ],
            [
                "title"=> "UBS V COSMOPOLITA",
                "fone"=> "1158123738",
                "street_title"=> "RUA JOAO FERNANDES CAMISA NOVA JUNIOR",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "J CELESTE",
                "latitude"=> -23.6566958,
                "longitude"=> -46.7414255
            ],
            [
                "title"=> "UBS V CURUCA",
                "fone"=> "1158165562",
                "street_title"=> "RUA MELO COUTINHO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "PARQUE REGINA",
                "latitude"=> -23.6346951,
                "longitude"=> -46.7557907
            ],
            [
                "title"=> "UBS V DALVA GUILHERME HENRIQUE P COELHO",
                "fone"=> "1158234937",
                "street_title"=> "RUA SEBASTIAO ADVINCULA CUNHA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JD ELEDY",
                "latitude"=> -23.6523005,
                "longitude"=> -46.7847295
            ],
            [
                "title"=> "UBS V DAS BELEZAS ALBERTO AMBROSIO",
                "fone"=> "1158246020",
                "street_title"=> "RUA VITORINO PALHARES",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "J MAGDALENA",
                "latitude"=> -23.5322723,
                "longitude"=> -46.6518551
            ],
            [
                "title"=> "UBS V DAS MERCES",
                "fone"=> "1158252278",
                "street_title"=> "RUA GASTAO RAUL FOURTON BOUSQUET",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM MARCELO",
                "latitude"=> -23.6557057,
                "longitude"=> -46.7710947
            ],
            [
                "title"=> "UBS V EDE",
                "fone"=> "1158255124",
                "street_title"=> "AVENIDA DON RODRIGO SANCHES",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "PARQUE DO ENGENHO",
                "latitude"=> -23.6611996,
                "longitude"=> -46.7862551
            ],
            [
                "title"=> "UBS V ESPERANCA CASSIO BITTENCOURT FILHO",
                "fone"=> "1158256166",
                "street_title"=> "AV CARLOS LACERDA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JD VALQUIRIA",
                "latitude"=> -23.5313493,
                "longitude"=> -46.532566
            ],
            [
                "title"=> "UBS V ESPERANCA EMILIO SANTIAGO DE OLIVEIRA",
                "fone"=> "1158311448",
                "street_title"=> "RUA PORTA DO PRADO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "PQ NOVO STO AMARO",
                "latitude"=> -23.5234676,
                "longitude"=> -46.5263225
            ],
            [
                "title"=> "UBS V FORMOSA II",
                "fone"=> "1158315647",
                "street_title"=> "RUA MANOEL VITOR DE JESUS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM NAKAMURA",
                "latitude"=> -15.540641,
                "longitude"=> -47.33571
            ],
            [
                "title"=> "UBS V GRANADA ALFREDO F PAULINO FILHO",
                "fone"=> "1158326643",
                "street_title"=> "RUA IGNACIO LIMAS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "J HERCULANO",
                "latitude"=> -23.691851,
                "longitude"=> -46.7574863
            ],
            [
                "title"=> "UBS V GUACURI",
                "fone"=> "1158334282",
                "street_title"=> "RUA PIETRO DA MILANO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA SANTO AMARO",
                "latitude"=> -23.6970312,
                "longitude"=> -46.6325243
            ],
            [
                "title"=> "UBS V GUILHERMINA",
                "fone"=> "1158334375",
                "street_title"=> "RUA SERAFIM ALVARES",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM CAICARA",
                "latitude"=> -23.683562,
                "longitude"=> -46.7754015
            ],
            [
                "title"=> "UBS V IMPERIO II",
                "fone"=> "1158339952",
                "street_title"=> "AV PROF MARIO MAZAGAO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "ALTO DA RIVIERA",
                "latitude"=> -23.6996022,
                "longitude"=> -46.7673756
            ],
            [
                "title"=> "UBS V IPOJUCA WANDA COELHO DE MORAES",
                "fone"=> "1158340153",
                "street_title"=> "RUA DOS TEMAS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM KAGOHARA",
                "latitude"=> -23.5320078,
                "longitude"=> -46.70109799999999
            ],
            [
                "title"=> "UBS V ITAPEMA",
                "fone"=> "1158411871",
                "street_title"=> "RUA JORGE OZI",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM CATANDUVA",
                "latitude"=> -23.6366519,
                "longitude"=> -46.76394819999999
            ],
            [
                "title"=> "UBS V IZOLINA MAZZEI",
                "fone"=> "1158412654",
                "street_title"=> "RUA DR JOVIANO PACHECO AGUIRRI",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "CAMPO LIMPO",
                "latitude"=> -23.4932392,
                "longitude"=> -46.6009418
            ],
            [
                "title"=> "UBS V JAGUARA",
                "fone"=> "1158429292",
                "street_title"=> "R FREI XISTO TEUBER",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JD MITSUTANI",
                "latitude"=> -23.6422934,
                "longitude"=> -46.7808237
            ],
            [
                "title"=> "UBS V JOANIZA",
                "fone"=> "1158438340",
                "street_title"=> "RUA FRANCISCO SALES",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JD DAS PALMAS",
                "latitude"=> -23.6221715,
                "longitude"=> -46.7458416
            ],
            [
                "title"=> "UBS V LEONOR",
                "fone"=> "1158438861",
                "street_title"=> "RUA CANORI",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM ANA MARIA",
                "latitude"=> -23.4986387,
                "longitude"=> -46.5968382
            ],
            [
                "title"=> "UBS V MARIA LUIZ PAULO GNECCO",
                "fone"=> "1158519026",
                "street_title"=> "RUA ANTONIO DA MATA JUNIOR",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "J SAO LUIS",
                "latitude"=> -23.6548128,
                "longitude"=> -46.7334191
            ],
            [
                "title"=> "UBS V MEDEIROS",
                "fone"=> "1158719484",
                "street_title"=> "RUA LUAR DO SERTAO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "CHACARA STA MARIA",
                "latitude"=> -23.4586975,
                "longitude"=> -46.6965882
            ],
            [
                "title"=> "UBS V MISSIONARIA",
                "fone"=> "1158734880",
                "street_title"=> "RUA COSTA NOVA DO PRADO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM COMERCIAL",
                "latitude"=> -23.6714841,
                "longitude"=> -46.7795974
            ],
            [
                "title"=> "UBS V MORAES JOAO PAULO BOTELHO VIEIRA",
                "fone"=> "1158735089",
                "street_title"=> "RUA JOAO DE ALMADA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "J GUARUJA",
                "latitude"=> -23.6940993,
                "longitude"=> -46.7869621
            ],
            [
                "title"=> "UBS V N SRA APARECIDA",
                "fone"=> "1158743919",
                "street_title"=> "RUA JOAO ROBALO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM SAO BENTO",
                "latitude"=> -23.6791553,
                "longitude"=> -46.7795121
            ],
            [
                "title"=> "UBS V NIVI",
                "fone"=> "1158744721",
                "street_title"=> "R FERES BECHARA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM MARACA",
                "latitude"=> -23.6716433,
                "longitude"=> -46.7727496
            ],
            [
                "title"=> "UBS V NOVA CURUCA",
                "fone"=> "1158922134",
                "street_title"=> "RUA PHELIPPE DE VITRY",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "J STA JOSEFINA",
                "latitude"=> -23.6651855,
                "longitude"=> -46.7357921
            ],
            [
                "title"=> "UBS V NOVA GALVAO",
                "fone"=> "1158925799",
                "street_title"=> "RUA CAPAO REDONDO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "J STA MARGARIDA",
                "latitude"=> -23.6786808,
                "longitude"=> -46.7554963
            ],
            [
                "title"=> "UBS V NOVA MANCHESTER DR ARLINDO GENNARI",
                "fone"=> "1158958117",
                "street_title"=> "ESTRADA DO M BOI MIRIM",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA CALU",
                "latitude"=> -23.546588,
                "longitude"=> -46.5319789
            ],
            [
                "title"=> "UBS V NOVA YORK",
                "fone"=> "1158961027",
                "street_title"=> "RUA FRANCISCO HOMEM DEL REI",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM ARACATI",
                "latitude"=> -23.7189499,
                "longitude"=> -46.7700864
            ],
            [
                "title"=> "UBS V OLIMPIA MAX PERLMAN",
                "fone"=> "1158961143",
                "street_title"=> "RUA BARAO DE PAIVA MANSO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM CAPELA",
                "latitude"=> -23.7304448,
                "longitude"=> -46.7887786
            ],
            [
                "title"=> "UBS V ORATORIO TITO PEDRO MASCELANI",
                "fone"=> "1158985400",
                "street_title"=> "RUA CARMELO CALI",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA SANTA LUCIA",
                "latitude"=> -23.8790493,
                "longitude"=> -46.64262189999999
            ],
            [
                "title"=> "UBS V PALMEIRAS",
                "fone"=> "1158997754",
                "street_title"=> "AV FUNCIONARIOS PUBLICOS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JD VERA CRUZ",
                "latitude"=> -23.7350152,
                "longitude"=> -46.7813247
            ],
            [
                "title"=> "UBS V PRAIA VITORIO ROLANDO BOCCALETTI",
                "fone"=> "1159202604",
                "street_title"=> "RUA ALICE BASTIDE",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA ROSCHEL",
                "latitude"=> -23.8407878,
                "longitude"=> -46.7356451
            ],
            [
                "title"=> "UBS V PROGRESSO",
                "fone"=> "1159202618",
                "street_title"=> "AVENIDA DAS ARARAS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VARGEM GRANDE",
                "latitude"=> -23.4767758,
                "longitude"=> -46.703439
            ],
            [
                "title"=> "UBS V PROGRESSO J MONTE ALEGRE",
                "fone"=> "1159208860",
                "street_title"=> "RUA JUVENAL LUZ",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM NOVO PARELHEI",
                "latitude"=> -23.8293948,
                "longitude"=> -46.727152
            ],
            [
                "title"=> "UBS V RAMOS DR LUIS AUGUSTO DE CAMPOS",
                "fone"=> "1159208977",
                "street_title"=> "AVENIDA DOMENICO LANZETTI",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "SAO NORBERTO",
                "latitude"=> -23.6098201,
                "longitude"=> -46.640181
            ],
            [
                "title"=> "UBS V REGINA",
                "fone"=> "1159219504",
                "street_title"=> "RUA CONDE DE LANCASTRE",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM SANTA FE",
                "latitude"=> -23.8320275,
                "longitude"=> -46.7110247
            ],
            [
                "title"=> "UBS V ROMANA",
                "fone"=> "1159225053",
                "street_title"=> "RUA FORTE DE LADARIO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM IPORA",
                "latitude"=> -23.7833527,
                "longitude"=> -46.7126169
            ],
            [
                "title"=> "UBS V SANTANA",
                "fone"=> "1159243636",
                "street_title"=> "AVENIDA CARLOS OBERHUBER",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA SAO JOSE",
                "latitude"=> -23.7431294,
                "longitude"=> -46.7112191
            ],
            [
                "title"=> "UBS V STA CATARINA",
                "fone"=> "1159262002",
                "street_title"=> "AVENIDA SADAMU INOUE",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "RECANTO CAMPO BELO",
                "latitude"=> -23.7880492,
                "longitude"=> -46.7222204
            ],
            [
                "title"=> "UBS V TEREZINHA",
                "fone"=> "1159314151",
                "street_title"=> "RUA JOAO CARLOS DE OLIVEIRA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "CHACARA DO SOL",
                "latitude"=> -23.7799075,
                "longitude"=> -46.6741248
            ],
            [
                "title"=> "UBS VARGEM GRANDE",
                "fone"=> "1159314766",
                "street_title"=> "RUA FELINTO MILANEZ",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "PQ RESIDENCIAL COCAI",
                "latitude"=> -23.7449871,
                "longitude"=> -46.6688972
            ],
            [
                "title"=> "UBS VARGINHA",
                "fone"=> "1159324659",
                "street_title"=> "RUA HENRY ARTHUR JONES",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM ELIANA",
                "latitude"=> -23.7537635,
                "longitude"=> -46.6709666
            ],
            [
                "title"=> "UBS VELEIROS",
                "fone"=> "1159326799",
                "street_title"=> "AV SAO PAULO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM GAIVOTAS",
                "latitude"=> -23.7345569,
                "longitude"=> -46.6611558
            ],
            [
                "title"=> "UBS VERA POTY",
                "fone"=> "1159724328",
                "street_title"=> "RUA SAO ROQUE DO PARAGUACU",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA QUINTANA",
                "latitude"=> -23.8677033,
                "longitude"=> -46.6516849
            ],
            [
                "title"=> "UBS VILA ALPINA DR HERMINIO MOREIRA",
                "fone"=> "1159736400",
                "street_title"=> "RUA CONTOS AMAZONICOS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JORDANOPOLIS",
                "latitude"=> -23.5964669,
                "longitude"=> -46.5675552
            ],
            [
                "title"=> "UBS VILA CAIUBA",
                "fone"=> "1159738680",
                "street_title"=> "AVENIDA JOAO PAULO BARRETO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JD CASTRO ALVES",
                "latitude"=> -23.3999249,
                "longitude"=> -46.7510521
            ],
            [
                "title"=> "UBS VILA CALIFORNIA ZEILIVAL BRUSCAGIN",
                "fone"=> "1159742289",
                "street_title"=> "ESTRADA ITAQUAQUECETUBA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "ILHA DO BORORE",
                "latitude"=> -23.6050911,
                "longitude"=> -46.5550486
            ],
            [
                "title"=> "UBS VILA CALU",
                "fone"=> "1159742486",
                "street_title"=> "RUA LUIZ CARLOS DE ALMEIDA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "CHACARA SANTO AMARO",
                "latitude"=> -23.821353,
                "longitude"=> -46.673458
            ],
            [
                "title"=> "UBS VILA CAMPESTRE",
                "fone"=> "1159752192",
                "street_title"=> "ESTRADA BENEDITO SCHUNCK",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM EMBURA",
                "latitude"=> -23.8805586,
                "longitude"=> -46.7415585
            ],
            [
                "title"=> "UBS VILA CARIOCA",
                "fone"=> "1159752281",
                "street_title"=> "ESTRADA ENGENHEIRO MARSILAC",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "MARSILAC",
                "latitude"=> -23.5967402,
                "longitude"=> -46.5949857
            ],
            [
                "title"=> "UBS VILA DIONISIA",
                "fone"=> "1159754221",
                "street_title"=> "RUA MABEL NORMANDO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM DAS FONTES",
                "latitude"=> -23.4650738,
                "longitude"=> -46.661776
            ],
            [
                "title"=> "UBS VILA ESPANHOLA",
                "fone"=> "1159773212",
                "street_title"=> "RUA TRES",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "BARRAGEM",
                "latitude"=> -23.4814166,
                "longitude"=> -46.6679263
            ],
            [
                "title"=> "UBS VILA GUARANI",
                "fone"=> "1159773269",
                "street_title"=> "RUA NOSSA SENHORA APARECIDA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "COLONIA PAULISTA",
                "latitude"=> -23.534011,
                "longitude"=> -46.442704
            ],
            [
                "title"=> "UBS VILA HELOISA",
                "fone"=> "1159773507",
                "street_title"=> "RUA EDUARDO COLLIER FILHO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "CIDADE NOVA AMERICA",
                "latitude"=> -23.5896451,
                "longitude"=> -46.5348143
            ],
            [
                "title"=> "UBS VILA JACUI",
                "fone"=> "1159773851",
                "street_title"=> "RUA JOAO LANG",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "BARRAGEM",
                "latitude"=> -23.5804798,
                "longitude"=> -46.404453
            ],
            [
                "title"=> "UBS VILA MAGGI",
                "fone"=> "1159786394",
                "street_title"=> "ESTRADA DA LIGACAO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "PONTE SECA",
                "latitude"=> -23.4556109,
                "longitude"=> -46.7165214
            ],
            [
                "title"=> "UBS VILA MANGALOT",
                "fone"=> "1159798972",
                "street_title"=> "RUA AMADO BENEDITO VILAS BOAS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA MARCELO",
                "latitude"=> -23.7880492,
                "longitude"=> -46.7222204
            ],
            [
                "title"=> "UBS VILA MARCELO",
                "fone"=> "1161012979",
                "street_title"=> "RUA JOAO FIALHO DE CARVALHO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA DIVA",
                "latitude"=> -23.7926888,
                "longitude"=> -46.72459689999999
            ],
            [
                "title"=> "UBS VILA MATILDE DR RUBENS DO VAL",
                "fone"=> "1162016586",
                "street_title"=> "RUA ORLANDO RIBEIRO DANTAS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA IZOLINA MAZZEI",
                "latitude"=> -23.5367136,
                "longitude"=> -46.527624
            ],
            [
                "title"=> "UBS VILA NATAL",
                "fone"=> "1162030111",
                "street_title"=> "AVN VIRGILIA RODRIGUES A C PINTO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JD LEONOR M BARROS",
                "latitude"=> -23.4649947,
                "longitude"=> -46.6048435
            ],
            [
                "title"=> "UBS VILA NOVA JAGUARE",
                "fone"=> "1162048421",
                "street_title"=> "RUA DO HORTO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "HORTO FLORESTAL",
                "latitude"=> -23.5431831,
                "longitude"=> -46.7444491
            ],
            [
                "title"=> "UBS VILA PENTEADO",
                "fone"=> "1162318771",
                "street_title"=> "AV PERI RONCHETTI",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM PERI",
                "latitude"=> -23.4642231,
                "longitude"=> -46.6546591
            ],
            [
                "title"=> "UBS VILA PEREIRA BARRETO",
                "fone"=> "1162430052",
                "street_title"=> "AV EDU CHAVES",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "PQ EDU CHAVES",
                "latitude"=> -23.477548,
                "longitude"=> -46.5684592
            ],
            [
                "title"=> "UBS VILA PIAUI",
                "fone"=> "1169466660",
                "street_title"=> "RUA COLOMBO FLORENCE",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA ARAPUA",
                "latitude"=> -23.6336419,
                "longitude"=> -46.58910600000001
            ],
            [
                "title"=> "UBS VILA PIRITUBA",
                "fone"=> "1169501451",
                "street_title"=> "RUA COPACABANA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "SANTA TEREZINHA",
                "latitude"=> -23.4943794,
                "longitude"=> -46.6373103
            ],
            [
                "title"=> "UBS VILA PREL ANTONIO BERNARDES DE OLIVEIRA",
                "fone"=> "1169542851",
                "street_title"=> "RUA SOBRAL JUNIOR",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA MARIA ALTA",
                "latitude"=> -23.6483509,
                "longitude"=> -46.7541212
            ],
            [
                "title"=> "UBS VILA PRUDENTE",
                "fone"=> "1169922380",
                "street_title"=> "RUA CLOVIS SALGADO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM DAS PEDRAS",
                "latitude"=> -23.4192552,
                "longitude"=> -46.5903996
            ],
            [
                "title"=> "UBS VILA RAMOS FREGUESIA DO O",
                "fone"=> "2255170045",
                "street_title"=> "ESTRADA DA BARONEZA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "PARQUE DO LAGO",
                "latitude"=> -23.4824475,
                "longitude"=> -46.6780218
            ],
            [
                "title"=> "UBS VILA RENATO",
                "fone"=> "3125618034",
                "street_title"=> "RUA JOSE ALEXANDRE MACHADO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "ITAIM PAULISTA",
                "latitude"=> -23.617727,
                "longitude"=> -46.50694
            ],
            [
                "title"=> "UBS VILA ROSCHEL",
                "fone"=> "11366251775",
                "street_title"=> "RUA PAUVA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA JAGUARA",
                "latitude"=> -23.8416921,
                "longitude"=> -46.7371907
            ],
            [
                "title"=> "UBS VILA SABRINA DR CARLOS AUGUSTO AUTRAN PEDERNEIRAS LIMA",
                "fone"=> "11586163342",
                "street_title"=> "R ODEMIS",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JD UMUARAMA",
                "latitude"=> -23.4925308,
                "longitude"=> -46.5730975
            ],
            [
                "title"=> "UBS VILA SANTA MARIA",
                "fone"=> "11922925687",
                "street_title"=> "AVENIDA CELSO GARCIA",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "BELENZINHO",
                "latitude"=> -23.5371988,
                "longitude"=> -46.59556080000001
            ],
            [
                "title"=> "UBS VILA SANTO ESTEVAO",
                "fone"=> "27030947",
                "street_title"=> "RUA BERNARDINO FERRAZ",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "VILA RENATO",
                "latitude"=> -23.61831,
                "longitude"=> -46.5059874
            ],
            [
                "title"=> "UBS VILA SILVIA",
                "fone"=> "27048997",
                "street_title"=> "R BATISTA FERGUZIO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "V CARDOSO FRANCO",
                "latitude"=> -23.4916477,
                "longitude"=> -46.5045905
            ],
            [
                "title"=> "UBS VILA SONIA",
                "fone"=> "27213798",
                "street_title"=> "RUA JIM CLARK",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM SINHA",
                "latitude"=> -23.5980987,
                "longitude"=> -46.7346724
            ],
            [
                "title"=> "UBS VILA ZATT",
                "fone"=> "58422399",
                "street_title"=> "RUA THOMAS DE ARAUJO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JD HELGA",
                "latitude"=> -23.476988,
                "longitude"=> -46.71167579999999
            ],
            [
                "title"=> "UBS WAMBERTO DIAS DA COSTA",
                "fone"=> "",
                "street_title"=> "RUA CARUANENSE",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "JARDIM NOVO HORIZONT",
                "latitude"=> -23.4688603,
                "longitude"=> -46.6031472
            ],
            [
                "title"=> "UBS ZUMBI DOS PALMARES",
                "fone"=> "",
                "street_title"=> "RUA BATISTA FALCIDIO",
                "city"=> "São Paulo",
                "state"=> "SP",
                "district"=> "SILVEIRA",
                "latitude"=> -23.6720352,
                "longitude"=> -46.7460402
            ]
        ];

        foreach ($json as $key => $item){

            $discart = DiscartPoint::firstOrCreate([
                'title' => $item['title'],
                'fone' => $item['fone']
            ]);
            $discart->discart_point_addresses()->firstOrCreate([
                'street_code' => null,
                'street_title' => $item['street_title'],
                'number' => null,
                'complement' => null,
                'city' => $item['city'],
                'latitude' => $item['latitude'],
                'longitude' => $item['longitude'],
                'state' => $item['state'],
                'district' => $item['district']
            ]);
        }

    }
}
