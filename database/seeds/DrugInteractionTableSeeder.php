<?php

use Illuminate\Database\Seeder;
use DrPediu\Models\DrugInteraction;

class DrugInteractionTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$path = storage_path() . "/drug_interaction/drug_interaction.json";
		$drugs = json_decode(file_get_contents($path), true);

		foreach ($drugs as $drug) {

			foreach ($drug as $item) {

				DrugInteraction::firstOrCreate([
					"title_interaction" => $item['title_interaction'],
					"degree_interaction" => $item['degree_interaction'],
					"action_start" => $item['action_start'],
					"recommendation" => $item['recommendation'],
					"clinical_effect" => $item['clinical_effect']
				]);
			}
		}

	}
}
