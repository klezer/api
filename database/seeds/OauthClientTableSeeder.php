<?php

use Illuminate\Database\Seeder;
use DrPediu\Models\OauthClient;
class OauthClientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $key_client = OauthClient::find(1);

        if($key_client)
        {
            $key_client->update(['name' => 'Dr.Pediu','secret' => 'YUJxCMuTMl9P4m3039svgaA5FVqMcDAvOid8dWqZ']);
        }else{
            OauthClient::FirstOrCreate([
                'name' => 'Dr.Pediu',
                'secret' => 'YUJxCMuTMl9P4m3039svgaA5FVqMcDAvOid8dWqZ',
                'redirect' => 'http://localhost',
                'personal_access_client' => 1,
                'password_client' => 0,
                'revoked' => 0
            ]);
        }
    }
}
