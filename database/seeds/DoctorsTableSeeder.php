<?php

use Illuminate\Database\Seeder;

use DrPediu\Models\Doctor;

class DoctorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Doctor::firstOrCreate([
            'user_id' => 1,
            'crm_number' => '12389745PH',
            'signature_image' => 'assinatura/120px-Pitty_Assinatura.png'
        ]);
        Doctor::firstOrCreate([
            'user_id' => 2,
            'crm_number' => '234577TY',
            'signature_image' => 'assinatura/120px-Pitty_Assinatura.png'
        ]);
    }
}
