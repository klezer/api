<?php

use Illuminate\Database\Seeder;
use DrPediu\Models\TypeRecipe;

class TypeRecipesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TypeRecipe::firstOrCreate([
           'type' => 'TEMPORÁRIA',
           'description' => 'está flag indica que a medicação é temporária.'
        ]);

        TypeRecipe::firstOrCreate([
            'type' => 'PERMANENTE',
            'description' => 'está flag indica que a medicação é permanente.'
        ]);
    }
}
