<?php

use Illuminate\Database\Seeder;
use DrPediu\Models\Recipe;

class RecipesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Recipe::firstOrCreate([
           'description' => 'Lorem ipsum curabitur aliquam maecenas iaculis aenean curae conubia ultricies tellus, etiam sagittis lobortis habitant id diam blandit imperdiet curabitur, erat quisque volutpat at cursus tortor class cubilia potenti. ',
           'instructions' => 'Augue quisque faucibus vestibulum pharetra sit aenean fusce ultricies, curae ut risus quisque porttitor sed conubia dapibus',
           'validity_of_recipe' => '2022-10-10',
           'type_recipe_id' => 1,

        ]);
        Recipe::firstOrCreate([
            'description' => 'produto de teste',
            'instructions' => 'instruções de teste',
            'validity_of_recipe' => '2022-10-10',
            'type_recipe_id' => 1,

        ]);
        Recipe::firstOrCreate([
            'description' => 'Metiolate',
            'instructions' => 'instruções de uso passar moderadamente',
            'validity_of_recipe' => '2021-10-10',
            'type_recipe_id' => 2,

        ]);
        Recipe::firstOrCreate([
            'description' => 'Doril',
            'instructions' => 'instruções de uso tomar moderadamente',
            'validity_of_recipe' => '2021-10-10',
            'type_recipe_id' => 2,

        ]);
    }
}
