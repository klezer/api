<?php

use Illuminate\Database\Seeder;

use DrPediu\Models\PivotDoctorsXRecipesXUser;

class PivotDoctorsxRecipesxUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PivotDoctorsXRecipesXUser::firstOrCreate([
           'recipes_id' => 1,
           'user_id' => 1,
           'doctor_id' => 1,
        ]);

        PivotDoctorsXRecipesXUser::firstOrCreate([
            'recipes_id' => 2,
            'user_id' => 1,
            'doctor_id' => 2,

        ]);
        PivotDoctorsXRecipesXUser::firstOrCreate([
            'recipes_id' => 2,
            'user_id' => 1,
            'doctor_id' => 2,

        ]);

    }
}
