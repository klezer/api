<?php

use Illuminate\Database\Seeder;

use DrPediu\Models\PivotCommentsXRecipesXUser;

class PivotCommenstxRecipesxUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PivotCommentsXRecipesXUser::firstOrCreate([
           'recipes_id' => 1,
           'user_id' => 1,
            'comment_id' => 1
        ]);
        PivotCommentsXRecipesXUser::firstOrCreate([
            'recipes_id' => 1,
            'user_id' => 2,
            'comment_id' => 1
        ]);
        PivotCommentsXRecipesXUser::firstOrCreate([
            'recipes_id' => 2,
            'user_id' => 1,
            'comment_id' => 2
        ]);
        PivotCommentsXRecipesXUser::firstOrCreate([
            'recipes_id' => 2,
            'user_id' => 2,
            'comment_id' => 3
        ]);
    }
}
