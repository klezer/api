<?php

use Illuminate\Database\Seeder;

use DrPediu\Models\PivotRecipesXMedicinesXTreatment;

class PivotRecipesxMedicinesxTreatmentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PivotRecipesXMedicinesXTreatment::firstOrCreate([
           'recipes_id' => 1,
           'medicine_id' =>  1

        ]);

        PivotRecipesXMedicinesXTreatment::firstOrCreate([
            'recipes_id' => 2,
            'medicine_id' =>  2

        ]);
        PivotRecipesXMedicinesXTreatment::firstOrCreate([
            'recipes_id' => 3,
            'medicine_id' =>  3

        ]);
    }
}
