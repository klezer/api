<?php

use Illuminate\Database\Seeder;

use DrPediu\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::firstOrCreate([
            'name' => 'admin',
            'cpf' =>'3212378642345',
            'rg' => '678645665663',
            'date_of_birth' => '1981-02-02',
            'email' => 'admin@admin.doc88.com.br',
            'genre' => 'M',
            'password' => bcrypt('admin@doc88')
        ]);
        User::firstOrCreate([
            'name' => 'Marta',
            'cpf' =>'61367449375',
            'rg' => '753456662234xx',
            'date_of_birth' => '1971-05-05',
            'email' => 'marta@admin.doc88.com.br',
            'genre' => 'F',
            'password' => bcrypt('marta@doc88')
        ]);
        User::firstOrCreate([
            'name' => 'Maria',
            'cpf' =>'57057775741',
            'rg' => '75334225784',
            'date_of_birth' => '1956-08-12',
            'email' => 'maria@admin.doc88.com.br',
            'genre' => 'F',
            'password' => bcrypt('maria@doc88')
        ]);
    }
}
