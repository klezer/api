<?php

use Illuminate\Database\Seeder;

use DrPediu\Models\Medicine;

class MedicinesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Medicine::firstOrCreate([
            'title' => 'Doril',
            'note' => 'Paracetamol 250mg + Ácido acetilsalicílico 250mg + Cafeína 65mg',

        ]);
        Medicine::firstOrCreate([
            'title' => 'Cloreto de sódio 0,9% 9mg/mL',
            'note' => 'Cloreto de sódio 9mg/mL',

        ]);
        Medicine::firstOrCreate([
            'title' => 'Alivium 400mg , Cápsula (20un) Mantecorp',
            'note' => 'Ibuprofeno 400mg',
        ]);
        Medicine::firstOrCreate([
            'title' => 'Paracetamol 100mg',
            'note' => 'Paracetamol 100mg , Pó para preparação extemporânea (50un de 5g)',
        ]);


    }
}
