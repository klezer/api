<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableStartTreatments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('start_treatments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('medicine_id')->unsigned();
            $table->integer('user_id');
            $table->timestamp('start_period')->nullable();
            $table->timestamp('end_period')->nullable();
            $table->timestamp('taken_period')->nullable();
            $table->boolean('taken')->nullable();
            $table->boolean('schedule_generate');
            $table->boolean('notifications');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('medicine_id')->references('id')->on('medicines')->onDelete('no action')->onUpdate('no action');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('start_treatments');
    }
}
