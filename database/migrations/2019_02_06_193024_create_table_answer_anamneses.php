<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAnswerAnamneses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answer_anamneses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_quest_anamnese')->unsigned()->nullable();
            $table->integer('user_id')->unsigned();
            $table->boolean('option');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_quest_anamnese')->references('id')->on('quests_anamneses')->onDelete('no action')->onUpdate('no action');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('no action')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('answer_anamneses');
    }
}
