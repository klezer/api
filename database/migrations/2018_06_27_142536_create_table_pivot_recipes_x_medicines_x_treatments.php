<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePivotRecipesXMedicinesXTreatments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pivot_recipes_x_medicines_x_treatments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('recipes_id')->unsigned();
            $table->integer('medicine_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('recipes_id')->references('id')->on('recipes')->onDelete('no action')->onUpdate('no action');
            $table->foreign('medicine_id')->references('id')->on('medicines')->onDelete('no action')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pivot_recipes_x_medicines_x_treatments');
    }
}
