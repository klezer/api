<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMedicines extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicines', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
	        $table->string('subtitle')->nullable();
	        $table->string('dosage')->nullable();
	        $table->integer('frequency')->nullable();
	        $table->integer('number_of_days')->nullable();
	        $table->string('target')->nullable();
	        $table->date('validity_of_medicine')->nullable();
	        $table->integer('sales_of_medicines_id')->default(0);
            $table->text('note');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medicines');
    }
}
