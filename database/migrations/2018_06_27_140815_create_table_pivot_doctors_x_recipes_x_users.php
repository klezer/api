<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePivotDoctorsXRecipesXUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pivot_doctors_x_recipes_x_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('recipes_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('doctor_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('recipes_id')->references('id')->on('recipes')->onDelete('no action')->onUpdate('no action');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('no action')->onUpdate('no action');
            $table->foreign('doctor_id')->references('id')->on('doctors')->onDelete('no action')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pivot_doctors_x_recipes_x_users');
    }
}
