<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRecipes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipes', function (Blueprint $table) {
            $table->increments('id');
            $table->text('description')->nullable();
            $table->text('instructions')->nullable();
            $table->date('validity_of_recipe');
	        $table->boolean('start_recipe')->default(0);
            $table->integer('type_recipe_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('type_recipe_id')->references('id')->on('type_recipes')->onDelete('no action')->onUpdate('no action');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipes');
    }
}
