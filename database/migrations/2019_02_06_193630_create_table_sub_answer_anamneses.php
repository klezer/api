<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSubAnswerAnamneses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_answer_anamneses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->integer('id_answer_anamnese')->unsigned()->nullable();
            $table->string('answer');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_answer_anamnese')->references('id')->on('answer_anamneses')->onDelete('no action')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_answer_anamneses');
    }
}
