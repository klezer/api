<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePivotDoctorXSpecialty extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pivot_doctors_x_specialty', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('specialty_id')->unsigned();
            $table->integer('doctor_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('specialty_id')->references('id')->on('specialties_medicals')->onDelete('no action')->onUpdate('no action');
            $table->foreign('doctor_id')->references('id')->on('doctors')->onDelete('no action')->onUpdate('no action');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pivot_doctors_x_specialty');
    }
}
