<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDiscartPointAddress extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discart_point_address', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('discart_point_id')->unsigned();
            $table->string('street_code')->nullable();
            $table->string('street_title');
            $table->string('number',10)->nullable();
            $table->string('complement',25)->nullable();
            $table->string('latitude',80)->nullable();
            $table->string('longitude',80)->nullable();
            $table->string('city',25);
            $table->string('state',25);
            $table->string('district',25);
            $table->timestamps();

            $table->foreign('discart_point_id')->references('id')->on('discart_points')->onDelete('no action')->onUpdate('no action');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discart_point_address');
    }
}
