<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDrugInteraction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drug_interaction', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title_interaction');
            $table->string('degree_interaction')->nullable();
            $table->string('action_start')->nullable();
            $table->text('recommendation')->nullable();
            $table->text('clinical_effect')->nullable();
	        $table->timestamps();
	        $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drug_interaction');
    }
}
