<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSubQuestAnamneses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_quest_anamneses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->integer('id_quest_anamnese')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_quest_anamnese')->references('id')->on('quests_anamneses')->onDelete('no action')->onUpdate('no action');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_quest_anamneses');
    }
}
