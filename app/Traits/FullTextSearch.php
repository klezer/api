<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 03/09/18
 * Time: 10:36
 */

namespace DrPediu\Traits;


trait FullTextSearch

{
	/**
	 * Replaces spaces with full text search wildcards
	 *
	 * @param string $term
	 * @return string
	 */
	protected function fullTextWildcards($term)
	{

		$reservedSymbols = ['-', '+', '<', '>', '@', '(', ')', '~'];
		$term = str_replace($reservedSymbols, '', $term);

		$words = explode(',', $term);

		$result = collect($words);
		$text = null;

		if($result->count() > 1) {

			$result_match = $result->crossJoin($words);

			foreach ($result_match as $key => $word) {

				$unique_combination = collect($word)->unique();

				foreach ($result as $Key => $item) {

					if ($unique_combination->count() > 1) {
						$text .= ':' . '+' . $unique_combination[0] . ' +(>' . $unique_combination[1]  . ' <'.$unique_combination[1] . ')';
					}
				}
			}
			$searchTerm = explode(':', $text);
//			dd($searchTerm);
			return array_filter($searchTerm);
		}
		return null;
	}
}