<?php

namespace DrPediu\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

/**
 * @SWG\Swagger(
 *     schemes={"http","https"},
 *     host=L5_SWAGGER_CONST_HOST,
 *     basePath="/",
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="Dr. Pediu API",
 *     )
 * )
 */

// ====== Security

/**
 * @SWG\SecurityScheme(
 *   securityDefinition="oauth_client_credentials",
 *   type="oauth2",
 *   authorizationUrl="API_TOKEN_AUTHORIZATION_URL",
 *   flow="application",
 * )
 */

/**
 * @SWG\Post(
 *      path="/oauth/token",
 *      summary="Requests a refresh token",
 *      tags={"OAuth"},
 *      operationId="refreshToken",
 *      @SWG\Parameter(
 *          name="grant_type",
 *          in="formData",
 *          description="refresh_token or password",
 *          required=true,
 *          type="string"
 *      ),
 *      @SWG\Parameter(
 *          name="client_id",
 *          in="formData",
 *          description="OAuth Client ID",
 *          required=true,
 *          type="string"
 *      ),
 *      @SWG\Parameter(
 *          name="client_secret",
 *          in="formData",
 *          description="OAuth Client Secret",
 *          required=true,
 *          type="string"
 *      ),
 *      @SWG\Parameter(
 *          name="scope",
 *          in="query",
 *          description="What scopes are requested, for all, use '*'",
 *          required=false,
 *          type="string"
 *      ),
 *      @SWG\Parameter(
 *          name="refresh_token",
 *          in="formData",
 *          description="Refresh_token from the authorization response",
 *          required=false,
 *          type="string"
 *      ),
 *      @SWG\Parameter(
 *          name="username",
 *          in="formData",
 *          description="Username for login",
 *          required=false,
 *          type="string"
 *      ),
 *      @SWG\Parameter(
 *          name="password",
 *          in="formData",
 *          description="Password for the user",
 *          required=false,
 *          type="string"
 *      ),
 *      @SWG\Response(
 *          response=200,
 *          description="successful operation",
 *      )
 * )
 */

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
