<?php

namespace DrPediu\Http\Controllers\ApiAuth;

use DrPediu\Http\Requests\CreateAnamneseRequest;
use DrPediu\Http\Requests\UpdateAnamneseRequest;
use DrPediu\Services\AnamnesesService;
use Illuminate\Http\Request;
use DrPediu\Http\Controllers\Controller;

class AnamneseController extends Controller
{
    private $anamnesesService;

    public function __construct( AnamnesesService $anamnesesService)
    {
        $this->anamnesesService = $anamnesesService;
    }

    public function create(CreateAnamneseRequest $createAnamneseRequest)
    {
        return $this->anamnesesService->create($createAnamneseRequest);
    }
    public function update(UpdateAnamneseRequest $updateAnamneseRequest)
    {
        return $this->anamnesesService->update($updateAnamneseRequest);
    }

    public function getAnswerAnamneseForUser($id)
    {
        return $this->anamnesesService->getAnswerAnamneseForUser($id);
    }
    public function structureAnamnese()
    {
        return $this->anamnesesService->structureAnamnese();
    }

}
