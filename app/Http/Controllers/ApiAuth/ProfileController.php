<?php

namespace DrPediu\Http\Controllers\ApiAuth;

use DrPediu\Http\Requests\CreateProfileApiRequest;
use DrPediu\Services\DirectoryService;
use DrPediu\Services\ProfileService;
use Illuminate\Http\Request;
use DrPediu\Http\Controllers\Controller;

class ProfileController extends Controller
{
    private $profileService;

    public function __construct( ProfileService $profileService)
    {
        $this->profileService = $profileService;
    }
    public function store( CreateProfileApiRequest $createProfileApiRequest)
    {
        return $this->profileService->create($createProfileApiRequest);
    }
    public function getImage($image)
    {
        return  DirectoryService::getImageDiretorio('avatars/',$image);
    }

}
