<?php

namespace DrPediu\Http\Controllers\ApiAuth;

use DrPediu\Http\Requests\CreateTreatmentRequest;
use DrPediu\Http\Requests\UpdateTreatmentRequest;
use DrPediu\Services\StartTreatmentService;
use DrPediu\Http\Controllers\Controller;

class StartTreatmentController extends Controller
{

    private $startTreatmentService;

    public function __construct(StartTreatmentService $startTreatmentService)
    {
        $this->startTreatmentService = $startTreatmentService;
    }

    public function create(CreateTreatmentRequest $createTreatmentRequest)
    {
        return $this->startTreatmentService->create($createTreatmentRequest);
    }
    public function update(UpdateTreatmentRequest $updateTreatmentRequest)
    {
        return $this->startTreatmentService->update($updateTreatmentRequest);
    }
    public function getStartTreatment($user_id)
    {
        return $this->startTreatmentService->getStartTreatment($user_id);
    }
    public function getCalendarTreatment($id)
    {
        return $this->startTreatmentService->getCalendarTreatment($id);
    }

}
