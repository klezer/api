<?php

namespace DrPediu\Http\Controllers\ApiAuth;

use DrPediu\Http\Requests\CreateCommentApiRequest;
use DrPediu\Http\Requests\UpdateCommentPusherRequest;
use DrPediu\Services\CommentService;
use DrPediu\Http\Controllers\Controller;


class CommentController extends Controller
{
    private $commentService;

    public function __construct(CommentService $commentService)
    {
        $this->commentService = $commentService;
    }

    public function createService(CreateCommentApiRequest $createCommentApiRequest)
    {
      return $this->commentService->create($createCommentApiRequest);
    }

    public function updateCommentForPusher(UpdateCommentPusherRequest $updateCommentPusherRequest)
    {
        return $this->commentService->updateCommentForPusher($updateCommentPusherRequest);
    }

}
