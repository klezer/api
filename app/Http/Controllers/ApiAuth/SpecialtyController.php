<?php

namespace DrPediu\Http\Controllers\ApiAuth;

use DrPediu\Services\SpecialtiesService;
use Illuminate\Http\Request;
use DrPediu\Http\Controllers\Controller;

class SpecialtyController extends Controller
{
    private $specialtiesService;

    public function __construct(SpecialtiesService $specialtiesService)
    {
        $this->specialtiesService = $specialtiesService;
    }

    public function listSpecialties()
    {
        return $this->specialtiesService->listSpecialties();
    }
}
