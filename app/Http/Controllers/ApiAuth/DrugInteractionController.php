<?php

namespace DrPediu\Http\Controllers\ApiAuth;

use DrPediu\Services\DrugInteractionService;
use Illuminate\Http\Request;
use DrPediu\Http\Controllers\Controller;

class DrugInteractionController extends Controller
{
	private $drugInteractionService;

    public function __construct(DrugInteractionService $drugInteractionService)
    {
      $this->drugInteractionService = $drugInteractionService;
    }
    public function hasInteractionMedical(Request $request)
    {
    	return $this->drugInteractionService->interaction($request);
    }
}
