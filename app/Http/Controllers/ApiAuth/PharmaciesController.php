<?php

namespace DrPediu\Http\Controllers\ApiAuth;

use DrPediu\Http\Requests\UpdateSalesMedicinesRequest;
use DrPediu\Services\PharmaciesService;
use DrPediu\Http\Controllers\Controller;

class PharmaciesController extends Controller
{
	private $pharmaciesService;

	public function __construct(PharmaciesService $pharmaciesService)
	{
		$this->pharmaciesService = $pharmaciesService;
	}

	public function getRecipeForCPFUser($cpf)
	{
		return  $this->pharmaciesService->getRecipeForCPFUser($cpf);
	}

	public function updateSalesMedicines(UpdateSalesMedicinesRequest $updateSalesMedicinesRequest)
	{
		return $this->pharmaciesService->updateSalesMedicines($updateSalesMedicinesRequest);
	}
}
