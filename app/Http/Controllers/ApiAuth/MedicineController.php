<?php

namespace DrPediu\Http\Controllers\ApiAuth;

use DrPediu\Models\Medicine;
use DrPediu\Services\MedicineService;
use Illuminate\Http\Request;
use DrPediu\Http\Controllers\Controller;

class MedicineController extends Controller
{

    private $medicineService;

    public  function __construct(MedicineService $medicineService)
    {
        $this->medicineService = $medicineService;
    }
    public function listMedicine($search = null)
    {
      return $this->medicineService->listMedicine($search);
    }

}
