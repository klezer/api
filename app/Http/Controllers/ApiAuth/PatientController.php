<?php

namespace DrPediu\Http\Controllers\ApiAuth;

use DrPediu\Services\PatientService;
use DrPediu\Http\Controllers\Controller;

class PatientController extends Controller
{
    protected $patientService;

    public function __construct(PatientService $patientService)
    {
        $this->patientService = $patientService;
    }
    public function getAllUserRecipes($id,$search = null)
    {
        return $this->patientService->getListRecipes($id,$search);

    }
    public function getRecipeUser($id)
    {
        return $this->patientService->getRecipeUser($id);
    }

    public function getAllCommentsRecipes($id,$user)
    {
        return $this->patientService->getListCommetsRecipe($id,$user);
    }
    public function getAllMessage($patientId,$search =null)
    {
        return $this->patientService->getAllMessage($patientId,$search);
    }
    public function getListMyDoctors($id,$search = null)
    {
        return $this->patientService->getListMyDoctors($id,$search);
    }
    public function getRecipeInfoDoctor($id)
    {
        return $this->patientService->getRecipeInfoDoctor($id);
    }
    public function getSlicesRecipesForUser($id)
    {
        return $this->patientService->getSlicesRecipesForUser($id);
    }
    public function listMedicalsRecipeForDoctor($doctor_id,$user_id)
    {
        return $this->patientService->listMedicalsRecipeForDoctor($doctor_id,$user_id);
    }
    public function getListRecipesPatient($patient_id,$doctor_id)
    {
        return $this->patientService->getListRecipesPatient($patient_id,$doctor_id);
    }

}
