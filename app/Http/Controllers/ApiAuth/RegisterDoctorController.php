<?php

namespace DrPediu\Http\Controllers\ApiAuth;

use DrPediu\Http\Requests\CreateCadDoctorRequest;
use DrPediu\Http\Requests\StoreUserForCpfRequest;
use DrPediu\Http\Requests\UpdateCadDoctorRequest;
use DrPediu\Services\RegisterDoctorService;
use Illuminate\Http\Request;
use DrPediu\Http\Controllers\Controller;
use DrPediu\Services\DirectoryService;

class RegisterDoctorController extends Controller
{

    private $registerDoctorService;

    public function __construct( RegisterDoctorService $registerDoctorService)
    {
        $this->registerDoctorService = $registerDoctorService;
    }

    public function create(CreateCadDoctorRequest $createCadDoctorRequest)
    {
        return  $this->registerDoctorService->createService($createCadDoctorRequest);
    }
    public function update(UpdateCadDoctorRequest $updateCadDoctorRequest)
    {
        return $this->registerDoctorService->updateService($updateCadDoctorRequest);
    }
    public function getRegisterDoctor($id)
    {
        return $this->registerDoctorService->getRegisterDoctor($id);
    }
    public function getDocumentProfessional($image)
    {
        return  DirectoryService::getImageDiretorio('assinatura/',$image);
    }
    public function getRegisterUserForCpf($cpf)
    {
        return $this->registerDoctorService->getRegisterUserForCpf($cpf);
    }
    public function getRegisterUserForId($id)
    {
        return $this->registerDoctorService->getRegisterUserForId($id);
    }
    public function storeUserForCpf(StoreUserForCpfRequest $storeUserForCpfRequest)
    {
        return $this->registerDoctorService->storeUserForCpf($storeUserForCpfRequest);
    }

}
