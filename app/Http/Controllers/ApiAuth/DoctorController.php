<?php

namespace DrPediu\Http\Controllers\ApiAuth;


use DrPediu\Http\Requests\CreateRecipeRequest;
use DrPediu\Services\DoctorInfoService;
use DrPediu\Services\DoctorService;
use DrPediu\Http\Controllers\Controller;

class DoctorController extends Controller
{

    private $doctorRecipesService;
    private $doctorInfoService;

    public function __construct(DoctorService $doctorRecipesService,DoctorInfoService $doctorInfoService)
    {
        $this->doctorRecipesService  = $doctorRecipesService;
        $this->doctorInfoService = $doctorInfoService;
    }

    public function create( CreateRecipeRequest $createCadDoctorRequest)
    {
        return  $this->doctorRecipesService->create($createCadDoctorRequest);
    }

    public function getListRecipes($id,$search = null)
    {
        return $this->doctorRecipesService->getListRecipes($id,$search);
    }
    public function getListMedicalPatient($id,$search = null)
    {
        return $this->doctorRecipesService->getListMedicalPatient($id,$search);
    }
    public function getListRecipesPatient($patient_id,$doctor_id)
    {
        return $this->doctorRecipesService->getListRecipesPatient($patient_id,$doctor_id);
    }
    public function getAllMessage($doctor_id,$search =null)
    {
        return $this->doctorRecipesService->getAllMessage($doctor_id,$search);
    }

    public function getRecipeIfoPatient($id)
    {
        return $this->doctorRecipesService->getRecipeIfoPatient($id);
    }
    public function getDoctorInfoCad($id)
    {
        return $this->doctorInfoService->getInfoDoctorForId($id);
    }



}
