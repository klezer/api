<?php

namespace DrPediu\Http\Controllers\ApiAuth;


use DrPediu\Services\DiscartPointsService;
use DrPediu\Http\Controllers\Controller;

class DiscartPointsController extends Controller
{

    private $discartPointsService;

    public function __construct(DiscartPointsService $discartPointsService)
    {
        $this->discartPointsService = $discartPointsService;
    }

    public function list()
    {
       return $this->discartPointsService->list();
    }
}
