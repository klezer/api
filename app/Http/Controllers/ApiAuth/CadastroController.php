<?php

namespace DrPediu\Http\Controllers\ApiAuth;


use DrPediu\Http\Controllers\Controller;
use DrPediu\Services\CadastroService;
use DrPediu\Http\Requests\UpdateCadastroApiRequest;
use DrPediu\Http\Requests\CreateCadastroApiRequest;

class CadastroController extends Controller
{

    protected $cadastroService;


    public function __construct(CadastroService $cadastroService)
    {
        $this->cadastroService = $cadastroService;
    }

    public function create(CreateCadastroApiRequest $cadastroApiRequest)
    {
      return $this->cadastroService->createService($cadastroApiRequest);
    }

    public function update(UpdateCadastroApiRequest $updateCadastroApiRequest)
    {
      return $this->cadastroService->updateService($updateCadastroApiRequest);
    }
    public function getCitys($id)
    {
        return $this->cadastroService->citys($id);
    }
    public function getStates()
    {
      return $this->cadastroService->states();
    }
    public function getRegister($id)
    {
     return $this->cadastroService->getRegisterUser($id);
    }

    public function sendToken($id)
    {
        $user = $this->user->find($id);
        $success['token'] =  $user->createToken('Dr.Pediu')->accessToken;

        return response()->json(['success' => $success], 200);
    }


}
