<?php

namespace DrPediu\Http\Controllers\ApiAuth;

use DrPediu\Services\GeneratorPdfService;
use Illuminate\Http\Request;
use DrPediu\Http\Controllers\Controller;

class GeneratePdfController extends Controller
{
    private $generatorPdfService;

    public function __construct(GeneratorPdfService $generatorPdfService)
    {
        $this->generatorPdfService = $generatorPdfService;
    }
    public function generatePdfRecipe($id)
    {
      return  $this->generatorPdfService->generatePdfRecipe($id);
    }

}
