<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/06/18
 * Time: 17:24
 */

namespace DrPediu\Http\Controllers\ApiAuth;

use DrPediu\Http\Controllers\Controller;
use DrPediu\Http\Requests\LoginApiRequest;
use DrPediu\Http\Requests\UpdateLoginApiRequest;
use DrPediu\Mail\VerificAuth;
use DrPediu\Services\DirectoryService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DrPediu\Models\User;


class LoginController extends Controller
{

    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request

     * @return Response
     */
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function authenticate(LoginApiRequest $loginApiRequest)
    {
        $credentials = $loginApiRequest->only('email', 'password');

        if (auth()->guard('web')->attempt($credentials)) {

            $user = [
                'name'  => auth()->user()->name,
                'cpf'   => auth()->user()->cpf,
                'rg'    => auth()->user()->rg,
                'email' => auth()->user()->email,
                'cidade'=> auth()->user()->user_address->city->title,
                'uf'    => auth()->user()->user_address->city->state->initials,
            ];

            if(auth()->user()->profiles) {
                $user['image'] = DirectoryService::getImageDefaultIfNotImage(auth()->user()->genre,auth()->user()->profiles->image);
            }else{
                $user['image'] = DirectoryService::getImageDefaultIfNotImage(auth()->user()->genre,null);
            }
            if(auth()->user()->doctor) {

                $user['is_doctor'] = true;
                $user['id'] = auth()->user()->doctor->user_id;

                auth()->user()->update([
                    'device_token' => $loginApiRequest->input('device_token','null')
                ]);

            }else{

                $user['is_doctor'] = false;
                $user['id'] = auth()->user()->id;
                auth()->user()->update([
                    'device_token' => $loginApiRequest->input('device_token','null')
                ]);
            }

            return response()->json(['success' => $user], 200);
        }
        return response()->json(['errors' => ['fields' => ['usuário ou senha inválidos!']]]);
    }

    public function auth(Request $request)
    {
        # metodo para contrução do disparo para o email ainda em construção

        $credentials = $request->only('email', 'password');
        if (auth()->guard('web')->attempt($credentials)) {

            $user = auth()->user();
            $success['token'] =  $user->createToken('Dr.Pediu')->accessToken;

            return response()->json(['success' => $success], 200);
        }
        return response()->json(['status' => 'usuario não existe!']);
    }

    public function update(UpdateLoginApiRequest $updateLoginApiRequest)
    {
        $user = $this->user->find($updateLoginApiRequest->user_id);

        if($user){
            $user->update([
                'password' => $updateLoginApiRequest->password?bcrypt($updateLoginApiRequest->password):auth()->user()->password
            ]);
            return response()->json(['success' => 'Registro atualizado com sucesso!'],200);
        }
        return response()->json(['error' => 'Este usuário não existe em nossa base!'],404);
    }

}