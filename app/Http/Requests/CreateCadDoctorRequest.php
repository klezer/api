<?php

namespace DrPediu\Http\Requests;

use DrPediu\Http\Requests\Traits\CustomErrorJsonBody;
use DrPediu\Traits\FormatReturnMenssages;
use Illuminate\Foundation\Http\FormRequest;


class CreateCadDoctorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    use FormatReturnMenssages;

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'cpf' => 'required|max:11|unique:users|cpf',
            'rg' => 'required|unique:users',
            'crm_number' => 'required|unique:doctors',
            'password' => 'required|string|min:6|confirmed',
            'city_id' => 'required',
            #'document_professional' => 'required',
            'state_id' => 'required',
            'specialty_id' => 'required',
            'number' => 'required',
            'genre' => 'required|max:1',
            'district_title' => 'required',
            'street_title' => 'required',
            'street_code' => 'required'
        ];
    }
    public function messages()
    {
        return  [

            'name.required' => 'O campo nome é obrigatório!',
            'email.required' => 'O campo email é obrigatório!',
            'email.email' => 'O email digitado é inválido!',
            'email.unique' => 'Este email já se encontra em nossa base!',
            'cpf.unique' => 'Este cpf já se encontra em nossa base!',
            'crm_number.unique' => 'Este crm já se encontra em nossa base!',
            'crm_number.required' => 'O campo crm é obrigatório!',
            'cpf.required' => 'O campo cpf é obrigatório!',
            'cpf.max' => 'Você deve enviar no máximo 11 caracteres!',
            'rg.required' => 'O campo rg é obrigatório!',
            'specialty_id.required' => 'O campo specialty é obrigatório!',
            'rg.unique' => 'Este rg já se encontra em nossa base!',
            'password.required' => 'O campo password é obrigatório!',
            'password.confirmed' => 'A senha digitada não são iguais favor verificar o ocorrido!',
            'password.min' => 'Você digitar no minimo 6 caracteres!',
            'number.required' => 'O campo número é obrigatório!',
            'city_id.required' => 'O campo cidade é obrigatório!',
            #'document_professional.required' => 'O campo document_professional é obrigatório!',
            'state_id.required' => 'O campo estado é obrigatório!',
            'district_title.required' => 'O campo bairro é obrigatório!',
            'genre.required' => 'O campo gênero é obrigatório!',
            'genre.max' => 'Você deve enviar no máximo 1 caracter!',
            'street_title.required' => 'O campo endereço é obrigatório!',
            'street_code.required' => 'O campo cep é obrigatório!',

        ];
    }

}
