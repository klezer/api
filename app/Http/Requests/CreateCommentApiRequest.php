<?php

namespace DrPediu\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateCommentApiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'recipes_id' => 'required',
            'user_id' => 'required',
            'comments' => 'required'
        ];
    }

    public function messages()
    {
        return  [
            'recipes_id.required' => 'O campo recipes_id é obrigatório!',
            'user_id.required' => 'O campo user_id é obrigatório!',
            'comments.required' => 'O campo comments é obrigatório!',
        ];
    }
}
