<?php

namespace DrPediu\Http\Requests;

use DrPediu\Traits\FormatReturnMenssages;
use Illuminate\Foundation\Http\FormRequest;

class StoreUserForCpfRequest extends FormRequest
{

    use FormatReturnMenssages;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cpf' => 'required|max:11|unique:users|cpf',
        ];
    }
    public function messages()
    {
        return [
            'cpf.unique' => 'Este cpf já se encontra em nossa base!',
            'cpf.required' => 'O campo cpf é obrigatório!',
        ];
    }
}
