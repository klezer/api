<?php

namespace DrPediu\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateTreatmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'medicals' => 'required',
            'user_id' => 'required',
            'notifications' => 'required',
        ];
    }
    public function messages()
    {
        return [
          'medicals.required' => 'O campo medicine_id é obrigatório!',
          'user_id.required' => 'O campo user_id é obrigatório!',
          'notifications.required' => 'O campo notifications é obrigatório!',
        ];
    }
}
