<?php

namespace DrPediu\Http\Requests;

use DrPediu\Traits\FormatReturnMenssages;
use Illuminate\Foundation\Http\FormRequest;

class UpdateSalesMedicinesRequest extends FormRequest
{
	use FormatReturnMenssages;
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'medicine_id' => 'required'
		];
	}
	public function messages()
	{
		return [
			'medicine_id.required' => 'O campo medicine_id é obrigatório!'
		];
	}
}
