<?php

namespace DrPediu\Http\Requests;

use DrPediu\Traits\FormatReturnMenssages;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;


class CreateCadastroApiRequest extends FormRequest
{

    use FormatReturnMenssages;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'cpf' => 'required|max:11|unique:users|cpf',
            'rg' => 'required|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'city_id' => 'required',
            'date_of_birth' => 'required|date',
            'state_id' => 'required',
            'number' => 'required',
            'genre' => 'required|max:1',
            'district_title' => 'required',
            'street_title' => 'required',
            'street_code' => 'required'

        ];
    }

    public function messages()
    {
        return  [

            'name.required' => 'O campo nome é obrigatório!',
            'email.required' => 'O campo email é obrigatório!',
            'email.email' => 'O email digitado é inválido!',
            'email.unique' => 'Este email já se encontra em nossa base!',
            'cpf.unique' => 'Este cpf já se encontra em nossa base!',
            'cpf.required' => 'O campo cpf é obrigatório!',
            'cpf.max' => 'Você deve enviar no máximo 11 caracteres!',
            'date_of_birth.date' => 'O formato da data de nascimento está incorreto!',
            'date_of_birth.required' => 'O campo date é obrigatório!',
            'rg.required' => 'O campo rg é obrigatório!',
            'rg.unique' => 'Este rg já se encontra em nossa base!',
            'password.required' => 'O campo password é obrigatório!',
            'password.confirmed' => 'A senha digitada não são iguais favor verificar o ocorrido!',
            'password.min' => 'Você digitar no minimo 6 caracteres!',
            'number.required' => 'O campo número é obrigatório!',
            'city_id.required' => 'O campo cidade é obrigatório!',
            'state_id.required' => 'O campo estado é obrigatório!',
            'district_title.required' => 'O campo bairro é obrigatório!',
            'genre.required' => 'O campo gênero é obrigatório!',
            'genre.max' => 'Você deve enviar no máximo 1 caracter!',
            'street_title.required' => 'O campo endereço é obrigatório!',
            'street_code.required' => 'O campo cep é obrigatório!',

        ];
    }


}
