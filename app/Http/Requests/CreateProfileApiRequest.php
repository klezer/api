<?php

namespace DrPediu\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateProfileApiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => 'required',
            #'image' => 'required|image|mimes:jpg,png,jpeg|max:400',
            'user_id' => 'required|unique:profiles'
        ];
    }

    public function messages()
    {
        return [
            'image.max' => 'A imagem não pode ser maior que 2000 kilobytes.',
            'image.mimes' => 'A imagem deve ser um arquivo do tipo: jpg, png, jpeg.',
            'image.image' => 'O arquivo enviado deve ser uma imagem.',
            'image.required' => 'O envio da imagem é obrigatório.',
            'user_id.required' => 'O envio do user_id é obrigatório.',
            'user_id.unique' => 'Esse usuário já existe!'
        ];
    }

}
