<?php

namespace DrPediu\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAnamneseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'user_id' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'user_id.required' => 'O user_id é obrigatório!',
        ];
    }
}
