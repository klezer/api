<?php

namespace DrPediu\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCadDoctorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            #'email' => 'required|string|email|max:255|unique:users,email,'.$this->get('id'),
            'cpf' => 'required|cpf',
            'rg' => 'required|unique:users,id,'.$this->get('id'),
            'crm_number' => 'required|unique:doctors,id,'.$this->get('id'),
            'city_id' => 'required',
            'state_id' => 'required',
            'specialty_id' => 'required',
            'number' => 'required',
            'genre' => 'required',
            'district_title' => 'required',
            'street_title' => 'required',
            'street_code' => 'required',
            'address_id' => 'required',
            'user_id' => 'required'
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'O campo nome é obrigatório!',
            'email.required' => 'O campo email é obrigatório!',
            'email.email' => 'O email digitado é inválido!',
            'email.unique' => 'Este email já se encontra em nossa base!',
            'cpf.unique' => 'Este cpf já se encontra em nossa base!',
            'cpf.required' => 'O campo cpf é obrigatório!',
            'specialty_id.required' => 'O campo specialty é obrigatório!',
            'rg.required' => 'O campo rg é obrigatório!',
            'rg.unique' => 'Este rg já se encontra em nossa base!',
            'number.required' => 'O campo número é obrigatório!',
            'city_id.required' => 'O campo cidade é obrigatório!',
            'state_id.required' => 'O campo estado é obrigatório!',
            'district_title.required' => 'O campo bairro é obrigatório!',
            'genre.required' => 'O campo gênero é obrigatório!',
            'street_title.required' => 'O campo endereço é obrigatório!',
            'street_code.required' => 'O campo cep é obrigatório!',
            'address_id.required' => 'O campo address_id é obrigatório!',
            'user_id.required' => 'O campo user_id é obrigatório!',

        ];
    }
}
