<?php

namespace DrPediu\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTreatmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required',
            'taken' => 'required'
        ];
    }
    public function messages()
    {
        return [
            'id.required' => 'o campo id é obrigatório!',
            'taken.required' => 'o campo taken é obrigatório!',
        ];
    }
}
