<?php

namespace DrPediu\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class UpdateCadastroApiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'name' => 'required|string|max:255',
                'cpf' => 'required|cpf',
                'rg' => 'required|unique:users,id,'.$this->get('id'),
                'city_id' => 'required',
                'state_id' => 'required',
                'number' => 'required',
                'genre' => 'required',
                'district_title' => 'required',
                'street_title' => 'required',
                'street_code' => 'required',
                'address_id' => 'required',
                'user_id' => 'required'
        ];
    }
    public function messages()
    {
        return [
                'name.required' => 'O campo nome é obrigatório!',
                'email.required' => 'O campo email é obrigatório!',
                'email.email' => 'O email digitado é inválido!',
                'email.unique' => 'Este email já se encontra em nossa base!',
                'cpf.unique' => 'Este cpf já se encontra em nossa base!',
                'cpf.required' => 'O campo cpf é obrigatório!',
                'rg.required' => 'O campo rg é obrigatório!',
                'rg.unique' => 'Este rg já se encontra em nossa base!',
                'number.required' => 'O campo número é obrigatório!',
                'city_id.required' => 'O campo cidade é obrigatório!',
                'state_id.required' => 'O campo estado é obrigatório!',
                'district_title.required' => 'O campo bairro é obrigatório!',
                'genre.required' => 'O campo gênero é obrigatório!',
                'street_title.required' => 'O campo endereço é obrigatório!',
                'street_code.required' => 'O campo cep é obrigatório!',
                'address_id.required' => 'O campo id do endereço é obrigatório!',
                'user_id.required' => 'O campo id do usuário é obrigatório!',

            ];
    }
    public function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(response()->json(['errors' => $errors
        ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }
}
