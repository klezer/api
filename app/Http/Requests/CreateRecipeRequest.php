<?php

namespace DrPediu\Http\Requests;

use DrPediu\Traits\FormatReturnMenssages;
use Illuminate\Foundation\Http\FormRequest;

class CreateRecipeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    use FormatReturnMenssages;

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required',
            'doctor_id' => 'required',
            'type_recipe_id' => 'required',
            'medicines.*.dosage' => 'required',
            'medicines.*.frequency' => 'required',
            'medicines.*.validity_of_medicine' => 'required',
            'medicines.*.title' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'user_id' => 'O campo user_id é obrigatório!',
            'doctor_id' => 'O campo doctor_id é obrigatório!',
            'type_recipe_id' => 'O campo type_recipe_id é obrigatório!',
            'medicines.*.dosage.required' => 'O campo dosage é obrigatório!',
            'medicines.*.frequency.required' => 'O campo frequency é obrigatório!',
            'medicines.*.validity_of_medicine.required' => 'O campo validity_of_medicine é obrigatório!',
            'medicines.*.title.required' => 'O campo title é obrigatório!',
        ];
    }
}
