<?php

namespace DrPediu\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class LoginApiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'required|string|min:6',
            'email' => 'required|string|email|max:255',
        ];
    }

    public function messages()
    {
        return [
            'password.required' => 'O campo password é obrigatório!',
            'password.unique' => 'Este rg já se encontra em nossa base!',
            'password.min' => 'Você deverá digitar no mínimo 6 caracteres!',
            'email.required' => 'O campo email é obrigatório!',
            'email.email' => 'O email digitado é inválido!',
            'email.max' => 'O email digitado deverá conter no máximo 255 caracteres!',
        ];
    }
    public function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(response()->json(['errors' => $errors
        ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }
}
