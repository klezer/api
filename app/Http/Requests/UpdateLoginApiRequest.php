<?php

namespace DrPediu\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateLoginApiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'required|string|min:6|confirmed',
            'user_id'  => 'required'
        ];
    }
    public function messages()
    {
        return [
            'password.required' => 'O campo password é obrigatório!',
            'user_id.required' => 'O campo user_id é obrigatório!',
            'password.unique' => 'Este rg já se encontra em nossa base!',
            'password.confirmed' => 'A senha digitada não são iguais favor verificar o ocorrido!',
            'password.min' => 'Você deverá digitar no minimo 6 caracteres!',
        ];
    }
}
