<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 28/06/18
 * Time: 14:37
 */

namespace DrPediu\Services;
use DrPediu\Models\City;
use DrPediu\Models\Doctor;
use DrPediu\Models\State;
use DrPediu\Models\User;
use DrPediu\Models\UserAddress;
use http\Env\Request;
use http\Env\Response;
use DrPediu\Util\Util;


class RegisterDoctorService
{

	private $user;
	private $userAddress;
	private $doctor;
	private $directoryService;

	public function __construct(User $user, UserAddress $userAddress, Doctor $doctor, DirectoryService $directoryService)
	{
		$this->user = $user;
		$this->userAddress = $userAddress;
		$this->doctor  = $doctor;
		$this->directoryService = $directoryService;
	}

	public function createService($cadastroApiRequest)
	{
		\DB::beginTransaction();

		try {

			$user = [
				'name' => $cadastroApiRequest->input('name'),
				'email' => $cadastroApiRequest->input('email'),
				'password' => bcrypt($cadastroApiRequest->input('password')),
				'cpf' => $cadastroApiRequest->input('cpf'),
				'rg' => $cadastroApiRequest->input('rg'),
				'genre' => $cadastroApiRequest->input('genre'),
				'date_of_birth' => Util::formatStringDateToIso($cadastroApiRequest->input('date_of_birth')),
			];

			$last_insert_user = $this->user->create($user);


			$user_address = [
				'user_id' => $last_insert_user->id,
				'street_title' => $cadastroApiRequest->input('street_title'),
				'city_id' => $cadastroApiRequest->input('city_id'),
				'locality' => $cadastroApiRequest->input('locality'),
				'district_title' => $cadastroApiRequest->input('district_title'),
				'complement' => $cadastroApiRequest->input('complement', null),
				'number' => $cadastroApiRequest->input('number'),
				'street_code' => $cadastroApiRequest->input('street_code')
			];

			$last_insert_user->user_addresses()->create($user_address);

			$user_contacts = [
				'user_id' => $last_insert_user->id,
				'telephone_one' => $cadastroApiRequest->input('telephone_one', null),
				'telephone_two' => $cadastroApiRequest->input('telephone_two', null),
				'cell_phone' => $cadastroApiRequest->input('cell_phone', null),

			];

			if ($cadastroApiRequest->hasFile('document_professional')) {

				$cadastroApiRequest->file('document_professional')->store('public/assinatura');
				$data = ['document_professional' => $cadastroApiRequest->file('document_professional')->hashName()];
			}
			if ($cadastroApiRequest->hasFile('signature_image')) {

				$cadastroApiRequest->file('signature_image')->store('public/assinatura');
				$data = ['signature_image' => $cadastroApiRequest->signature_image->hasName()];
			}
			$data  = [
				'user_id' => $last_insert_user->id,
				'crm_number' => $cadastroApiRequest->input('crm_number'),
			];

			$last_insert_doctor = $this->doctor->create($data);

			$last_insert_doctor->pivot_doctors_x_specialties()->create([
				'specialty_id' => $cadastroApiRequest->input('specialty_id'),
				'doctor_id' => $last_insert_doctor->id,
			]);

			$last_insert_user->contacts()->create($user_contacts);

			\DB::commit();

			return response()->json([
					'id' => $last_insert_user->id,
					'success' => 'Registro criado com sucesso!'
				]
			);


		}catch (\Exception $e){
			\DB::rollback();
			return $e->getMessage();

		}
	}

	public function updateService($updateCadastroApiRequest)
	{
		$user = $this->user->find($updateCadastroApiRequest->user_id);

		$user_address  = $user->user_addresses->where('id',$updateCadastroApiRequest->input('address_id'))->first();

		\DB::beginTransaction();

		try {

			if($user_address) {


				$user_address = [
					'user_id' => $user->id,
					'street_title' => $updateCadastroApiRequest->input('street_title'),
					'city_id' => $updateCadastroApiRequest->input('city_id'),
					'locality' => $updateCadastroApiRequest->input('locality'),
					'district_title' => $updateCadastroApiRequest->input('district_title'),
					'complement' => $updateCadastroApiRequest->input('complement', null),
					'number' => $updateCadastroApiRequest->input('number'),
					'street_code' => $updateCadastroApiRequest->input('street_code')
				];

				$user->user_addresses()->update($user_address);

				$user_contacts = [
					'user_id' => $user->id,
					'telephone_one' => $updateCadastroApiRequest->input('telephone_one', null),
					'telephone_two' => $updateCadastroApiRequest->input('telephone_two', null),
					'cell_phone' => $updateCadastroApiRequest->input('cell_phone', null),

				];

				$user->contacts()->update($user_contacts);

				$user_data = [
					'name' => $updateCadastroApiRequest->input('name'),
					'cpf' => $updateCadastroApiRequest->input('cpf'),
					'rg' => $updateCadastroApiRequest->input('rg'),
					'genre' => $updateCadastroApiRequest->input('genre'),
					'password' => ($updateCadastroApiRequest->input('password'))?bcrypt($updateCadastroApiRequest->input('password')):$user->password,
				];

				if ($updateCadastroApiRequest->hasFile('document_professional')) {

					$updateCadastroApiRequest->file('document_professional')->store('public/assinatura');

					$data = [
						'document_professional' => $updateCadastroApiRequest->file('document_professional')->hashName(),
					];
					$this->directoryService->getDeleteImageDiretorio('assinatura/',$user->doctor->document_professional);
					$this->doctor->where('user_id',$user->id)->update($data);
				}
				if ($updateCadastroApiRequest->hasFile('signature_image')) {

					$updateCadastroApiRequest->file('signature_image')->store('public/assinatura');

					$data = [
						'signature_image' => $updateCadastroApiRequest->signature_image->hashName()
					];
					$this->directoryService->getDeleteImageDiretorio('assinatura/',$user->doctor->signature_image);
					$this->doctor->where('user_id',$user->id)->update($data);
				}

				$doctor =  $this->doctor->where('user_id',$user->id)->first();
				$doctor->pivot_doctors_x_specialties()->update(['specialty_id' => $updateCadastroApiRequest->specialty_id]);
				$doctor->update([
					'crm_number' => $updateCadastroApiRequest->crm_number,
				]);

				$user->update($user_data);

				\DB::commit();

				return response()->json(['success' => 'Registro atualizado com sucesso!']);
			}


		}catch (\Exception $e){
			\DB::rollback();
			return $e->getMessage();

		}

		return response()->json(['success' => 'Registro atualizado com sucesso!']);

	}

	public function getRegisterDoctor($id)
	{
		$user = $this->user->find($id);
		$data = [];

		$data['personal'] = [
			'name' => $user->name,
			'cpf' => $user->cpf,
			'email' => $user->email,
			'rg' => $user->rg,
			'date_of_birth' => $user->date_of_birth->format('d/m/Y'),
			'crm_number' =>  $user->doctor->crm_number,
			'specialty' =>  $user->doctor->pivot_doctors_x_specialties->first()->specialty_id,
			'genre' => $user->genre
		];

		$contacts = $user->contacts()->first();

		$data['contact'] = [
			'telephone_one' => $contacts->telephone_one,
			'telephone_two' => $contacts->telephone_two,
			'cell_phone' => $contacts->cell_phone,
		];

		$user_address = $user->user_addresses()->first();

		$data['address'] = [
			'address_id' => $user_address->id,
			'locality' => $user_address->locality,
			'city_id' => $user_address->city_id,
			'state_id' => $user_address->city->state_id,
			'number' => $user_address->number,
			'complement' => $user_address->complement,
			'street_code' => $user_address->street_code,
			'street_title' => $user_address->street_title,
			'district_title' => $user_address->district_title
		];

		$data['picture'] = [
			'avatar' =>  DirectoryService::getImageDefaultIfNotImage($user->genre,$user->profiles->image),
			'document_professional' =>  DirectoryService::getDocumentProfessional($user->doctor->document_professional)
		];


		return response()->json(['data' => $data]);
	}

	public function getRegisterUserForId($id)
	{
		$user = $this->user->find($id);
		$data = [];

		$data['personal'] = [
			'user_id' => $user->id,
			'name' => $user->name,
			'email' => $user->email,
			'cpf' => $user->cpf,
			'rg' => $user->rg,
			'genre' => $user->genre
		];

		$contacts = $user->contacts()->first();

		$data['contact'] = [
			'telephone_one' => $contacts->telephone_one,
			'telephone_two' => $contacts->telephone_two,
			'cell_phone' => $contacts->cell_phone,
		];

		$user_address = $user->user_addresses()->first();

		$data['address'] = [
			'city_id' => $user_address->city->title,
			'state_id' => $user_address->city->state->title,
			'number' => $user_address->number,
			'complement' => $user_address->complement,
			'street_code' => $user_address->street_code,
			'street_title' => $user_address->street_title,
			'district_title' => $user_address->district_title
		];

		$data['picture'] = [
			'image' =>  DirectoryService::getImageDefaultIfNotImage(
				$user->genre,
				(isset($user->profiles->image)?$user->profiles->image:'')
			)
		];

		return response()->json(['data' => $data]);
	}
	public function getRegisterUserForCpf($cpf)
	{
		$data = [];
		$user = $this->user->where('cpf',$cpf)->first();

		if($user) {
			$data['personal'] = [
				'user_id' => $user->id,
				'name' => $user->name,
				'cpf' => $user->cpf,
				'email' => $user->email,
				'rg' => $user->rg,
				'date_of_birth' => $user->date_of_birth->format('d/m/Y'),
				'genre' => $user->genre,
				'picture' =>  DirectoryService::getImageDefaultIfNotImage(
					$user->genre,
					(isset($user->profiles->image)?$user->profiles->image:'')
				)
			];

			return response()->json(['data' => $data]);
		}
		return $this->storeUserForCpf($cpf);
	}

	public function storeUserForCpf($request)
	{

		$user = $this->user->create([
			'name' => 'Pseudo Paciente',
			'cpf' => $request,
			'rg' => '00000000001',
			'genre' => 'M',
			'date_of_birth' => now(),
			'email' => $request . '@patiente.com.br',
			'password' => bcrypt('123456')
		]);

		$user->contacts()->create([
			'telephone_one' => '(11) 90000010'
		]);

		$user->user_address()->create([
			'city_id' => 25,
			'number' => 001,
			'locality' => 'Casa',
			'complement' => 'casa 1',
			'street_code' => '07085310',
			'street_title' => 'rua das joias',
			'district_title' => 'São Paulo'
		]);

		return $this->getRegisterUserForCpf($request);

	}

}