<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/02/19
 * Time: 09:55
 */

namespace DrPediu\Services;


use Carbon\Carbon;
use DrPediu\Helpers\CalcDateRecipe;
use DrPediu\Models\StartTreatment;
use http\Env\Response;

class StartTreatmentService
{

    private $startTreatment;

    public function __construct(StartTreatment $startTreatment)
    {
        $this->startTreatment = $startTreatment;
    }
    public function create($request)
    {
        $medicines = $request->medicals;

        foreach ($medicines as $medicine) {

            $this->startTreatment->create([
                'user_id' => $request->user_id,
                'start_period' => Carbon::now(),
                'end_period' => CalcDateRecipe::setDateExpiration($medicine['number_of_days']),
                'medicine_id' => $medicine['id'],
                'schedule_generate' => false,
                'notifications' => (boolean)$request->notifications
            ]);
        }
        return response()->json(['success' => 'Registro criado com sucesso!']);
    }
    public function update($request)
    {
        try
        {
            $this->startTreatment->findOrFail($request->id)->update([
                'taken_period' => Carbon::now(),
                'taken' => $request->taken
            ]);

            return  response()->json(['status' => 'Tratamento iniciado com sucesso!']);

        }catch (\Exception $exception){
            return response()->json(['error' => 'Registro não encontrado']);
        }
    }
    public function getStartTreatment($user_id)
    {
        $data = [];

        $treatments =  $this->startTreatment->with(
            'medicine:id,title,dosage,subtitle,frequency,number_of_days',
            'medicine.recipes.users',
            'medicine.recipes.doctors.user',
            'medicine.recipes.doctors.pivot_doctors_x_specialties.specialties_medical',
            'medicine.recipes.doctors.user.profiles'
        )->where('user_id',$user_id)->get();

        foreach ($treatments as $treatment){

            if($treatment->start_period->format('Y-m-d') == Carbon::now()->format('Y-m-d'))

                $data[] = collect([
                    'id' => $treatment->id,
                    'title' => $treatment->medicine->title,
                    'doctor_name' => $treatment->medicine->recipes->first()->doctors->first()->user->name,
                    'doctor_image' =>  DirectoryService::getImageDefaultIfNotImage(
                        $treatment->medicine->recipes->first()->doctors->first()->user->genre,
                        $treatment->medicine->recipes->first()->doctors->first()->user->profiles->image),
                    'doctor_especialty' => $treatment->medicine->recipes->first()
                        ->doctors->first()
                        ->pivot_doctors_x_specialties->first()
                        ->specialties_medical->title,
                    'hour_medical' => $treatment->start_period->format('H:i'),
                    'dosage' => $treatment->medicine->dosage,
                    'frequency' => $treatment->medicine->frequency,
                    'schedule_generate' => $treatment->schedule_generate,
                    'number_of_days' => $treatment->medicine->number_of_days,
                    'description' => $treatment->medicine->subtitle,
                    'taken' => $treatment->taken,
                    'time_line' => CalcDateRecipe::calcTimeLineExpiration(
                        $treatment->end_period,
                        ($treatment->medicine->number_of_days)
                    )
                ]);
        }

        return response()->json(['data' => $data]);
    }

    public function getCalendarTreatment($id)
    {
        $data = [];

        $treatments =  $this->startTreatment->with(
            'medicine:id,title,dosage,subtitle,frequency,number_of_days'
        )->where('user_id',$id)->get();

        foreach ($treatments as $treatment){
            $data[] = collect([
                'id' => $treatment->id,
                'title' => $treatment->medicine->title,
                'start_period' => $treatment->start_period->format('U'),
            ]);
        }

        return response()->json(['data' => $data]);
    }

}