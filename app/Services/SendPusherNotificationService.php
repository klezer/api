<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 18/01/19
 * Time: 16:21
 */

namespace DrPediu\Services;


use DrPediu\Curl\Curl;

class SendPusherNotificationService
{

    private $token;
    private $title;
    private $color;
    private $subtitle;
    private $body;
    private $thumbnail;
    private $category;
    private $recipe_id;
    private $patient_id;
    private $curl;


    public function __construct(Curl $curl)
    {
        $this->curl = $curl;
    }


    public function notification($data)
    {
        $fcmUrl = env('FIREBASE_CLOUD_MESSAING_URL');
        $fcm = $data['fcm'];

        $this->token = $data['token'];
        $this->title = $data['title'];
        $this->color = $data['color'];
        $this->subtitle = $data['subtitle'];
        $this->body = $data['body'];
        $this->thumbnail = $data['thumbnail'];
        $this->category = $data['category'];
        $this->recipe_id = $data['recipe_id'];
        $this->patient_id = (isset($data['patient_id'])?$data['patient_id']:null);

        $data = [
            "title" => $this->title,
            "subtitle" => $this->subtitle,
            "body" => $this->body,
            "thumbnail" => $this->thumbnail,
            "category" => $this->category,
            "recipe_id" => $this->recipe_id,
            'patient_id' => $this->patient_id,
        ];

        $fcmNotification = [
            'to'   =>  $this->token,
            'data' => $data
        ];

        $headers = [
            'Authorization: key='. $fcm,
            'Content-Type: application/json'
        ];

        $this->curl->exeCurl([
            CURLOPT_URL => $fcmUrl,
            CURLOPT_POST => true,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_POSTFIELDS => json_encode($fcmNotification)
        ]);

        return true;
    }
}