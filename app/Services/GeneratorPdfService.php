<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 14/02/19
 * Time: 14:46
 */

namespace DrPediu\Services;


use Barryvdh\DomPDF\PDF;
use DrPediu\Models\Recipe;
use Illuminate\Support\Facades\Storage;


class GeneratorPdfService
{
    private $invoice;
    private $recipe;

    public function __construct(PDF $invoice,Recipe $recipe)
    {
        $this->invoice = $invoice;
        $this->recipe = $recipe;
    }

    public function generatePdfRecipe($id)
    {
      $invoice = $this->recipe->with(
            'users',
            'doctors.user',
            'medicines'
        )->find($id);

       return $this->invoice->loadView('vendor.invoices.default', compact('invoice'))->download();

    }
}