<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 20/06/18
 * Time: 11:33
 */

namespace DrPediu\Services;


use DrPediu\Helpers\CalcDateRecipe;
use DrPediu\Helpers\UserHelper;
use DrPediu\Models\Doctor;
use DrPediu\Models\Medicine;
use DrPediu\Models\PivotCommentsXRecipesXUser;
use DrPediu\Models\PivotRecipesXMedicinesXTreatment;
use DrPediu\Models\Recipe;
use DrPediu\Models\PivotDoctorsXRecipesXUser;

class PatientService
{

    protected $pivotDoctorsXRecipesXUser;
    protected $pivotRecipesXMedicinesXTreatment;
    protected $pivotCommentsXRecipesXUser;
    protected $recipe;
    protected $medicine;
    protected $doctor;

    public function __construct(
        PivotDoctorsxRecipesxUser $pivotDoctorsXRecipesXUser,
        PivotRecipesXMedicinesXTreatment $pivotRecipesXMedicinesXTreatment,
        PivotCommentsXRecipesXUser $pivotCommentsXRecipesXUser,
        Recipe $recipe,
        Medicine $medicine,
        Doctor $doctor

    )
    {
        $this->pivotDoctorsXRecipesXUser = $pivotDoctorsXRecipesXUser;
        $this->pivotRecipesXMedicinesXTreatment = $pivotRecipesXMedicinesXTreatment;
        $this->pivotCommentsXRecipesXUser = $pivotCommentsXRecipesXUser;
        $this->recipe = $recipe;
        $this->medicine = $medicine;
        $this->doctor = $doctor;
    }

    public function getSlicesRecipesForUser($id)
    {
        $data = [];

        $results = $this->pivotDoctorsXRecipesXUser
            ->join('recipes','recipes.id','=','pivot_doctors_x_recipes_x_users.recipes_id')
            ->join('pivot_recipes_x_medicines_x_treatments','recipes.id','=','pivot_recipes_x_medicines_x_treatments.recipes_id')
            ->join('medicines','pivot_recipes_x_medicines_x_treatments.medicine_id','=','medicines.id')
            ->select('medicines.*')
            ->where('pivot_doctors_x_recipes_x_users.user_id',$id)
            ->paginate(6);

        foreach ($results as $result){
            $data[] = collect([
                'medicines' => $result->medicines
            ]);
        }

        return $results;
    }


    public function getListRecipes($id,$search)
    {
        $data = [];

        $treatments =  $this->pivotDoctorsXRecipesXUser->with(
                        'doctor',
                                 'doctor.user',
                                 'recipe')
                        ->orderBy('created_at','desc')
                        ->where('user_id',$id);

        if($search) {
            $results = $treatments->whereHas('doctor.user', function ($query) use ($search) {
                $query->where('name', 'like', "%{$search}%");
            })->paginate(6);
        }else{
            $results =  $treatments->paginate(6);
        }

        foreach ($results as $item){
            $data[] = collect([
                'name' => $item->doctor->user->name,
                'recipe_id' => $item->recipes_id,
                'validity_of_recipe' => $item->recipe->validity_of_recipe->format('d/m/Y'),
                'date_generator' => $item->recipe->created_at->format('d/m/Y H:i'),
                'started_out' => (bool)$item->recipe->start_recipe,
            ]);

        }

        return response()->json(['data' => $data]);

    }

    public function getRecipeUser($id)
    {
        $results = $this->recipe->find($id);
        $data = [];
        $doctor = $results->doctors->first();
        $user = $results->users->first();

        $data['doctor'] = collect([
            'id' => $doctor->user_id,
            'specialty' => $doctor->specialty,
            'name' => $doctor->user->name,
            'date_of_birth' => $doctor->user->date_of_birth->format('d/m/Y'),
            'image' =>  DirectoryService::getImageDefaultIfNotImage(
                $doctor->user->genre,
                (isset($doctor->user->profiles->image)?$doctor->user->profiles->image:'')
            ),
            'crm_number' => $doctor->crm_number,
            'telephones' => $doctor->user->contacts()->select('telephone_one','telephone_two','cell_phone')->get(),
            'email' => $doctor->user->email,
            'signature_image' => DirectoryService::getImageDefaultIfNotImage($doctor->user->genre,$doctor->signature_image)
        ]);

        $data['medical_patient'] = collect([
            'id' => $user->id,
            'name' => $user->name,
            'image' =>  DirectoryService::getImageDefaultIfNotImage(
                $user->genre,
                (isset($user->profiles->image)?$user->profiles->image:'')
            ),
            'date_of_birth' => $user->date_of_birth->format('d/m/Y'),
            'cpf' => $user->cpf,
            'rg' => $user->rg,
            'genre' => $user->genre,
            'address' => $user->user_address->street_title . ', '. $user->user_address->number . ' - '. $user->user_address->city->title . ' / '. $user->user_address->district_title
        ]);

        foreach ($results->medicines as $medicine) {
            $collection = collect([
                'id' => $medicine->id,
                'title' => $medicine->title,
                'start_medical' => $medicine->start_treatments->isNotEmpty(),
                'dosage' => $medicine->dosage,
                'frequency' => $medicine->frequency,
                'note' => $medicine->note,
                'number_of_days' => $medicine->number_of_days,
                'target' => $medicine->target,
                'validity_of_medicine' => $medicine->validity_of_medicine,
            ]);

            if($medicine->start_treatments->isNotEmpty()){
                $data['medicines'][] = $collection->merge([
                    'start_period' => $medicine->start_treatments->first()->start_period->format('d/m/Y'),
                    'end_period' => $medicine->start_treatments->first()->end_period->format('d/m/Y'),
                    'time_line' => CalcDateRecipe::calcTimeLineExpiration(
                        $medicine->start_treatments->first()->end_period,
                        $medicine->number_of_days
                    )
                ]);
            } else {
                $data['medicines'][] = $collection;
            }
        }

        return response()->json(['data' => $data]);
    }

    public function getListCommetsRecipe($id,$user)
    {

        $data = [];

        $results = $this->pivotCommentsXRecipesXUser
            ->join('recipes','recipes.id','=','pivot_comments_x_recipes_x_users.recipes_id')
            ->join('users','pivot_comments_x_recipes_x_users.user_id','=','users.id')
            ->join('comments','pivot_comments_x_recipes_x_users.comment_id','=','comments.id')
            ->select('name','genre','comments.comments','comments.id as comment_id',
                'comments.view_comment','recipes.validity_of_recipe','pivot_comments_x_recipes_x_users.created_at as created_at',
                'pivot_comments_x_recipes_x_users.user_id as user_id'
            )
            ->latest()
            ->where('recipes_id',$id)
            ->paginate(50);

        foreach ($results as $key => $item){

            $data[$key] = collect([
                'user_name' => $item->name,
                'time' => $item->created_at->format('U'),
                'comments' => $item->comments,
            ]);
            if($user != $item->user_id) {
                CommentsViewUpdate::updateViewStatus(['id' => $item->comment_id, 'view_comment' => $item->view_comment]);
            }
        }

        $results = collect($data)->reverse()->values()->all();

        return response()->json(['data' => $results]);


    }

    public function getAllMessage($patient_id,$search = null)
    {

        $data = [];

        $results = $this->pivotDoctorsXRecipesXUser
            ->where('user_id', $patient_id)
            ->paginate(15);

        if($results) {

            foreach ($results as $item) {

                if($item->recipe->comments->last()) {

                    $data[] = collect([
                        #'patient_id' => $item->recipe,
                        'recipe_id' => $item->recipe->id,
                        'name' => $item->recipe->comments->last()->users->first()->name,
                        'comments' => $item->recipe->comments->last()->comments,
                        'created_at' => $item->recipe->comments->last()->created_at->format('U'),
                        'last_comments' => CommentsViewUpdate::countMessageComments($item->recipe->id,$patient_id),
                        'image' => DirectoryService::getImageDefaultIfNotImage(
                            $item->user->genre,
                            (isset($item->user->image) ? $item->user->image : '')
                        )
                    ]);
                }
            }
        }

        if($search){
            $data = collect($data)->where('name',$search)->values()->all();
        }

        return response()->json(['data' => collect($data)->reverse()->values()->all()]);
    }

    public function getListMyDoctors($id,$search = null)
    {

        $data = [];

        $results = $this->pivotDoctorsXRecipesXUser
            ->join('users','pivot_doctors_x_recipes_x_users.user_id','=','users.id')
            ->join('doctors','pivot_doctors_x_recipes_x_users.doctor_id','=','doctors.id')
            ->join('pivot_doctors_x_specialty','doctors.id','=','pivot_doctors_x_specialty.doctor_id')
            ->join('specialties_medicals','pivot_doctors_x_specialty.specialty_id','=','specialties_medicals.id')
            ->leftjoin('profiles','users.id','=','profiles.user_id')
            ->select('name','genre','image','pivot_doctors_x_recipes_x_users.created_at as created_at','doctors.user_id as id_doctor','specialties_medicals.title','doctors.id')
            ->orderBy('created_at', 'desc')
            ->where('pivot_doctors_x_recipes_x_users.user_id',$id)
            ->paginate(15);

        foreach ($results as $item){
            $data[] = collect([
                'id' => $item->id,
                'name' => UserHelper::getDataUser($item->id_doctor)['name'],
                'specialty' => $item->title,
                'image' =>  UserHelper::getDataUser($item->id_doctor)['image']
            ]);
        }
        $data =  collect($data)->unique();

        if($search){
            $data = collect($data)->where('name',$search);
        }
        return response()->json(['data' => $data->values()->all()]);
    }

    public function getRecipeInfoDoctor($id)
    {
        $results = $this->recipe->find($id);

        if($results) {

            $user = $results->doctors->first();

            $data = collect([
                'id' => $user->user_id,
                'name' => $user->user->name,
                'email' => $user->user->email,
                'telephone' => $user->user->contacts->first()->telephone_one,
                'created_at' => $results->created_at->format('d/m/Y - H:i'),
                'image' => DirectoryService::getImageDefaultIfNotImage($user->user->genre, (isset($user->user->profiles->image) ? $user->user->profiles->image : null))
            ]);

            return response()->json(['data' => $data]);
        }
        return response()->json(['error' => 'dados não existentes!']);
    }

    public function listMedicalsRecipeForDoctor($doctor_id,$user_id)
    {
        $data = [];

        $results = $this->pivotDoctorsXRecipesXUser
            ->join('recipes','recipes.id','=','pivot_doctors_x_recipes_x_users.recipes_id')
            ->join('pivot_recipes_x_medicines_x_treatments','recipes.id','=','pivot_recipes_x_medicines_x_treatments.recipes_id')
            ->join('medicines','pivot_recipes_x_medicines_x_treatments.medicine_id','=','medicines.id')
            ->select('medicines.*')
            ->where('pivot_doctors_x_recipes_x_users.doctor_id',$doctor_id)
            ->where('pivot_doctors_x_recipes_x_users.user_id',$user_id)
            ->paginate(6);

        foreach ($results as $result){
            $data[] = collect([
                'medicines' => $result->medicines
            ]);
        }

        return $results;
    }

    public function getListRecipesPatient($patient_id,$doctor_id)
    {
        $data = [];

        $allListRecipesUser = $this->pivotDoctorsXRecipesXUser
            ->where('user_id', $patient_id)
            ->where('doctor_id', $doctor_id)
            ->orderBy('created_at', 'desc')
            ->paginate(15);

        if ($allListRecipesUser) {
            foreach ($allListRecipesUser as $key => $item) {

                $data[$key]['recipe'] = collect([
                    'recipe_id' => $allListRecipesUser[$key]->recipe->id,
                    'name' => ($allListRecipesUser[$key]->recipe->medicines()->count() > 1)
                        ? $allListRecipesUser[$key]->recipe->medicines()->count() . ' medicamentos'
                        : $allListRecipesUser[$key]->recipe->medicines()->count() . ' medicamento',
                    'started_out' => (bool)$allListRecipesUser[$key]->recipe->start_recipe,
                    'date_generator' => $allListRecipesUser[$key]->recipe->created_at->format('d/m/Y H:i')
                ]);

            }
            return response()->json(['data' => $data]);
        }

        return response()->json(['error' => 'Registro não encontrado'],404);

    }


}