<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 28/12/18
 * Time: 14:18
 */

namespace DrPediu\Services;

use DrPediu\Models\AnswerAnamnese;
use DrPediu\Models\QuestsAnamnese;
use function GuzzleHttp\Psr7\str;

class AnamnesesService
{
    private $answerAnamnese;
    private $questsAnamnese;

    public function __construct(QuestsAnamnese $questsAnamnese, AnswerAnamnese $answerAnamnese)
    {
        $this->questsAnamnese = $questsAnamnese;
        $this->answerAnamnese = $answerAnamnese;
    }


    public function getAnswerAnamneseForUser($id)
    {
        $data = [];

        $anamneses = $this->answerAnamnese->where('user_id',$id)->get();


        foreach ($anamneses as $anamnese){
            $data[] = collect([
                'topic_id' => $anamnese->id_quest_anamnese,
                'title_question' => $anamnese->quests_anamnese->title,
                'sub_title_quetion' => $anamnese->sub_answer_anamnese->toArray(),
                'value' => $anamnese->option
            ]);
        }

        return response()->json(['data' => $data]);

    }

    public function create($request)
    {

        $data = $request->all();


        foreach ($data['answer'] as $items){

            if(isset($items['sub_title_quetion']) and $items['value'] == true){

                $answers = $this->answerAnamnese->create([
                    'id_quest_anamnese' => $items['topic_id'],
                    'user_id' => $data['user_id'],
                    'option' => (int)$items['value']
                ]);

                foreach ($items['sub_title_quetion'] as $item){

                    $answers->sub_answer_anamnese()->create([
                        'title' => $item['title'],
                        'answer' => $item['value']
                    ]);
                }
            } else {
                $this->answerAnamnese->create([
                    'id_quest_anamnese' => $items['topic_id'],
                    'user_id' => $data['user_id'],
                    'option' => (int)$items['value']
                ]);
            }
        }

        return response()->json(['success' => 'Registro criado com sucesso!']);

    }

    public function update($request)
    {
        $data = $request->all();

        $this->delete($data['user_id']);

        foreach ($data['answer'] as $items){

            if(isset($items['sub_title_quetion']) and $items['value'] == true){

                $answers = $this->answerAnamnese->create([
                    'id_quest_anamnese' => $items['topic_id'],
                    'user_id' => $data['user_id'],
                    'option' => (int)$items['value']
                ]);

                foreach ($items['sub_title_quetion'] as $item){

                    $answers->sub_answer_anamnese()->create([
                        'title' => $item['title'],
                        'answer' => $item['value']
                    ]);
                }
            } else {
                $this->answerAnamnese->create([
                    'id_quest_anamnese' => $items['topic_id'],
                    'user_id' => $data['user_id'],
                    'option' => (int)$items['value']
                ]);
            }
        }

        return response()->json(['success' => 'Registro atualizado com sucesso!']);

    }

    public function delete($id)
    {
        $anamneses = $this->answerAnamnese->where('user_id',$id)->get();

        foreach ($anamneses as $anamnese){
            $anamnese->sub_answer_anamnese()->forceDelete();
            $anamnese->forceDelete();
        }
    }

    public function structureAnamnese()
    {

        $data = [];

        $anamneses = $this->questsAnamnese->all();

        if(!$anamneses->isEmpty()) {

            foreach ($anamneses as $sub_quest) {

                if (!$sub_quest->sub_quest_anamnese->isEmpty()) {
                    $data[] = collect([
                        'topic_id' => $sub_quest->id,
                        'title_question' => $sub_quest->title,
                        'sub_title_quetion' => $sub_quest->sub_quest_anamnese,
                    ]);
                } else {
                    $data[] = collect([
                        'topic_id' => $sub_quest->id,
                        'title_question' => $sub_quest->title,
                    ]);
                }
            }

            return response()->json(['data' => $data]);
        }
        return response()->json(['error' => 'Dados não existentes!']);

    }

}