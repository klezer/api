<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 20/06/18
 * Time: 11:33
 */

namespace DrPediu\Services;

use DrPediu\Events\CommentsUpdateStatusView;
use DrPediu\Events\MessageSend;
use DrPediu\Jobs\CommentsViewStatusUpdate;
use DrPediu\Models\Comment;
use DrPediu\Models\Doctor;
use DrPediu\Models\Medicine;
use DrPediu\Models\PivotCommentsXRecipesXUser;
use DrPediu\Models\PivotRecipesXMedicinesXTreatment;
use DrPediu\Models\Recipe;
use DrPediu\Models\PivotDoctorsXRecipesXUser;
use DrPediu\Models\User;
use DrPediu\Util\Util;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;

class DoctorService
{

    protected $pivotDoctorsXRecipesXUser;
    protected $pivotRecipesXMedicinesXTreatment;
    protected $pivotCommentsXRecipesXUser;
    protected $recipe;
    protected $medicine;
    protected $doctor;
    protected $user;

    public function __construct(
        PivotDoctorsxRecipesxUser $pivotDoctorsXRecipesXUser,
        PivotRecipesXMedicinesXTreatment $pivotRecipesXMedicinesXTreatment,
        PivotCommentsXRecipesXUser $pivotCommentsXRecipesXUser,
        Recipe $recipe,
        Medicine $medicine,
        User $user,
        Doctor $doctor
    )
    {
        $this->pivotDoctorsXRecipesXUser = $pivotDoctorsXRecipesXUser;
        $this->pivotRecipesXMedicinesXTreatment = $pivotRecipesXMedicinesXTreatment;
        $this->pivotCommentsXRecipesXUser = $pivotCommentsXRecipesXUser;
        $this->recipe = $recipe;
        $this->medicine = $medicine;
        $this->doctor = $doctor;
        $this->user = $user;

    }

    public function create($request)
    {

        \DB::beginTransaction();

        try {

            $doctor =  $this->doctor->where('user_id',$request['doctor_id'])->first();

            $recipe = $this->recipe->create([
                'type_recipe_id' => $request['type_recipe_id'],
                'validity_of_recipe' => Carbon::now(),
            ]);

            foreach ($request->medicines as $medicine) {

                $medicine = $this->medicine->create([
                    'dosage' => $medicine['dosage'],
                    'frequency' => $medicine['frequency'],
                    'considerations' => (isset($medicine['considerations'])?$medicine['considerations']:null),
                    'number_of_days' => $medicine['number_of_days'],
                    'title' => $medicine['title'],
                    'validity_of_medicine' => Util::formatStringDateToIso($medicine['validity_of_medicine']),
                    'subtitle' => (isset($medicine['subtitle'])?$medicine['subtitle']:null),
                    'note' => $medicine['note'],
                ]);

                $this->pivotRecipesXMedicinesXTreatment->create([
                    'recipes_id' => $recipe->id,
                    'medicine_id' => $medicine->id
                ]);
            }
            $this->pivotDoctorsXRecipesXUser->create([
                'recipes_id' =>  $recipe->id,
                'user_id' =>  $request['user_id'],
                'doctor_id' =>  $doctor->id,
            ]);

            \DB::commit();
            return response()->json(['success' => 'Registro criado com sucesso!']);

        }catch (\Exception $e){
            \DB::rollback();
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    public function getListRecipes($id,$search = null)
    {
        $data = [];

        $results = $this->doctor
            ->join('pivot_doctors_x_recipes_x_users','doctors.id','=','pivot_doctors_x_recipes_x_users.doctor_id')
            ->join('recipes','recipes.id','=','pivot_doctors_x_recipes_x_users.recipes_id')
            ->join('users','pivot_doctors_x_recipes_x_users.user_id','=','users.id')
            ->leftjoin('profiles','users.id','=','profiles.user_id')
            ->select('name','genre','image','recipes.validity_of_recipe','pivot_doctors_x_recipes_x_users.created_at as created_at','recipes.id as recipe_id','recipes.start_recipe')
            ->orderBy('created_at', 'desc')
            ->where('doctors.user_id',$id)
            ->where('name','like','%'.$search.'%')
            ->paginate(6);

        foreach ($results as $item){
            $data[] = collect([
                'name' => $item->name,
                'recipe_id' => $item->recipe_id,
                'validity_of_recipe' => $item->validity_of_recipe,
                'date_generator' => $item->created_at->format('d/m/Y H:i'),
                'started_out' => (bool)$item->start_recipe,
                'image' =>  DirectoryService::getImageDefaultIfNotImage(
                    $item->genre,
                    (isset($item->image)?$item->image:'')
                )
            ]);
        }

        return response()->json(['data' => $data]);
    }

    public function getListMedicalPatient($id,$search = null)
    {
        $data = [];

        $results = $this->doctor
            ->join('pivot_doctors_x_recipes_x_users','doctors.id','=','pivot_doctors_x_recipes_x_users.doctor_id')
            ->join('users','pivot_doctors_x_recipes_x_users.user_id','=','users.id')
            ->leftjoin('profiles','users.id','=','profiles.user_id')
            ->select('name','genre','image','pivot_doctors_x_recipes_x_users.created_at as created_at','users.id as user_id')
            ->orderBy('created_at', 'desc')
            ->where('doctors.user_id',$id)
            ->where('name','like','%'.$search.'%')
            ->paginate(15);

        foreach ($results as $item){
            $data[] = collect([
                'id' => $item->user_id,
                'name' => $item->name,
                'image' =>  DirectoryService::getImageDefaultIfNotImage(
                    $item->genre,
                    (isset($item->image)?$item->image:'')
                )
            ])->unique();
        }

        $filtered =  collect($data)->unique();

        return response()->json(['data' => $filtered->values()->all()]);
    }

    public function getListRecipesPatient($patient_id,$doctor_id)
    {
        $doctor =  $this->doctor->where('user_id',$doctor_id)->first();

        if($doctor) {

            $allListRecipesUser = $this->pivotDoctorsXRecipesXUser
                ->where('user_id', $patient_id)
                ->where('doctor_id', $doctor->id)
                ->orderBy('created_at', 'desc')
                ->paginate(15);
            $data = [];


            if ($allListRecipesUser) {
                foreach ($allListRecipesUser as $key => $item) {

                    $data[$key]['recipe'] = collect([
                        'recipe_id' => $allListRecipesUser[$key]->recipe->id,
                        'name' => ($allListRecipesUser[$key]->recipe->medicines()->count() > 1)
                            ? $allListRecipesUser[$key]->recipe->medicines()->count() . ' medicamentos'
                            : $allListRecipesUser[$key]->recipe->medicines()->count() . ' medicamento',
                        'started_out' => (bool)$allListRecipesUser[$key]->recipe->start_recipe,
                        'date_generator' => $allListRecipesUser[$key]->recipe->created_at->format('d/m/Y H:i')
                    ]);

                }
                return response()->json(['data' => $data]);
            }
        }
        return response()->json(['error' => 'Registro não encontrado'],404);

    }

    public function getAllMessage($doctor_id,$search = null)
    {

        $data = [];

        $doctor_id = $this->doctor->where('user_id',$doctor_id)->first();

        if($doctor_id) {

            $results = $this->pivotDoctorsXRecipesXUser
                            ->where('doctor_id', $doctor_id->id)
                            ->paginate(15);

            foreach ($results as $item) {

                if($item->recipe->comments->last()) {

                    $data[] = collect([
                        'recipe_id' => $item->recipe->id,
                        'name' => $item->recipe->users->first()->name,
                        'comments' => $item->recipe->comments->last()->comments,
                        'created_at' => $item->recipe->comments->last()->created_at->format('U'),
                        'last_comments' => CommentsViewUpdate::countMessageComments($item->recipe->id,$doctor_id->user_id),
                        'image' => DirectoryService::getImageDefaultIfNotImage(
                            $item->recipe->users->first()->genre,
                            (isset($item->recipe->users->first()->profiles->image) ? $item->recipe->users->first()->profiles->image : '')
                        )
                    ]);
                }
            }
        }

        if($search){
            $data = collect($data)->where('name',$search)->values()->all();
        }

        return response()->json(['data' => collect($data)->reverse()->values()->all()]);

    }

    public function getRecipeIfoPatient($id)
    {
        $results = $this->recipe->find($id);
        $user = $results->users->first();

        $data = collect([
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'telephone' => $user->contacts->first()->telephone_one,
            'created_at' => $results->created_at->format('d/m/Y - H:i'),
            'image' => DirectoryService::getImageDefaultIfNotImage($user->genre,(isset($user->profiles->image)?$user->profiles->image:null))
        ]);

        return response()->json(['data' => $data]);
    }
}