<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/07/18
 * Time: 16:20
 */

namespace DrPediu\Services;


use DrPediu\Models\Profile;
use Illuminate\Http\Request;

class ProfileService
{

    private $directoryService;
    private $profile;

    public function __construct( DirectoryService $directoryService,Profile $profile)
    {
        $this->directoryService = $directoryService;
        $this->profile = $profile;
    }

    public function create(Request $request)
    {
        $data = $request->all();

        if ($request->image) {

            $image = $request->image;
            $image = str_replace('data:image/png;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $data['image'] = str_random(15).'.'.'png';
            \File::put(storage_path(). '/app/public/avatars/' . $data['image'], base64_decode($image));

            $this->profile->create($data);
            return response()->json(['success' => 'Registro criado com sucesso!'],200);
        }
    }

}