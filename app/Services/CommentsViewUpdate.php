<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 14/01/19
 * Time: 19:18
 */

namespace DrPediu\Services;


use DrPediu\Models\Comment;
use DrPediu\Models\Doctor;
use DrPediu\Models\PivotCommentsXRecipesXUser;
use DrPediu\Models\Recipe;

class CommentsViewUpdate
{


    private $pivotCommentsXRecipesXUser;

    public function __construct(PivotCommentsXRecipesXUser $pivotCommentsXRecipesXUser)
    {
        $this->pivotCommentsXRecipesXUser = $pivotCommentsXRecipesXUser;
    }

    public static function updateViewStatus($data)
    {
        $comments = new Comment();
        if($data) {
            if ($data['view_comment'] == '0')
                $comments->find($data['id'])->update(['view_comment' => '1']);
        }
    }

    public static function countMessageComments($id,$user_id)
    {
        $recipes = new Recipe();

         $results = $recipes->join('pivot_comments_x_recipes_x_users','recipes.id','pivot_comments_x_recipes_x_users.recipes_id')
                            ->join('comments','pivot_comments_x_recipes_x_users.comment_id','comments.id')
                            ->select('recipes.id','view_comment','user_id')
                            ->where('user_id','<>',$user_id)
                            ->where('view_comment','0')
                            ->where('recipes.id',$id)->count();
         return $results;

    }

}