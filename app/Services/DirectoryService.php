<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/07/18
 * Time: 14:16
 */

namespace DrPediu\Services;
Use Response;

class DirectoryService
{

    public function getDeleteImageDiretorio($path, $image)
    {
        if (isset($image)) {
            $diretorio = storage_path('app/public/') . $path . $image;
            \File::delete($diretorio);
            return true;
        }
    }
    public static function getImageDiretorio($path, $image)
    {
        if (isset($image)) {

            $diretorio = storage_path('app/public/') . $path . $image;

            if (!\File::exists($diretorio)) {
                return response()->json(['error' =>'imagem não existente'],200);
            }

            $file = \File::get($diretorio);
            $type = \File::mimeType($diretorio);

            $response = Response::make($file, 200);
            return	$response->header("Content-Type", $type);
        }
    }

    public static function getImageDefaultIfNotImage($genre,$image)
    {
        if($image){
            return secure_url('api/storage',$image);
        }
        if($genre == 'M') {
            return  secure_url('api/storage','menino.png');
        }elseif ($genre == 'F') {
            return  secure_url('api/storage','menina.png');
        }else{
            return  secure_url('api/storage','outro.png');
        }

    }

    public static function getDocumentProfessional($image)
    {
        if($image) {
            return secure_url(route('files.doctor.document.get', $image));
        }
        return 'imagem não existente';
    }


}