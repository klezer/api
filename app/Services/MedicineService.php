<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 01/12/18
 * Time: 19:53
 */

namespace DrPediu\Services;


use DrPediu\Models\Medicine;
use Ixudra\Curl\Facades\Curl;

class MedicineService
{
    private $medicine;

    public function __construct(Medicine $medicine)
    {
      $this->medicine = $medicine;
    }

    public function listMedicine($search = null)
    {
        $data = [];
        $results = $this->medicine->select('title','id')->get();


        foreach ($results as $result) {
            $data[] = collect([
                    'display' => $result->title,
                    'value' => $result->id
                ]);

        }
       return $data;
    }

}