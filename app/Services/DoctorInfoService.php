<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 08/02/19
 * Time: 15:28
 */

namespace DrPediu\Services;


use DrPediu\Models\Doctor;

class DoctorInfoService
{
    private $doctor;

    public function __construct(Doctor $doctor)
    {
     $this->doctor = $doctor;
    }

    public function getInfoDoctorForId($id)
    {
        $user = $this->doctor->find($id);
        $data = [];

        $data['personal'] = [
            'name' => $user->user->name,
            'email' => $user->user->email,
            'cpf' => $user->user->cpf,
            'rg' => $user->user->rg,
            'genre' => $user->user->genre
        ];

        $contacts = $user->user->contacts()->first();

        $data['contact'] = [
            'telephone_one' => $contacts->telephone_one,
            'telephone_two' => $contacts->telephone_two,
            'cell_phone' => $contacts->cell_phone,
        ];

        $user_address = $user->user->user_addresses()->first();

        $data['address'] = [
            'city_id' => $user_address->city_id,
            'state_id' => $user_address->city->state_id,
            'number' => $user_address->number,
            'complement' => $user_address->complement,
            'street_code' => $user_address->street_code,
            'street_title' => $user_address->street_title,
            'district_title' => $user_address->district_title
        ];

        $data['picture'] = [
            'image' =>  DirectoryService::getImageDefaultIfNotImage($user->user->genre,(isset($user->user->profiles->image)?$user->user->profiles->image:''))
        ];

        return response()->json(['data' => $data]);

    }

}