<?php


namespace DrPediu\Services;

use DrPediu\Helpers\UserHelper;
use DrPediu\Models\Medicine;
use DrPediu\Models\PivotDoctorsXRecipesXUser;
use DrPediu\Models\PivotDoctorsXSpecialty;
use DrPediu\Util\Util;
use Illuminate\Support\Facades\Lang;
use mysql_xdevapi\Collection;

class PharmaciesService
{
	private $pivotDoctorsXRecipesXUser;
	private $medicine;

	public function __construct( PivotDoctorsXRecipesXUser $pivotDoctorsXRecipesXUser , Medicine $medicine)
	{
		$this->pivotDoctorsXRecipesXUser = $pivotDoctorsXRecipesXUser;
		$this->medicine  = $medicine;
	}

	public function getRecipeForCPFUser($cpf)
	{
		 $results = $this->pivotDoctorsXRecipesXUser->with('doctor.user')
			->join('users','pivot_doctors_x_recipes_x_users.user_id','=','users.id')
			->join('recipes','recipes.id','=','pivot_doctors_x_recipes_x_users.recipes_id')
			->join('pivot_recipes_x_medicines_x_treatments','recipes.id','=','pivot_recipes_x_medicines_x_treatments.recipes_id')
			->join('medicines','pivot_recipes_x_medicines_x_treatments.medicine_id','=','medicines.id')
			->where('medicines.validity_of_medicine','>=',now()->format('Y-m-d'))
			->where('medicines.sales_of_medicines_id','=',0)
			->where('users.cpf',$cpf)
			->paginate(6);

		$filters = $results->map(function ($result) {
			return [
				'medicine' => [
					'id' => $result->medicine_id,
					'title' => $result->title,
					'dosage' => $result->dosage,
					'frequency' => $result->frequency,
					'number_of_days' => $result->number_of_days,
					'validity_of_medicine' => Util::formatStringDateToIso($result->validity_of_medicine)->format('Y/m/d'),
					'subtitle' => $result->subtitle,
					'note' => $result->note,
					'sales_of_medicines_id' => $result->sales_of_medicines_id,
				],
				'doctor' => [
					'name' => $result->doctor->user->name,
					'cpf' => $result->doctor->user->cpf,
					'crm_number' => $result->doctor->crm_number,
					'specialties_medicals' => PivotDoctorsXSpecialty::getSpecialtiesDoctor($result->doctor_id)->title
				],
			];
		});

		$patient = $results->map(function ($result) {
			return [
				'patient' => [
					'name' => $result->name,
					'cpf' => $result->cpf,
					'image' => UserHelper::getDataUser($result->user_id)['image'],
					'date_of_birth' => $result->date_of_birth,
					'genre' => $result->genre

				],
			];
		})->first();

		if($filters->isNotEmpty()){
			return collect($patient)->merge(['medicines' => $filters])->all();
		}
		return response()->json(['errors' => Lang::trans('custom.pharmacies.not-found-cpf')]);
	}
	public function updateSalesMedicines($request)
	{
		$results = $this->medicine->where('id',$request->medicine_id)->where('sales_of_medicines_id',0)->first();

		if ($results) {
			$results->update(['sales_of_medicines_id' => 1]);
			return response()->json([
				'success' => Lang::trans('custom.pharmacies.success'),
				'sales' => false
			]);
		}
		return response()->json([
			'errors' =>  Lang::trans('custom.pharmacies.error'),
			'sales' => true
		]);
	}
}