<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 05/02/19
 * Time: 10:55
 */

namespace DrPediu\Services;


use DrPediu\Models\DiscartPoint;

class DiscartPointsService
{

    private $discartPoint;

    public function __construct(DiscartPoint $discartPoint)
    {
      $this->discartPoint = $discartPoint;
    }

    public function list()
    {
        $data = [];

         $results = $this->discartPoint
                  ->with('discart_point_addresses')
                  ->get();


      foreach ($results as $result){
          $data[] = collect([
              'title' => $result->title,
              'fone' => $result->fone,
              'street_title' => $result->discart_point_addresses()->value('street_title'),
              'city' => $result->discart_point_addresses()->value('city'),
              'latitude' => $result->discart_point_addresses()->value('latitude'),
              'longitude' => $result->discart_point_addresses()->value('longitude'),
              'state' => $result->discart_point_addresses()->value('state'),
              'district' => $result->discart_point_addresses()->value('district'),
          ]);
      }
      return $data;


    }

}