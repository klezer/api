<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 03/01/19
 * Time: 19:56
 */

namespace DrPediu\Services;

use Pusher\Pusher;

class ServicesPusherSend
{

    private $pusher;


   public function __construct()
   {
       $options = array(
           'cluster' => 'us2',
           'encrypted' => true
       );
       $this->pusher =  new Pusher(
           env('PUSHER_APP_KEY'),
           env('PUSHER_APP_SECRET'),
           env('PUSHER_APP_ID'),
           $options);
   }

   public function triggerPusher($data)
   {
       $this->pusher->trigger($data['chanel'], 'message', $data);
       return "Mensagem enviada com sucesso!";
   }

}