<?php


namespace DrPediu\Services;


use DrPediu\Models\DrugInteraction;
use DrPediu\Traits\FullTextSearch;

class DrugInteractionService
{
	use FullTextSearch;
	private $drugInteraction;

	public function __construct(DrugInteraction $drugInteraction)
	{
		$this->drugInteraction = $drugInteraction;
	}

	public function interaction($request)
	{
		$full_text = $this->fullTextWildcards($request->interaction);

		if($full_text) {

			foreach ($full_text as $item) {

				$result = $this->drugInteraction
					->whereRaw(
						"MATCH (title_interaction) AGAINST (? IN BOOLEAN MODE)",

						$item)
					->get();
				if ($result->isNotEmpty()) {
					return $result;
				}
			}
		}

		return response()->json(['menssage' => 'Não existe Interações']);
	}

	protected function tratmentText($term)
	{
		// removing symbols used by MySQL
		$reservedSymbols = ['-', '+', '<', '>', '@', '(', ')', '~'];
		$term = str_replace($reservedSymbols, '', $term);

		$words = explode(' ', $term);

		$result = collect($words)->crossJoin($words);
		$text = null;

		foreach($result as $key => $word) {

			$unique_combination = collect($word)->unique();

			foreach ($unique_combination as $Key => $item) {
				if($unique_combination->count() > 1) {
					$text .= ':' . '+' . $unique_combination[0] . ' +(>' . $unique_combination[1]  . ' <'.$unique_combination[1] . ')';
				}
			}
		}
		$searchTerm = explode( ':', $text);
		return array_filter($searchTerm);

	}

}