<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 10/08/18
 * Time: 17:21
 */

namespace DrPediu\Services;

use DrPediu\Models\Comment;
use DrPediu\Models\PivotCommentsXRecipesXUser;

class CommentService
{
    private $comment;
    private $pivotCommentsXRecipesXUser;
    private $servicesPusherSend;

    public function __construct(PivotCommentsXRecipesXUser $pivotCommentsXRecipesXUser,
                                Comment $comment,
                                ServicesPusherSend $servicesPusherSend)
    {
        $this->comment = $comment;
        $this->pivotCommentsXRecipesXUser = $pivotCommentsXRecipesXUser;
        $this->servicesPusherSend = $servicesPusherSend;
    }

    public function create($request)
    {

        $last_insert_comment = $this->comment->create(['comments' => $request->comments]);

        $pivot = $this->pivotCommentsXRecipesXUser->create([
            'recipes_id' => $request->recipes_id,
            'user_id' => $request->user_id,
            'comment_id' => $last_insert_comment->id
        ]);

        $comments = $this->getCreateComments($pivot->id);
        $comments->prepend((string)$request->recipes_id,'chanel');
        $this->servicesPusherSend->triggerPusher($comments->all());

        return response()->json(['success' => 'comentário enviado com sucesso!' ]);
    }

    public function getCreateComments($id)
    {
        $data = [];

        $pivot = $this->pivotCommentsXRecipesXUser->find($id);

        $data = collect([
            'comment_id' => $pivot->comment_id,
            'user_name' => $pivot->user->name,
            'image' => DirectoryService::getImageDefaultIfNotImage(
                $pivot->user->genre,
                (isset($pivot->user->profiles->image))?$pivot->user->profiles->image:''

            ),
            'time' => $pivot->comment->created_at->format('U'),
            'comments' => $pivot->comment->comments
        ]);

        return $data;

    }

    public function updateCommentForPusher($request)
    {
        $this->comment->find($request->comment_id)->update(['view_comment' => '1']);
        return response()->json(['success' => 'Registro atualizado com sucesso!']);
    }

}