<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 21/01/19
 * Time: 14:02
 */

namespace DrPediu\Services;


use DrPediu\Models\SpecialtiesMedical;

class SpecialtiesService
{

    private $specialtiesMedical;

    public function __construct(SpecialtiesMedical $specialtiesMedical)
    {
        $this->specialtiesMedical = $specialtiesMedical;
    }

    public function listSpecialties()
    {
         return   $this->specialtiesMedical->select('title as display','id as value')->get();
    }

}