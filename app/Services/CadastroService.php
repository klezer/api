<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 28/06/18
 * Time: 14:37
 */

namespace DrPediu\Services;
use DrPediu\Models\City;
use DrPediu\Models\State;
use DrPediu\Models\User;
use DrPediu\Models\UserAddress;
use DrPediu\Util\Util;
use http\Env\Request;


class CadastroService
{

    protected $user;
    protected $userAddress;

    public function __construct(User $user, UserAddress $userAddress)
    {
        $this->user = $user;
        $this->userAddress = $userAddress;
    }

    public function createService($cadastroApiRequest)
    {
        \DB::beginTransaction();

        try {


            $user = [
                'name' => $cadastroApiRequest->input('name'),
                'email' => $cadastroApiRequest->input('email'),
                'password' => bcrypt($cadastroApiRequest->input('password')),
                'cpf' => $cadastroApiRequest->input('cpf'),
                'rg' => $cadastroApiRequest->input('rg'),
                'genre' => $cadastroApiRequest->input('genre'),
                'date_of_birth' => Util::formatStringDateToIso($cadastroApiRequest->input('date_of_birth')),
            ];

            $last_insert_user = $this->user->create($user);


            $user_address = [
                'user_id' => $last_insert_user->id,
                'street_title' => $cadastroApiRequest->input('street_title'),
                'city_id' => $cadastroApiRequest->input('city_id'),
                'district_title' => $cadastroApiRequest->input('district_title'),
                'complement' => $cadastroApiRequest->input('complement', null),
                'number' => $cadastroApiRequest->input('number'),
                'street_code' => $cadastroApiRequest->input('street_code')
            ];

            $last_insert_user->user_addresses()->create($user_address);

            $user_contacts = [
                'user_id' => $last_insert_user->id,
                'telephone_one' => $cadastroApiRequest->input('telephone_one', null),
                'telephone_two' => $cadastroApiRequest->input('telephone_two', null),
                'cell_phone' => $cadastroApiRequest->input('cell_phone', null),

            ];

            $last_insert_user->contacts()->create($user_contacts);

            \DB::commit();

            return response()->json([
                    'id' => $last_insert_user->id,
                    'success' => 'Registro criado com sucesso!'
                ]
            );


        }catch (\Exception $e){
            \DB::rollback();
            return response()->json(['error' => $e->getMessage()]);

        }
    }

    public function updateService($updateCadastroApiRequest)
    {
        $user = $this->user->find($updateCadastroApiRequest->user_id);

        $user_address  = $user->user_addresses->where('id',$updateCadastroApiRequest->input('address_id'))->first();

        \DB::beginTransaction();

        try {

            if($user_address) {


                $user_address = [
                    'user_id' => $user->id,
                    'street_title' => $updateCadastroApiRequest->input('street_title'),
                    'city_id' => $updateCadastroApiRequest->input('city_id'),
                    'district_title' => $updateCadastroApiRequest->input('district_title'),
                    'complement' => $updateCadastroApiRequest->input('complement', null),
                    'number' => $updateCadastroApiRequest->input('number'),
                    'street_code' => $updateCadastroApiRequest->input('street_code')
                ];

                $user->user_addresses()->update($user_address);

                $user_contacts = [
                    'user_id' => $user->id,
                    'telephone_one' => $updateCadastroApiRequest->input('telephone_one', null),
                    'telephone_two' => $updateCadastroApiRequest->input('telephone_two', null),
                    'cell_phone' => $updateCadastroApiRequest->input('cell_phone', null),

                ];

                $user->contacts()->update($user_contacts);

                $user_data = [
                    'name' => $updateCadastroApiRequest->input('name'),
                    'cpf' => $updateCadastroApiRequest->input('cpf'),
                    'rg' => $updateCadastroApiRequest->input('rg'),
                    'genre' => $updateCadastroApiRequest->input('genre'),
                    'password' => ($updateCadastroApiRequest->input('password'))?bcrypt($updateCadastroApiRequest->input('password')):$user->password,
                ];

                $user->update($user_data);

                \DB::commit();
                return response()->json(['success' => 'Registro atualizado com sucesso!']);
            }


        }catch (\Exception $e){
            \DB::rollback();
            return response()->json(['error' => $e->getMessage()]);

        }

        return response()->json(['success' => 'Registro atualizado com sucesso!']);

    }

    public function citys($id)
    {
        $data['id'] = $id;

        $messages = [
            'id.required' => 'O parâmetro passado é obrigatório!',
            'id.numeric' => 'O parâmetro passado deverá ser numérico!',
        ];
        $validator = \Validator::make($data, [
            'id' => 'required|numeric',
        ], $messages);

        if($validator->fails()){
            return response()->json(['errors' => $validator->messages()]);
        }
        $citys =  City::where('state_id',$id)->select('title as display','id as value')->get();

        return response()->json(['citys' => $citys]);
    }
    public function states()
    {
        return State::states();
    }
    public function getRegisterUser($id)
    {
        $user = $this->user->find($id);
        $data = [];

         $data['personal'] = [
                'name' => $user->name,
                'email' => $user->email,
                'cpf' => $user->cpf,
                'rg' => $user->rg,
                'date_of_birth' => $user->date_of_birth->format('d/m/Y'),
                'genre' => $user->genre
         ];

         $contacts = $user->contacts()->first();

         $data['contact'] = [
             'telephone_one' => $contacts->telephone_one,
             'telephone_two' => $contacts->telephone_two,
             'cell_phone' => $contacts->cell_phone,
         ];

         $user_address = $user->user_addresses()->first();

         $data['address'] = [
             'address_id' => $user_address->id,
             'city_id' => $user_address->city_id,
             'state_id' => $user_address->city->state_id,
             'number' => $user_address->number,
             'complement' => $user_address->complement,
             'street_code' => $user_address->street_code,
             'street_title' => $user_address->street_title,
             'district_title' => $user_address->district_title
         ];

         $data['picture'] = [
           'image' =>  DirectoryService::getImageDefaultIfNotImage($user->genre,(isset($user->profiles->image)?$user->profiles->image:''))
         ];

         return response()->json(['data' => $data]);
    }

}