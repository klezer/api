<?php

namespace DrPediu\Providers;


use DrPediu\Models\PivotDoctorsXRecipesXUser;
use DrPediu\Models\StartTreatment;
use DrPediu\Observers\AnamnesesObserver;
use DrPediu\Observers\RecipesObserver;
use DrPediu\Observers\StartTreatmentObserver;
use Illuminate\Support\ServiceProvider;

class RecipesActionsProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        PivotDoctorsxRecipesxUser::observe(RecipesObserver::class);
        StartTreatment::observe( StartTreatmentObserver::class);
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
