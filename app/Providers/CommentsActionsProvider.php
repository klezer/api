<?php

namespace DrPediu\Providers;

use Illuminate\Support\ServiceProvider;
use DrPediu\Models\PivotCommentsXRecipesXUser;
use DrPediu\Observers\CommentsObserver;

class CommentsActionsProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        PivotCommentsXRecipesXUser::observe(CommentsObserver::class);
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
