<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/02/19
 * Time: 10:47
 */

namespace DrPediu\Helpers;


use Carbon\Carbon;

class CalcDateRecipe
{

    public static function setDateExpiration($date)
    {
        return Carbon::now()->addDays($date);
    }
    public static function calcTimeLineExpiration($end_period,$number_of_days = null)
    {
        $dt = Carbon::now();
        $dt2 = $end_period;
        $daysForExtraCoding = $dt->diffInDays($dt2);

        return  self::percentValue($daysForExtraCoding,$number_of_days);

    }
    public static function percentValue($param_one,$param_two)
    {
        if ($param_one == 0) {
            return 0;
        } else {
                return $percent_friendly = 100 - number_format(($param_one / $param_two),2) * 100;
        }
    }
    public static function generaScheduleHours($freguency)
    {
        $mod = 24 / $freguency;
        return (is_float($mod)?$mod+1:$mod);
    }

}