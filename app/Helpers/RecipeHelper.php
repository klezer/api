<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 13/02/19
 * Time: 16:21
 */

namespace DrPediu\Helpers;


use DrPediu\Models\Recipe;

class RecipeHelper
{

    public static function updateStarTreatmentRecipe($id)
    {
        return Recipe::find($id)->update(['start_recipe' => true]);
    }

}