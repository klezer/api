<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 18/01/19
 * Time: 15:27
 */

namespace DrPediu\Helpers;


use DrPediu\Models\User;
use DrPediu\Services\DirectoryService;

class UserHelper
{

    public static function getDataUser($id)
    {
        $data = [];

        $users = new User();
        $user = $users->select('id','name','device_token','genre')->find($id);

        $data = [
            'name' => $user['name'],
            'image' => DirectoryService::getImageDefaultIfNotImage(
                $user['genre'],
                (isset($user->profiles->image))?$user->profiles->image:''

            ),
            'device_token' => $user['device_token']
        ];

        return $data;
    }

    public static function getImageProfile($image,$genre)
    {
      return  DirectoryService::getImageDefaultIfNotImage($genre, (isset($image))?$image:'');
    }

}