<?php

namespace DrPediu\Observers;

use DrPediu\Helpers\RecipeHelper;
use DrPediu\Helpers\UserHelper;
use DrPediu\Models\StartTreatment;
use Carbon\Carbon;
use DrPediu\Helpers\CalcDateRecipe;
use DrPediu\Services\SendPusherNotificationService;

class StartTreatmentObserver
{

    public function __construct(SendPusherNotificationService $sendPusherNotificationService)
    {
        $this->sendPusherNotificationService = $sendPusherNotificationService;
    }

    public function updated(StartTreatment $startTreatment)
    {
        $treatments =  $startTreatment->with(
            'medicine:id,number_of_days,frequency',
            'medicine.recipes.doctors.user',
            'medicine.recipes.users.profiles'
        )->find($startTreatment->id);

        $carbon = Carbon::now();
        $recipe_id = $treatments->medicine->recipes->first()->id;

        if(!$treatments->schedule_generate) {

            RecipeHelper::updateStarTreatmentRecipe($recipe_id);
            $this->sendNotify($treatments);

            for ($i = 0; $i < $treatments->medicine->number_of_days; $i++) {

                for ($s = 0; $s < CalcDateRecipe::generaScheduleHours(($treatments->medicine->frequency)); $s++) {

                    $startTreatment->create([
                        'user_id' => $treatments->user_id,
                        'start_period' => $carbon->addHour($treatments->medicine->frequency),
                        'end_period' => CalcDateRecipe::setDateExpiration($treatments->medicine->number_of_days),
                        'medicine_id' => $treatments->medicine->id,
                        'schedule_generate' => true,
                        'notifications' => (boolean)$treatments->notifications
                    ]);
                }
            }
            $treatments->forceDelete();
        }
    }

    public function sendNotify($treatments)
    {

        $data = [
            'title' => 'Olá Dr'. ($treatments->medicine->recipes->first()->users->first()->genre == 'F') ? 'a.' : '.' . $treatments->medicine->recipes->first()->doctors->first()->user->name,
            'subtitle' => 'Novo Tratamento Iniciado!',
            'color' => "#55D3AE",
            'body' => 'Seu paciente '.$treatments->medicine->recipes->first()->users->first()->name. ' acabou de iniciar o trataemnto!',
            'thumbnail' => UserHelper::getImageProfile(
                $treatments->medicine->recipes->first()->users->first()->profiles->image,
                $treatments->medicine->recipes->first()->users->first()->genre
            ),
            'category' => 'receita',
            'recipe_id' => $treatments->medicine->recipes->first()->id,
            'token' =>  $treatments->medicine->recipes->first()->doctors->first()->user->device_token,
            'fcm' =>  env('FIREBASE_CLOUD_MESSAGING_SERVER_KEY_DOCTOR')
        ];

        $this->sendPusherNotificationService->notification($data);
    }
}
