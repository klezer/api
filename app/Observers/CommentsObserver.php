<?php

namespace DrPediu\Observers;

use DrPediu\Models\PivotCommentsXRecipesXUser;
use DrPediu\Helpers\UserHelper;
use DrPediu\Services\SendPusherNotificationService;

class CommentsObserver
{
    /**
     * Handle the comment "created" event.
     *
     * @param  \DrPediu\Comment  $comment
     * @return void
     */

    private $pivotCommentsXRecipesXUser;
    private $sendPusherNotificationService;

    public function __construct(PivotCommentsXRecipesXUser $pivotCommentsXRecipesXUser,
                                SendPusherNotificationService $sendPusherNotificationService)
    {
        $this->pivotCommentsXRecipesXUser = $pivotCommentsXRecipesXUser;
        $this->sendPusherNotificationService = $sendPusherNotificationService;
    }

    public function retrieved(PivotCommentsXRecipesXUser $pivotCommentsXRecipesXUser)
    {

    }

    public function created(PivotCommentsXRecipesXUser $pivotCommentsXRecipesXUser)
    {
        $data = [];

        $pivots  = $this->pivotCommentsXRecipesXUser->find($pivotCommentsXRecipesXUser->id);


        if($pivotCommentsXRecipesXUser->user_id == $pivots->recipe->doctors->first()->user_id ) {

            $token = $pivots->recipe->users->first()->device_token;
            $fcm = env('FIREBASE_CLOUD_MESSAGING_SERVER_KEY_PATIENT');
        }else{
            $token = $pivots->recipe->doctors->first()->user->device_token;
            $fcm = env('FIREBASE_CLOUD_MESSAGING_SERVER_KEY_DOCTOR');
        }


        $data = [
            'title' =>  UserHelper::getDataUser($pivotCommentsXRecipesXUser->user_id)['name'],
            'subtitle' => 'Novas Mensagens',
            'color' => "#55D3AE",
            'body' => $pivots->comment->comments,
            'thumbnail' => UserHelper::getImageProfile($pivots->recipe->users->first()->profiles->image,$pivots->recipe->users->first()->genre),
            'category' => 'chat',
            'recipe_id' => $pivotCommentsXRecipesXUser->recipes_id,
            'patient_id' => $pivotCommentsXRecipesXUser->user_id,
            'token' => $token,
            'fcm' => $fcm
        ];

        $this->sendPusherNotificationService->notification($data);

    }

    /**
     * Handle the comment "updated" event.
     *
     * @param  \DrPediu\Comment  $comment
     * @return void
     */
    public function updated(PivotCommentsXRecipesXUser $pivotCommentsXRecipesXUser)
    {
        //
    }

    /**
     * Handle the comment "deleted" event.
     *
     * @param  \DrPediu\Comment  $comment
     * @return void
     */
    public function deleted(PivotCommentsXRecipesXUser $pivotCommentsXRecipesXUser)
    {
        //
    }

    /**
     * Handle the comment "restored" event.
     *
     * @param  \DrPediu\Comment  $comment
     * @return void
     */
    public function restored(PivotCommentsXRecipesXUser $pivotCommentsXRecipesXUser)
    {
        //
    }

    /**
     * Handle the comment "force deleted" event.
     *
     * @param  \DrPediu\Comment  $comment
     * @return void
     */
    public function forceDeleted(PivotCommentsXRecipesXUser $pivotCommentsXRecipesXUser)
    {
        //
    }
}
