<?php

namespace DrPediu\Observers;

use DrPediu\Models\PivotDoctorsxRecipesxUser;
use DrPediu\Helpers\UserHelper;
use DrPediu\Services\SendPusherNotificationService;

class RecipesObserver
{
    /**
     * Handle the recipe "created" event.
     *
     * @param  \DrPediu\Recipe  $recipe
     * @return void
     */

    private $sendPusherNotificationService;


    public function __construct(SendPusherNotificationService $sendPusherNotificationService)
    {
      $this->sendPusherNotificationService = $sendPusherNotificationService;
    }

    public function created(PivotDoctorsxRecipesxUser $pivotDoctorsxRecipesxUser)
    {

        $recipes = $pivotDoctorsxRecipesxUser->find($pivotDoctorsxRecipesxUser->id);

        $data = [
            'title' => 'Olá '. $recipes->user->name,
            'subtitle' => 'Nova Receita',
            'color' => "#55D3AE",
            'body' => "Sua receita já está disponivel para a compra do medicamento!",
            'thumbnail' => UserHelper::getImageProfile($recipes->doctor->user->profiles->image,$recipes->doctor->user->genre),
            'category' => 'receita',
            'recipe_id' => $pivotDoctorsxRecipesxUser->recipes_id,
            'token' => $recipes->user->device_token,
            'fcm' =>  env('FIREBASE_CLOUD_MESSAGING_SERVER_KEY_PATIENT')
        ];

        $this->sendPusherNotificationService->notification($data);

    }

    /**
     * Handle the recipe "updated" event.
     *
     * @param  \DrPediu\Recipe  $recipe
     * @return void
     */
    public function updated(PivotDoctorsxRecipesxUser $recipe)
    {
        //
    }

    /**
     * Handle the recipe "deleted" event.
     *
     * @param  \DrPediu\Recipe  $recipe
     * @return void
     */
    public function deleted(PivotDoctorsxRecipesxUser $recipe)
    {
        //
    }

    /**
     * Handle the recipe "restored" event.
     *
     * @param  \DrPediu\Recipe  $recipe
     * @return void
     */
    public function restored(PivotDoctorsxRecipesxUser $recipe)
    {
        //
    }

    /**
     * Handle the recipe "force deleted" event.
     *
     * @param  \DrPediu\Recipe  $recipe
     * @return void
     */
    public function forceDeleted(PivotDoctorsxRecipesxUser $recipe)
    {
        //
    }
}
