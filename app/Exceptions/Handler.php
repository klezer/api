<?php

namespace DrPediu\Exceptions;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if($exception instanceof ModelNotFoundException) {
            abort(404);
        }
        if ($this->isHttpException($exception)) {
            switch ($exception->getStatusCode()) {
                // not authorized
                case '403':
                    return response()->json(['status' => ['errors' => 'not authorized', 'message' => $exception->getMessage()]], 403);
                    break;

                // not found
                case '404':
                    return response()->json(['status' => ['errors' => 'not found', 'message' => $exception->getMessage() . 'Está rota não existe!']], 404);
                    break;

                // internal error
                case '500':
                    return response()->json(['status' => ['errors' => 'internal error', 'message' => $exception->getMessage()]], 500);
                    break;

                default:
                    return $this->renderHttpException($exception);
                    break;
            }
        } else {
            return parent::render($request, $exception);
        }
    }
}
