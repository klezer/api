<?php

/**
 * Created by Clezer A. Ramos.
 * Date: Wed, 11 Jul 2018 17:07:39 +0000.
 */

namespace DrPediu\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Profile
 * 
 * @property int $id
 * @property int $user_id
 * @property string $image
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \DrPediu\Models\User $user
 *
 * @package DrPediu\Models
 */
class Profile extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'user_id' => 'int'
	];

	protected $fillable = [
		'user_id',
		'image',
		'description'
	];

	public function user()
	{
		return $this->belongsTo(\DrPediu\Models\User::class);
	}
}
