<?php

/**
 * Created by Clezer A. Ramos.
 * Date: Wed, 08 May 2019 18:00:44 -0300.
 */

namespace DrPediu\Models;

use DrPediu\Traits\FullTextSearch;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class DrugInteraction
 *
 * @property int $id
 * @property string $title_interaction
 * @property string $degree_interaction
 * @property string $action_start
 * @property string $recommendation
 * @property string $clinical_effect
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 *
 * @package DrPediu\Models
 */
class DrugInteraction extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes,FullTextSearch;
	protected $table = 'drug_interaction';

	protected $searchable = [
		'title_interaction'
	];
	protected $fillable = [
		'title_interaction',
		'degree_interaction',
		'action_start',
		'recommendation',
		'clinical_effect'
	];
}
