<?php

/**
 * Created by Clezer A. Ramos.
 * Date: Wed, 27 Jun 2018 16:05:28 +0000.
 */

namespace DrPediu\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class State
 * 
 * @property int $id
 * @property string $title
 * @property string $initials
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $cities
 *
 * @package DrPediu\Models
 */
class State extends Eloquent
{
	protected $fillable = [
		'title',
		'initials'
	];

	public function cities()
	{
		return $this->hasMany(\DrPediu\Models\City::class);
	}
	public static function states()
    {
      return self::select('initials as display','id as value')->get();

    }
}
