<?php

/**
 * Created by Clezer A. Ramos.
 * Date: Fri, 08 Feb 2019 16:16:55 +0000.
 */

namespace DrPediu\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Medicine
 * 
 * @property int $id
 * @property string $title
 * @property string $dosage
 * @property int $frequency
 * @property int $number_of_days
 * @property string $target
 * @property \Carbon\Carbon $validity_of_medicine
 * @property string $subtitle
 * @property string $note
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $recipes
 * @property \Illuminate\Database\Eloquent\Collection $start_treatments
 *
 * @package DrPediu\Models
 */
class Medicine extends Eloquent
{
	use SoftDeletes;

	protected $casts = [
		'frequency' => 'int',
		'number_of_days' => 'int'
	];

	protected $dates = [
		'validity_of_medicine'
	];

	protected $fillable = [
		'title',
		'dosage',
		'frequency',
		'number_of_days',
		'target',
		'validity_of_medicine',
		'subtitle',
		'note',
		'sales_of_medicines_id'
	];

	public function recipes()
	{
		return $this->belongsToMany(\DrPediu\Models\Recipe::class, 'pivot_recipes_x_medicines_x_treatments', 'medicine_id', 'recipes_id')
					->withPivot('id', 'deleted_at')
					->withTimestamps();
	}

	public function start_treatments()
	{
		return $this->hasMany(\DrPediu\Models\StartTreatment::class);
	}


}
