<?php

/**
 * Created by Clezer A. Ramos.
 * Date: Thu, 26 Jul 2018 14:31:28 +0000.
 */

namespace DrPediu\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserAddress
 * 
 * @property int $id
 * @property int $user_id
 * @property int $city_id
 * @property string $number
 * @property string $complement
 * @property string $street_code
 * @property string $street_title
 * @property string $district_title
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \DrPediu\Models\City $city
 * @property \DrPediu\Models\User $user
 *
 * @package DrPediu\Models
 */
class UserAddress extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'user_address';

	protected $casts = [
		'user_id' => 'int',
		'city_id' => 'int'
	];

	protected $fillable = [
		'user_id',
		'city_id',
		'number',
		'locality',
		'complement',
		'street_code',
		'street_title',
		'district_title'
	];

	public function city()
	{
		return $this->belongsTo(\DrPediu\Models\City::class);
	}

	public function user()
	{
		return $this->belongsTo(\DrPediu\Models\User::class);
	}
}
