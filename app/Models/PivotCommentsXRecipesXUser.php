<?php

/**
 * Created by Clezer A. Ramos.
 * Date: Wed, 27 Jun 2018 16:04:30 +0000.
 */

namespace DrPediu\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PivotCommentsXRecipesXUser
 * 
 * @property int $id
 * @property int $recipes_id
 * @property int $user_id
 * @property int $comment_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \DrPediu\Models\Comment $comment
 * @property \DrPediu\Models\Recipe $recipe
 * @property \DrPediu\Models\User $user
 *
 * @package DrPediu\Models
 */
class PivotCommentsXRecipesXUser extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'recipes_id' => 'int',
		'user_id' => 'int',
		'comment_id' => 'int'
	];

	protected $fillable = [
		'recipes_id',
		'user_id',
		'comment_id'
	];

	public function comment()
	{
		return $this->belongsTo(\DrPediu\Models\Comment::class);
	}

	public function recipe()
	{
		return $this->belongsTo(\DrPediu\Models\Recipe::class, 'recipes_id');
	}

	public function user()
	{
		return $this->belongsTo(\DrPediu\Models\User::class);
	}
}
