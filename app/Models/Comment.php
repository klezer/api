<?php

/**
 * Created by Clezer A. Ramos.
 * Date: Sun, 02 Dec 2018 14:19:40 +0000.
 */

namespace DrPediu\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Comment
 * 
 * @property int $id
 * @property string $comments
 * @property string $view_comment
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $recipes
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package DrPediu\Models
 */
class Comment extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $fillable = [
		'comments',
        'view_comment',
	];

	protected $guarded = [
	    'id'
    ];

	public function recipes()
	{
		return $this->belongsToMany(\DrPediu\Models\Recipe::class, 'pivot_comments_x_recipes_x_users', 'comment_id', 'recipes_id')
					->withPivot('id', 'user_id', 'deleted_at')
					->withTimestamps();
	}

	public function users()
	{
		return $this->belongsToMany(\DrPediu\Models\User::class, 'pivot_comments_x_recipes_x_users')
					->withPivot('id', 'recipes_id', 'deleted_at')
					->withTimestamps();
	}
}
