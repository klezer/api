<?php

/**
 * Created by Clezer A. Ramos.
 * Date: Thu, 27 Dec 2018 21:00:23 +0000.
 */

namespace DrPediu\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Doctor
 * 
 * @property int $id
 * @property int $user_id
 * @property string $crm_number
 * @property string $signature_image
 * @property string $document_professional
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \DrPediu\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $recipes
 * @property \Illuminate\Database\Eloquent\Collection $users
 * @property \Illuminate\Database\Eloquent\Collection $pivot_doctors_x_specialties
 *
 * @package DrPediu\Models
 */
class Doctor extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'user_id' => 'int'
	];

	protected $fillable = [
		'user_id',
		'crm_number',
		'signature_image',
		'document_professional'
	];

	public function user()
	{
		return $this->belongsTo(\DrPediu\Models\User::class);
	}

	public function recipes()
	{
		return $this->belongsToMany(\DrPediu\Models\Recipe::class, 'pivot_doctors_x_recipes_x_users', 'doctor_id', 'recipes_id')
					->withPivot('id', 'user_id', 'deleted_at')
					->withTimestamps();
	}

	public function users()
	{
		return $this->belongsToMany(\DrPediu\Models\User::class, 'pivot_doctors_x_recipes_x_users')
					->withPivot('id', 'recipes_id', 'deleted_at')
					->withTimestamps();
	}

	public function pivot_doctors_x_specialties()
	{
		return $this->hasMany(\DrPediu\Models\PivotDoctorsXSpecialty::class);
	}
}
