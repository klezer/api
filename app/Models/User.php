<?php

/**
 * Created by Clezer A. Ramos.
 * Date: Wed, 27 Jun 2018 16:03:21 +0000.
 */
namespace DrPediu\Models;
use \Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

/**
 * Class User
 * 
 * @property int $id
 * @property string $name
 * @property string $cpf
 * @property string $rg
 * @property string $genre
 * @property \Carbon\Carbon $date_of_birth
 * @property string $email
 * @property string $password
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $anamnese
 * @property \Illuminate\Database\Eloquent\Collection $contacts
 * @property \Illuminate\Database\Eloquent\Collection $doctors
 * @property \Illuminate\Database\Eloquent\Collection $comments
 * @property \Illuminate\Database\Eloquent\Collection $recipes
 * @property \Illuminate\Database\Eloquent\Collection $profiles
 * @property \Illuminate\Database\Eloquent\Collection $user_addresses
 *
 * @package DrPediu\Models
 */
class User extends Authenticatable
{
    use SoftDeletes, Notifiable, HasApiTokens;

	protected $dates = [
		'date_of_birth'
	];

	protected $hidden = [
		'password',
		'remember_token'
	];

	protected $fillable = [
		'name',
		'cpf',
		'rg',
		'genre',
		'date_of_birth',
		'email',
		'password',
		'device_token',
		'remember_token'
	];

	public function anamnese()
	{
		return $this->hasMany(\DrPediu\Models\Anamnese::class);
	}

	public function contacts()
	{
		return $this->hasMany(\DrPediu\Models\Contact::class);
	}

	public function doctors()
	{
		return $this->belongsToMany(\DrPediu\Models\Doctor::class, 'pivot_doctors_x_recipes_x_users')
					->withPivot('id', 'recipes_id', 'deleted_at')
					->withTimestamps();
	}

	public function comments()
	{
		return $this->belongsToMany(\DrPediu\Models\Comment::class, 'pivot_comments_x_recipes_x_users')
					->withPivot('id', 'recipes_id', 'deleted_at')
					->withTimestamps();
	}

	public function recipes()
	{
		return $this->belongsToMany(\DrPediu\Models\Recipe::class, 'pivot_doctors_x_recipes_x_users', 'user_id', 'recipes_id')
					->withPivot('id', 'doctor_id', 'deleted_at')
					->withTimestamps();
	}

	public function profiles()
	{
		return $this->HasOne(\DrPediu\Models\Profile::class);
	}

	public function user_addresses()
	{
		return $this->hasMany(\DrPediu\Models\UserAddress::class);
	}

    public function user_address()
    {
        return $this->hasOne(\DrPediu\Models\UserAddress::class,'user_id','id');
    }

    public function doctor()
    {
        return $this->HasOne(\DrPediu\Models\Doctor::class,'user_id');
    }
}
