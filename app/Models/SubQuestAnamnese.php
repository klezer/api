<?php

/**
 * Created by Clezer A. Ramos.
 * Date: Wed, 06 Feb 2019 19:51:03 +0000.
 */

namespace DrPediu\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class SubQuestAnamnese
 * 
 * @property int $id
 * @property string $title
 * @property int $id_quest_anamnese
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \DrPediu\Models\QuestsAnamnese $quests_anamnese
 *
 * @package DrPediu\Models
 */
class SubQuestAnamnese extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'sub_quest_anamneses';

	protected $casts = [
		'id_quest_anamnese' => 'int'
	];

	protected $fillable = [
		'title',
		'id_quest_anamnese'
	];

	public function quests_anamnese()
	{
		return $this->belongsTo(\DrPediu\Models\QuestsAnamnese::class, 'id_quest_anamnese');
	}
}
