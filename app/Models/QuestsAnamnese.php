<?php

/**
 * Created by Clezer A. Ramos.
 * Date: Wed, 06 Feb 2019 19:50:25 +0000.
 */

namespace DrPediu\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class QuestsAnamnese
 * 
 * @property int $id
 * @property string $title
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $answer_anamnese
 * @property \Illuminate\Database\Eloquent\Collection $sub_quest_anamnese
 *
 * @package DrPediu\Models
 */
class QuestsAnamnese extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'quests_anamneses';

	protected $fillable = [
		'title'
	];

	public function answer_anamnese()
	{
		return $this->hasMany(\DrPediu\Models\AnswerAnamnese::class, 'id_quest_anamnese');
	}

	public function sub_quest_anamnese()
	{
		return $this->hasMany(\DrPediu\Models\SubQuestAnamnese::class, 'id_quest_anamnese')->select('title');
	}
}
