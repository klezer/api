<?php

/**
 * Created by Clezer A. Ramos.
 * Date: Wed, 27 Jun 2018 16:02:57 +0000.
 */

namespace DrPediu\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PivotDoctorsXRecipesXUser
 * 
 * @property int $id
 * @property int $recipes_id
 * @property int $user_id
 * @property int $doctor_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \DrPediu\Models\Doctor $doctor
 * @property \DrPediu\Models\Recipe $recipe
 * @property \DrPediu\Models\User $user
 *
 * @package DrPediu\Models
 */
class PivotDoctorsXRecipesXUser extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'recipes_id' => 'int',
		'user_id' => 'int',
		'doctor_id' => 'int'
	];

	protected $fillable = [
		'recipes_id',
		'user_id',
		'doctor_id'
	];

	public function doctor()
	{
		return $this->belongsTo(\DrPediu\Models\Doctor::class);
	}

	public function recipe()
	{
		return $this->belongsTo(\DrPediu\Models\Recipe::class, 'recipes_id');
	}

	public function user()
	{
		return $this->belongsTo(\DrPediu\Models\User::class);
	}
    public function comment()
    {
        return $this->belongsTo(\DrPediu\Models\Comment::class);
    }
}
