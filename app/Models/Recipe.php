<?php

/**
 * Created by Clezer A. Ramos.
 * Date: Wed, 27 Jun 2018 16:01:57 +0000.
 */

namespace DrPediu\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Recipe
 * 
 * @property int $id
 * @property string $description
 * @property string $instructions
 * @property \Carbon\Carbon $validity_of_recipe
 * @property int $type_recipe_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \DrPediu\Models\TypeRecipe $type_recipe
 * @property \Illuminate\Database\Eloquent\Collection $comments
 * @property \Illuminate\Database\Eloquent\Collection $users
 * @property \Illuminate\Database\Eloquent\Collection $doctors
 * @property \Illuminate\Database\Eloquent\Collection $medicines
 * @property \Illuminate\Database\Eloquent\Collection $treatments
 *
 * @package DrPediu\Models
 */
class Recipe extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'type_recipe_id' => 'int'
	];

	protected $dates = [
		'validity_of_recipe'
	];

	protected $fillable = [
		'description',
		'instructions',
		'validity_of_recipe',
		'start_recipe',
		'type_recipe_id'
	];

	public function type_recipe()
	{
		return $this->belongsTo(\DrPediu\Models\TypeRecipe::class);
	}

	public function comments()
	{
		return $this->belongsToMany(\DrPediu\Models\Comment::class, 'pivot_comments_x_recipes_x_users', 'recipes_id')
					->withPivot('id', 'user_id', 'deleted_at')
					->withTimestamps();
	}

	public function users()
	{
		return $this->belongsToMany(\DrPediu\Models\User::class, 'pivot_doctors_x_recipes_x_users', 'recipes_id')
					->withPivot('id', 'doctor_id', 'deleted_at')
					->withTimestamps();
	}

	public function doctors()
	{
		return $this->belongsToMany(\DrPediu\Models\Doctor::class, 'pivot_doctors_x_recipes_x_users', 'recipes_id')
					->withPivot('id', 'user_id', 'deleted_at')
					->withTimestamps();
	}

	public function medicines()
	{
		return $this->belongsToMany(\DrPediu\Models\Medicine::class, 'pivot_recipes_x_medicines_x_treatments', 'recipes_id')
					->withPivot('id', 'deleted_at')
					->withTimestamps();
	}

}
