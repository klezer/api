<?php

/**
 * Created by Clezer A. Ramos.
 * Date: Wed, 06 Feb 2019 19:51:49 +0000.
 */

namespace DrPediu\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class SubAnswerAnamnese
 * 
 * @property int $id
 * @property string $title
 * @property int $id_answer_anamnese
 * @property string $answer
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \DrPediu\Models\AnswerAnamnese $answer_anamnese
 *
 * @package DrPediu\Models
 */
class SubAnswerAnamnese extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'sub_answer_anamneses';

	protected $casts = [
		'id_answer_anamnese' => 'int'
	];

	protected $fillable = [
		'title',
		'id_answer_anamnese',
		'answer'
	];

	public function answer_anamnese()
	{
		return $this->belongsTo(\DrPediu\Models\AnswerAnamnese::class, 'id_answer_anamnese');
	}
}
