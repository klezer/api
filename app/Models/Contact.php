<?php

/**
 * Created by Clezer A. Ramos.
 * Date: Sun, 02 Dec 2018 14:19:40 +0000.
 */

namespace DrPediu\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Contact
 * 
 * @property int $id
 * @property int $user_id
 * @property string $telephone_one
 * @property string $telephone_two
 * @property string $cell_phone
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \DrPediu\Models\User $user
 *
 * @package DrPediu\Models
 */
class Contact extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'user_id' => 'int'
	];

	protected $fillable = [
		'user_id',
		'telephone_one',
		'telephone_two',
		'cell_phone'
	];

	public function user()
	{
		return $this->belongsTo(\DrPediu\Models\User::class);
	}
}
