<?php

/**
 * Created by Clezer A. Ramos.
 * Date: Wed, 27 Jun 2018 15:59:54 +0000.
 */

namespace DrPediu\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TypeRecipe
 * 
 * @property int $id
 * @property string $type
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $recipes
 *
 * @package DrPediu\Models
 */
class TypeRecipe extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $fillable = [
		'type',
		'description'
	];

	public function recipes()
	{
		return $this->hasMany(\DrPediu\Models\Recipe::class);
	}
}
