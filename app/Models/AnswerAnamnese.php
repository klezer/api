<?php

/**
 * Created by Clezer A. Ramos.
 * Date: Wed, 06 Feb 2019 19:51:37 +0000.
 */

namespace DrPediu\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class AnswerAnamnese
 * 
 * @property int $id
 * @property int $id_quest_anamnese
 * @property int $user_id
 * @property string $option
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \DrPediu\Models\QuestsAnamnese $quests_anamnese
 * @property \DrPediu\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $sub_answer_anamnese
 *
 * @package DrPediu\Models
 */
class AnswerAnamnese extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'answer_anamneses';

	protected $casts = [
		'id_quest_anamnese' => 'int',
		'user_id' => 'int'
	];

	protected $fillable = [
		'id_quest_anamnese',
		'user_id',
		'option'
	];

	public function quests_anamnese()
	{
		return $this->belongsTo(\DrPediu\Models\QuestsAnamnese::class, 'id_quest_anamnese');
	}

	public function user()
	{
		return $this->belongsTo(\DrPediu\Models\User::class);
	}

	public function sub_answer_anamnese()
	{
		return $this->hasMany(\DrPediu\Models\SubAnswerAnamnese::class, 'id_answer_anamnese')->select('title','answer');
	}
}
