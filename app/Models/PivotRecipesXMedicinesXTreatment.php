<?php

/**
 * Created by Clezer A. Ramos.
 * Date: Wed, 27 Jun 2018 16:01:04 +0000.
 */

namespace DrPediu\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PivotRecipesXMedicinesXTreatment
 * 
 * @property int $id
 * @property int $recipes_id
 * @property int $treatment_id
 * @property int $medicine_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \DrPediu\Models\Medicine $medicine
 * @property \DrPediu\Models\Recipe $recipe
 * @property \DrPediu\Models\Treatment $treatment
 *
 * @package DrPediu\Models
 */
class PivotRecipesXMedicinesXTreatment extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'recipes_id' => 'int',
		'medicine_id' => 'int'
	];

	protected $fillable = [
		'recipes_id',
		'medicine_id'
	];

	public function medicine()
	{
		return $this->belongsTo(\DrPediu\Models\Medicine::class);
	}

	public function recipe()
	{
		return $this->belongsTo(\DrPediu\Models\Recipe::class, 'recipes_id');
	}
}
