<?php

/**
 * Created by Clezer A. Ramos.
 * Date: Fri, 08 Feb 2019 16:09:01 +0000.
 */

namespace DrPediu\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class StartTreatment
 * 
 * @property int $id
 * @property int $medicine_id
 * @property int $user_id
 * @property \Carbon\Carbon $start_period
 * @property \Carbon\Carbon $end_period
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \DrPediu\Models\Medicine $medicine
 *
 * @package DrPediu\Models
 */
class StartTreatment extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'medicine_id' => 'int',
		'user_id' => 'int'
	];

	protected $dates = [
		'start_period',
		'end_period',
		'taken_period',
	];

	protected $fillable = [
		'medicine_id',
		'user_id',
		'start_period',
		'end_period',
		'taken_period',
        'notifications',
        'taken',
        'schedule_generate'
	];

	public function medicine()
	{
		return $this->belongsTo(\DrPediu\Models\Medicine::class);
	}
}
