<?php

/**
 * Created by Clezer A. Ramos.
 * Date: Mon, 04 Feb 2019 18:51:18 +0000.
 */

namespace DrPediu\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class DiscartPointAddress
 * 
 * @property int $id
 * @property int $discart_point_id
 * @property string $street_code
 * @property string $street_title
 * @property string $number
 * @property string $complement
 * @property string $latitude
 * @property string $longitude
 * @property string $city
 * @property string $states
 * @property string $district
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \DrPediu\Models\DiscartPoint $discart_point
 *
 * @package DrPediu\Models
 */
class DiscartPointAddress extends Eloquent
{
	protected $table = 'discart_point_address';

	protected $casts = [
		'discart_point_id' => 'int'
	];

	protected $fillable = [
		'discart_point_id',
		'street_code',
		'street_title',
		'number',
		'complement',
		'city',
		'state',
		'district',
        'latitude',
        'longitude'
	];

	public function discart_point()
	{
		return $this->belongsTo(\DrPediu\Models\DiscartPoint::class);
	}
}
