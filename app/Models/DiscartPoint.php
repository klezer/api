<?php

/**
 * Created by Clezer A. Ramos.
 * Date: Mon, 04 Feb 2019 18:50:54 +0000.
 */

namespace DrPediu\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class DiscartPoint
 * 
 * @property int $id
 * @property string $title
 * @property string $fone
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $discart_point_addresses
 *
 * @package DrPediu\Models
 */
class DiscartPoint extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $fillable = [
		'title',
		'fone'
	];

	public function discart_point_addresses()
	{
		return $this->hasMany(\DrPediu\Models\DiscartPointAddress::class);
	}
}
