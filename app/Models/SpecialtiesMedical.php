<?php

/**
 * Created by Clezer A. Ramos.
 * Date: Thu, 27 Dec 2018 20:56:11 +0000.
 */

namespace DrPediu\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class SpecialtiesMedical
 * 
 * @property int $id
 * @property string $title
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $pivot_doctors_x_specialties
 *
 * @package DrPediu\Models
 */
class SpecialtiesMedical extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $fillable = [
		'title',
		'description'
	];

	public function pivot_doctors_x_specialties()
	{
		return $this->hasMany(\DrPediu\Models\PivotDoctorsXSpecialty::class, 'specialty_id');
	}
}
