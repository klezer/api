<?php

/**
 * Created by Clezer A. Ramos.
 * Date: Sun, 02 Dec 2018 14:19:40 +0000.
 */

namespace DrPediu\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class City
 * 
 * @property int $id
 * @property int $state_id
 * @property string $title
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \DrPediu\Models\State $state
 * @property \Illuminate\Database\Eloquent\Collection $user_addresses
 *
 * @package DrPediu\Models
 */
class City extends Eloquent
{
	protected $table = 'citys';

	protected $casts = [
		'state_id' => 'int'
	];

	protected $fillable = [
		'state_id',
		'title'
	];

	public function state()
	{
		return $this->belongsTo(\DrPediu\Models\State::class);
	}

	public function user_addresses()
	{
		return $this->hasMany(\DrPediu\Models\UserAddress::class);
	}
}
