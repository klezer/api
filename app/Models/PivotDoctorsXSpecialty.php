<?php

/**
 * Created by Clezer A. Ramos.
 * Date: Thu, 27 Dec 2018 20:55:55 +0000.
 */

namespace DrPediu\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PivotDoctorsXSpecialty
 * 
 * @property int $id
 * @property int $specialty_id
 * @property int $doctor_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \DrPediu\Models\Doctor $doctor
 * @property \DrPediu\Models\SpecialtiesMedical $specialties_medical
 *
 * @package DrPediu\Models
 */
class PivotDoctorsXSpecialty extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'pivot_doctors_x_specialty';

	protected $casts = [
		'specialty_id' => 'int',
		'doctor_id' => 'int'
	];

	protected $fillable = [
		'specialty_id',
		'doctor_id'
	];

	public function doctor()
	{
		return $this->belongsTo(\DrPediu\Models\Doctor::class);
	}

	public function specialties_medical()
	{
		return $this->belongsTo(\DrPediu\Models\SpecialtiesMedical::class, 'specialty_id');
	}
	public static function getSpecialtiesDoctor($doctor_id)
	{
		return self::where('doctor_id',$doctor_id)->first()->specialties_medical;
	}
}
