<!DOCTYPE html>
<html>
<style>
    .page-break {
        page-break-before: always;
    }
</style>
<head>
    <meta charset="utf-8">
    <title>Dr Pediu </title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <style>
        h1,h2,h3,h4,p,span,div { font-family: DejaVu Sans; }
    </style>
</head>

<body>

@foreach ($invoice->medicines as $item)

    <div style="clear:both; position:relative;">
        <div style="position:absolute; left:0pt; width:250pt;">
            <img class="img-rounded" height="60" src="{{asset('seeds-images/DrPediu_logo.png')}}">
            {{--<img class="img-rounded" height="60" src="https://drpediu.doc88.com.br//seeds-images/DrPediu_logo.png">--}}
        </div>
        <div style="margin-left:300pt;">
            <b>Data: </b> {{ now()->formatLocalized('%A %d %B %Y') }}<br />
            {{--@if ($invoice->number)--}}
            {{--<b>Receita Medica #: </b>--}}
            {{--@endif--}}
            {{--{{dd($invoice)}}--}}
            {{--{{dd(asset('seeds-images/DrPediu_logo.png'))}}--}}
            <br />
        </div>
    </div>
    <br />
    <h2>Receita Médica</h2>
    <div style="clear:both; position:relative;">
        <div style="position:absolute; left:0pt; width:250pt">
            <h4>Dados do Medico:</h4>
            <div class="panel panel-default">
                <div class="panel-body">
                    {!! $invoice->doctors->count() == 0 ? '<i>No business details</i><br />' : '' !!}
                    {{ $invoice->doctors->first()->user->name }}<br />
                    CRM: {{ $invoice->doctors->first()->crm_number }}<br />
                    {{ $invoice->doctors->first()->user->email }}<br />&nbsp;
                </div>

            </div>
        </div>
        <div style="margin-left: 300pt;">
            <h4>Dados do Paciente:</h4>
            <div class="panel panel-default">
                <div class="panel-body">
                    {!! $invoice->users->count() == 0 ? '<i>No customer details</i><br />' : '' !!}
                    {{ $invoice->users->first()->name }}<br />
                    {{ $invoice->users->first()->email }}<br />
                    CPF:{{ $invoice->users->first()->cpf }}<br />
                    RG:{{ $invoice->users->first()->rg }}<br />
                </div>
            </div>
        </div>
    </div>


    <h4>Medicação:</h4>
    <table class="table table-bordered">
        <thead>

        </thead>
        <tbody>

        <tr>
            <td><b>Nome</b></td>
            <td>{{$item->title}}</td>
        </tr>
        <tr>
            <td><b>Posologia</b></td>
            <td>{{$item->subtitle}}</td>
        </tr>
        <tr>
            <td><b>Dosagem</b></td>
            @if($item->frequency > 1)
                <td>{{$item->dosage}} Unidades</td>
            @else
                <td>{{$item->dosage}} Unidade</td>
            @endif
        </tr>
        <tr>
            <td><b>Frequência</b></td>
            @if($item->frequency > 1)
                <td>De {{$item->frequency}} em {{$item->frequency}} Horas</td>
            @else
                <td>De {{$item->frequency}} em {{$item->frequency}} Hora</td>
            @endif
        </tr>
        <tr>
            <td><b>Duração</b></td>
            @if($item->number_of_days > 1)
                <td>{{$item->number_of_days}} Dias</td>
            @else
                <td>{{$item->number_of_days}} Dia</td>
            @endif
        </tr>
        @if(!empty($item->note))
            <tr>
                <td><b>Observação: </b></td>
                <td>{{$item->note}}</td>
            </tr>
        @endif

        </tbody>
    </table>
    <div class="well">
        Validade da Receita:  {{ $invoice->validity_of_recipe->format('d/m/Y') }}
    </div>
    <br>
    <br>

    <p>________________________________________________________________________________</p>
    {{ $invoice->doctors->first()->user->name .' CRM: ' . $invoice->doctors->first()->crm_number }}
     <br><br><br><br><br><br><br>
    {{--<div class="page-break"></div>--}}

@endforeach

</body>
</html>
