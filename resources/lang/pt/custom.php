<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 21/03/19
 * Time: 10:43
 */

return [

	/*
	|--------------------------------------------------------------------------
	| Authentication Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used during authentication for various
	| messages that we need to display to the user. You are free to modify
	| these language lines according to your application's requirements.
	|
	*/
	'store' => [
		'success' => 'Registro criado com sucesso!'
	],
	'update' => [
		'success' => 'Registro atualizado com sucesso!'
	],
	'update-ckeckout' => [
		'success' => 'Check-out realizado com sucesso!',
		'error' => 'Processo de check-out inválido!',
		'not-checkout' => 'Você não possui nenhum processo em andamento!',
	],
	'store-checkin' => [
		'success' => 'Check-in realizado com sucesso!',
		'has-checkin' => 'Você já realizou esse procedimento!',
		'has-reserve-other-parking' => 'Você já possui uma reserva em outro Estacionamento!',
	],
	'store-reserve' => [
		'success' => 'Reserva realizada com sucesso!',
		'has-reserve' => 'Você já possui uma reserva agendada!',
		'not-vacancy' => 'Este estacionamento não possui mais vagas disponíveis!',
		'cancel-reserve' => 'Reserva cancelada com sucesso!',
		'has-checkin' => 'Você já possui um checkin em andamento!',

	],
	'cancel-reserve' => [
		'cancel-reserve' => 'Reserva cancelada com sucesso!',
		'not-reserve' => 'Desculpe!, reserva inexistente!',
	],

	'image' => [
		'error' => 'Imagem não existente!',
		''
	],
	'login' => [

		'failed' => 'Login ou Senha inválidos!',
		'throttle' => 'Muitas tentativas de login. Tente novamente em :seconds segundos.',
	],
	'sms' => [
		'success' => 'Sms enviado com sucesso!',
		'error' => 'Sms inválido!',
		'validate' => 'Sms Validado com sucesso!',
	],
	'hoops' => [
		'error' => 'Dados não encontrados!',
	],
	'notFound' => [
		'error' => 'Dados não encontrados!',
	],
	'not-history' => [
		'error' => 'Não exite histórico de uso',
	],
	'notvagance' => [
		'error' => 'Não à vagas disponivéis!',
	],
	'vehicle' => [
		'delete' => 'Veículo removido com sucesso!',
		'error' => 'Veículo inexistente!',
	],
	'login-update' => [
		'success' => 'Senha atualizada com sucesso!',
		'error' => 'Esse usuário não existe!'
	],
	'cadastro' => [
		'update' => 'Cadastro atualizado com sucesso!',
		'error' => 'O usuário não existe!'
	],
	'profile' => [
		'not-user' => 'O usuário não existe!',
		'update' => 'Registro atualizado com sucesso!',
		'error' => 'O usuário não existe!'
	],
	'user' => [
	'not-vehicle' => 'Você não possui nenhum veículo adicionado!'
	],
	'contact-sms' => [
		'not-contact' => 'Você não possui nenhum telefone vincuado!',
		'success' => 'Sms enviado com sucesso!'
	],
	'pharmacies' => [
		'success' => 'Procedimento realizado com sucesso!',
		'error' => 'Este medicamento já foi vendido!',
		'not-found-cpf' => "Este paciente não possui medicamentos para a venda!"
	]

];
