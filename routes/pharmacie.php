<?php

Route::prefix('v3')->middleware('client.passport')->namespace('ApiAuth')->group(function () {
	Route::get('get-recipe-for-cpf/{cpf}', 'PharmaciesController@getRecipeForCPFUser')->name('get');
	Route::post('sales-medicines', 'PharmaciesController@updateSalesMedicines')->name('update');
});
