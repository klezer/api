<?php

Route::prefix('v2')->middleware('client.passport')->namespace('ApiAuth')->group(function () {

	Route::name('address.')->group(function () {

		Route::get('doctor-cad-citys/{state}', 'CadastroController@getCitys')->name('api.get.citys');
		Route::get('doctor-cad-states', 'CadastroController@getStates')->name('api.get.states');
	});

	Route::name('register.')->group(function () {

		Route::post('doctor-cad-create', 'RegisterDoctorController@create')->name('doctor.cad.create');
		Route::post('doctor-cad-update', 'RegisterDoctorController@update')->name('doctor.cad.update');
		Route::get('doctor-cad-get/{id}', 'RegisterDoctorController@getRegisterDoctor')->name('doctor.cad.get');

	});

	Route::name('home.')->group(function () {
		Route::get('doctor-list-recipe/{id}/{search?}', 'DoctorController@getListRecipes')->name('doctor.get.user');
		Route::get('doctor-list-all-recipe-for-patient/{patient}/{doctor}', 'DoctorController@getListRecipesPatient')->name('doctor.list.all.recipes.patient');
	});

	Route::name('medicine.')->group(function () {
		Route::get('doctor-list-medicine/{search?}', 'MedicineController@listMedicine')->name('list.medicine');
	});


	Route::name('login.')->group(function () {
		Route::post('doctor-login', 'LoginController@authenticate')->name('api.authenticate');
		Route::post('doctor-login-update', 'LoginController@update')->name('api.update');
	});

	Route::name('files.')->group(function () {
		Route::get('doctor-storage/{filename}', 'RegisterDoctorController@getDocumentProfessional')->name('doctor.document.get');
	});

	Route::name('doctor.patient.')->group(function () {
		Route::get('doctor-list-patient/{id}/{search?}', 'DoctorController@getListMedicalPatient')->name('list');
	});

	Route::name('doctor.message.')->group(function () {
		Route::get('doctor-list-message/{doctor}/{search?}', 'DoctorController@getAllMessage')->name('list.message.doctor');
	});

	Route::name('doctor.recipe.')->group(function () {
		Route::get('doctor-recipe-info-patient/{recipe}', 'DoctorController@getRecipeIfoPatient')->name('get.recipe.info');
		Route::get('doctor-recipe-patient/{recipe}', 'PatientController@getRecipeUser')->name('api.get.recipe');
	});

	Route::name('comment.')->group(function () {
		Route::get('doctor-all-comments-recipe/{recipe_id}/{user}', 'PatientController@getAllCommentsRecipes')->name('doctor.comments.recipe');
		Route::post('doctor-comment-create', 'CommentController@createService')->name('create.comment');
		Route::post('pusher-comment-update', 'CommentController@updateCommentForPusher')->name('pusher.comment.update');
	});

	Route::name('doctor.get.info.patient')->group(function () {
		Route::get('doctor-info-cad-patient/{user}', 'CadastroController@getRegister')->name('get.register');
	});

	Route::name('recipes.')->group(function () {
		Route::post('doctor-create-recipe', 'DoctorController@create')->name('recipes.create');
		Route::get('doctor-search-user-cpf/{cpf}', 'RegisterDoctorController@getRegisterUserForCpf')->name('doctor.get.user.cpf');
		Route::get('doctor-get-user-id/{id}', 'RegisterDoctorController@getRegisterUserForId')->name('doctor.info.patient');
		Route::post('doctor-store-patient-cpf', 'RegisterDoctorController@storeUserForCpf')->name('doctor.store.patient');
	});

	Route::name('profile.')->group(function () {
		Route::post('doctor-profile-create', 'ProfileController@store')->name('api.create.profile');
	});

	Route::name('specialties.')->group(function () {
		Route::get('doctor-list-specialty', 'SpecialtyController@listSpecialties')->name('list.specialty');
	});

	Route::name('anamnese.')->group(function () {
		Route::get('doctor-get-anamnese-patient/{id}', 'AnamneseController@getAnswerAnamneseForUser');
		Route::post('doctor-interaction-medical', 'DrugInteractionController@hasInteractionMedical');
	});

});