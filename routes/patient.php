<?php

Route::prefix('v1')->middleware('client.passport')->namespace('ApiAuth')->group(function () {

	Route::name('cadastro.')->group(function () {
		Route::post('patient-cad-create', 'CadastroController@create')->name('create');
		Route::get('patient-cad-citys/{state}', 'CadastroController@getCitys')->name('get.citys');
		Route::get('patient-cad-states', 'CadastroController@getStates')->name('get.states');
		Route::post('patient-cad-update', 'CadastroController@update')->name('api.update');
		Route::get('patient-cad-get/{user}', 'CadastroController@getRegister')->name('get.register');
	});

	Route::name('login.')->group(function () {
		Route::post('patient-login', 'LoginController@authenticate')->name('authenticate');
		Route::post('patient-login-update', 'LoginController@update')->name('update');
	});

	Route::name('recipes.')->group(function () {
		Route::get('patient-list-recipe/{user}/{search?}', 'PatientController@getAllUserRecipes')->name('get.all');
		Route::get('patient-recipe-doctor/{recipe}', 'PatientController@getRecipeUser')->name('api.get.recipe');
		Route::get('patient-all-comments-recipe/{recipe_id}/{user}', 'PatientController@getAllCommentsRecipes')->name('get.comments');
		Route::get('patient-slice-recipe/{user}/{search?}', 'StartTreatmentController@getStartTreatment')->name('get.recipes.slice');
		Route::post('patient-slice-medical-update', 'StartTreatmentController@update')->name('update.medical.slice');

	});

	Route::name('treatment.')->group(function () {
		Route::post('patient-treatment-create', 'StartTreatmentController@create')->name('create.treatment');
		Route::get('patient-treatment-calendar/{user_id}', 'StartTreatmentController@getCalendarTreatment')->name('calendar.treatment');
	});

	Route::name('profile.')->group(function () {
		Route::post('patient-profile-create', 'ProfileController@store')->name('create.profile');
		Route::get('storage/{filename}', 'ProfileController@getImage')->name('api.get.image');
	});

	Route::name('comment.')->group(function () {
		Route::post('patient-comment-create', 'CommentController@createService')->name('create.comment');
	});
	Route::name('anamnese.')->group(function () {
		Route::post('patient-anamnese-create', 'AnamneseController@create')->name('create.anamnese');
		Route::post('patient-anamnese-update', 'AnamneseController@update')->name('create.anamnese');
		Route::get('patient-anamnese-get/{id_user}', 'AnamneseController@getAnswerAnamneseForUser')->name('get.anamnese');
		Route::get('patient-anamnese-structure', 'AnamneseController@structureAnamnese')->name('list.anamnese');
	});

	Route::name('comment.')->group(function () {
		Route::post('pusher-comment-update', 'CommentController@updateCommentForPusher')->name('pusher.comment.update');
	});

	Route::name('patient.message.')->group(function () {
		Route::get('patient-list-message/{patient}/{search?}', 'PatientController@getAllMessage')->name('list.message.doctor');
	});
	Route::name('patient.doctor.')->group(function () {
		Route::get('patient-list-doctor/{id}/{search?}', 'PatientController@getListMyDoctors')->name('list');
		Route::get('patient-list-medical-for-doctor/{doctor_id}/{user_id}', 'PatientController@listMedicalsRecipeForDoctor')->name('list');
	});

	Route::name('patient.info.')->group(function () {
		Route::get('patient-recipe-info-doctor/{recipe_id}', 'PatientController@getRecipeInfoDoctor')->name('list');
		Route::get('patient-info-cad-doctor/{id}', 'DoctorController@getDoctorInfoCad')->name('list');
	});

	Route::name('discart.points')->group(function () {
		Route::get('discart-points-list/{search?}', 'DiscartPointsController@list')->name('list.discart.points');
	});

	Route::name('home.')->group(function () {
		Route::get('patient-list-all-recipe-for-doctor/{patient}/{doctor}', 'PatientController@getListRecipesPatient')->name('patient.list.all.recipes.doctor');
	});

	Route::name('generate.pdf')->group(function () {
		Route::get('patient-generate-recipe-pdf/{recipe_id}', 'GeneratePdfController@generatePdfRecipe')->name('patient.list.all.recipes.doctor');
	});

});