<?php

#------------------------------------------------ Rotas para o app do Paciente -----------------------------------------------------------
include ('patient.php');
#------------------------------------------------Rotas para o app do Medico----------------------------------------------------------------
include ('doctor.php');
#------------------------------------------------Rotas para o serviço de pharmacies vendas------------------------------------------------
include ('pharmacie.php');
#-----------------------------------------------------------------------------------------------------------------------------------------
Route::name('reset.')->namespace('Auth')->group(function () {
	Route::post('send-email-reset', 'ForgotPasswordController@sendResetLinkEmail')->name('send.email.reset');
	Route::post('password-reset', 'ResetPasswordController@reset')->name('reset.password');
});

Route::name('generate.pdf')->group(function () {
    Route::get('patient-generate-recipe-pdf/{recipe_id}', 'ApiAuth\\GeneratePdfController@generatePdfRecipe')->name('patient.list.all.recipes.doctor');
});

Route::name('profile.')->namespace('ApiAuth')->group(function () {
    Route::get('storage/{filename}', 'ProfileController@getImage')->name('api.get.image');
});

Route::post('api-profile-create', 'ApiAuth\\ProfileController@store')->name('api.create.profile');
#rotas funcionais apos ser realiado a requisição na rota (api.login)

Route::middleware('auth:api')->get('/auth', 'ApiAuth\\LoginController@auth')->name('auth');
